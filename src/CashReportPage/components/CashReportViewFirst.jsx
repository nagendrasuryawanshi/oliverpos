import React from 'react';
import { connect } from 'react-redux';
import { cash_reportActions } from '../actions/cash_report.action';

const cashierList = [
    {
        cashier_name:'Arno Barty',
        cashier_status:'OPEN',
        cuurent_date:'Today',
        current_time:'1:31PM',
        cash_detail:[
            {
                description	: 'Cash Sale',
                user : 'Arno Barty',
                time : '16.54PM',
                amount	:'420.00',
                balance : '480.00'

            },
            {
                description	: 'Cash Sale',
                user : 'Arno Barty',
                time : '13.54PM',
                amount	:'420.00',
                balance : '480.00'

            }
        ]

    },
    {
        cashier_name:'Anita Boheme',
        cashier_status:'1:31PM',
        cuurent_date:'10 October 2018',
        current_time:'1:31PM',
        cash_detail:[
            {
                description	: 'Cash Sale',
                user : 'Anita Boheme',
                time : '13.54PM',
                amount	:'420.00',
                balance : '480.00'

            },
            {
                description	: 'Cash Sale',
                user : 'Anita Boheme',
                time : '13.54PM',
                amount	:'420.00',
                balance : '480.00'

            }
        ]

    }
]



class CashReportViewFirst extends React.Component {
    constructor(props) {
        super(props);

    }

    render() {
        return (
            <div className="col-xs-5 col-sm-3 p-0">
                <div className="items">
                    <div className="panel panel-default panel-product-list p-0 bor-customer">
                        <div className="overflowscroll window-header-search">
                            <table className="table table-hover table-style1 tr1 font tbl-topbottom-1">
                                <colgroup>
                                    <col width="*" />
                                    <col width="40" />
                                </colgroup>
                                <tbody>
                                    {cashierList && cashierList.map((item,index)=>{
                                        return(
                                            <tr>
                                            <td>
                                              {item.cuurent_date}
                                              <p className="comman_subtitle">{item.cashier_name}</p>
                                            </td>
                                            <td className="text-right">
                                                {item.cashier_status}
                                           </td>
                                        </tr>
                                        )
                                    })}
                                   
                                    {/* <tr className="table_row_selected ">
                                        <td>
                                            12 October 2018
                                   <p className="comman_subtitle">Anita Boheme</p>
                                        </td>
                                        <td className="text-right">
                                            1:31PM
                               </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            10 October 2018
                                   <p className="comman_subtitle">Anita Boheme</p>
                                        </td>
                                        <td className="text-right">
                                            1:31PM
                               </td>
                                    </tr> */}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
function mapStateToProps(state) {
    const { cash_reportlist } = state;
    return {
        cash_reportlist
    };
}

const connectedCashReportViewFirst = connect(mapStateToProps, cash_reportActions)(CashReportViewFirst);
export { connectedCashReportViewFirst as CashReportViewFirst };