import Config from '../../Config'

export const settingService = {
    getAll
};

const API_URL=Config.key.OP_API_URL

function getAll() {
    const requestOptions = {
        method: 'GET',
     };

    return fetch(`${API_URL}/users`, requestOptions).then(handleResponse);
}


function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                logout();
                location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}