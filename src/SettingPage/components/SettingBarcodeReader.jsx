import React from 'react';
import { connect } from 'react-redux';
import { settingActions } from '../actions/setting.action';



class SettingBarcodeReader extends React.Component {

    render() {
        return (
            <div id="setting-barcode" className="tab-pane fade">
            </div>
        )
    }

}


function mapStateToProps(state) {
    const { registering } = state.registration;
    return {
        registering
    };
}

const connectedSettingBarcodeReader = connect(mapStateToProps, settingActions)(SettingBarcodeReader);
export { connectedSettingBarcodeReader as SettingBarcodeReader };
