import Config from '../../Config'

export const checkoutService = {
    getAll,
    save
};

const API_URL=Config.key.OP_API_URL

function getAll() {
    const requestOptions = {
        method: 'GET',
     };

    return fetch(`${config.apiUrl}/users`, requestOptions).then(handleResponse);
}

function save(shop_order) {
    console.log("JSON.stringify(shop_order) " ,JSON.stringify(shop_order))
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(shop_order)
    };

    return fetch(`${API_URL}/ShopOrders/Save`, requestOptions).then(handleResponse);
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                logout();
                location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}

