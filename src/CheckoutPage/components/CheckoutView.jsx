import React from 'react';
import { connect } from 'react-redux';
import { checkoutActions } from '../actions/checkout.action';
import { CommonHeaderFirst } from '../../_components';
import { CheckoutViewFirst } from './CheckoutViewFirst';
import { CheckoutViewSecond } from './CheckoutViewSecond';
import { CheckoutViewThird } from './CheckoutViewThird';
import { customerActions } from  '../../CustomerPage/actions/customer.action';
import { setOrderPayments ,getOrderPayments } from '../Checkout';
import { cartProductActions } from '../../_actions';
import { default as NumberFormat } from 'react-number-format';
import { LoadingModal } from '../../_components'


class CheckoutView extends React.Component {
    constructor(props) {
        super(props);
          this.state={
                user_fname : '',
                user_lname : '',
                user_address : '',
                user_city : '',
                user_contact : '',
                user_notes : '',
                user_email : '',
                user_pcode : '',
                user_id : '',
                UDID : '',
                paying_type : '',
                paying_amount : '',
                checkList : null,
                add_note : null,
                paid_amount : null,
                location_id : null,
                lay_Away : null,
                final_place_order : null,
                store_credit : 0,
                // order_status : "pending",
                set_order_notes : new Array(),
                set_calculator_remaining_amount : 0,
                isShowLoader : false,
           }

          this.handleChange = this.handleChange.bind(this);
          this.handleSubmit = this.handleSubmit.bind(this);
          this.getPayment = this.getPayment.bind(this);
          this.setPayment = this.setPayment.bind(this);
          this.layAwayPayment = this.layAwayPayment.bind(this);
          this.setOrderPartialPayments = this.setOrderPartialPayments.bind(this);
          this.createOrder = this.createOrder.bind(this);
          this.openOrderPopup = this.openOrderPopup.bind(this);
          this.setCalculatorRemainingprice = this.setCalculatorRemainingprice.bind(this);
    }

    getPayment(type, amount){
        this.setState({
            paying_type : type,
            paying_amount : amount.replace(",",""),
        });        
    }

    layAwayPayment(type ,amount){
         this.setState({
            lay_Away:type,
            store_credit:amount
        })
       
      
        if(type == 'lay_away'){
              $('.addnoctehere').modal();
           }  
        
        
    }

    componentWillMount(){
        let checkList = localStorage.getItem('CHECKLIST');
        let location_id = localStorage.getItem('Location')
        
        this.setState({
            checkList : JSON.parse(checkList),
            location_id : location_id,
        })       
    }
   
    componentDidMount(){
        setTimeout(function(){
            setHeightDesktop();        
        }, 1000);

        let AdCusDetail = localStorage.getItem('AdCusDetail');
        const UID = localStorage.getItem('UDID');
        this.setState({UDID:UID})
        if (AdCusDetail != null) {
            let checkoutList = JSON.parse(AdCusDetail) && JSON.parse(AdCusDetail).Content;            
            if(checkoutList!=null){
                this.setState({
                    user_fname:checkoutList.FirstName?checkoutList.FirstName:'',
                    user_lname:checkoutList.LastName,
                    user_address:checkoutList.StreetAddress,
                    user_city:checkoutList.city_name,
                    user_contact:checkoutList.Phone,
                    user_notes:checkoutList.Notes,
                    user_email:checkoutList.Email,
                    user_pcode:checkoutList.Pincode,
                    user_id:checkoutList.Id,               
                })
            }
        }                          
    }

    // its set the order payments
    setOrderPartialPayments(paying_amount, payment_type){      
        let order_id = 0;
        if(typeof(Storage) !== "undefined") {   

            if ( localStorage.oliver_order_payments ) { 
                var payments = JSON.parse(localStorage.getItem("oliver_order_payments") );
                payments.push({
                    "Id"             : 0,
                    "payment_type"   : payment_type,
                    // "payment_amount" : paying_amount.replace(/\s+/g, ''), by shakun
                    "payment_amount" : String(paying_amount).replace(',', ''),
                    "order_id"       : order_id,
                });
                localStorage.setItem("oliver_order_payments", JSON.stringify(payments));                
            } else { 
                var payments = new Array();
                payments.push({
                    "Id"             : 0,
                    "payment_type"   : payment_type,
                    "payment_amount" : String(paying_amount).replace(',', ''),
                    "order_id"       : order_id,
                });
                localStorage.setItem("oliver_order_payments", JSON.stringify(payments));                
            }

        } else {
            alert( "Your browser not support local storage" );
        }

        this.orderCart.getPaymentDetails();        
        this.isOrderPaymentComplete( order_id );        
    }

    // its get the payment from checkout third view
    getOrderPartialPayments(){
        this.orderPayments.setPartialPayment();
    }

    // its check the order paymets complete or not
    isOrderPaymentComplete( order_id ){
        let amountToBePaid = (typeof this.state.checkList !== 'undefined') ? parseFloat(this.state.checkList.totalPrice) : 0;
        let paidPaments = this.getOrderPayments(order_id);
        let paidAmount = 0;
        paidPaments.forEach(paid_payments => {     
            paidAmount += parseFloat(paid_payments.payment_amount);         
        });                
        
        if ( parseFloat(amountToBePaid).toFixed(2) == paidAmount ) { 
            this.createOrder( "completed" );
            this.removeOrderPayments();    
        }
    }

    //its used for get the order payments
    getOrderPayments( order_id ){ 
        if ( localStorage.oliver_order_payments ) {
            let paid_amount = 0;
            var payments = new Array();            

            JSON.parse(localStorage.getItem("oliver_order_payments")).forEach(paid_payments => {
                // if (parseFloat(order_id) == paid_payments.order_id) {                    
                    paid_amount += parseFloat(paid_payments.payment_amount);
                    payments.push({
                        "payment_type"   : paid_payments.payment_type,
                        "payment_amount" : paid_payments.payment_amount,
                    });
                // }                
            });
            
            return payments;
        } else {
            alert( "Your browser not support local storage" );
        }
    }

    //its used to remove the order payments
    removeOrderPayments(){
        if ( localStorage.oliver_order_payments ) {
            localStorage.removeItem("oliver_order_payments");
        } else {
            alert( "Your browser not support local storage" );
        }
    }

    setCalculatorRemainingprice( amount ){
        this.setState({
            set_calculator_remaining_amount : amount,
        })        
    }

    createOrder( status ){ 
        this.setState({
            isShowLoader : true,
        });
        this.setPayment( status );
    }

    openOrderPopup( status ){
        $("#park-sale-and-layaway-modal").modal("show");
        $("#park-sale-and-layaway-modal-title").text( status.replace("_", " ") );
        $("#park-sale-and-layaway-modal").data("status", status);       
    }

    placeParkLayAwayOrder(){
        let status = $("#park-sale-and-layaway-modal").data("status");
        let notes = this.state.set_order_notes;
        let note = $("#park-sale-and-layaway-modal-note").val();
        if ( note.length === 0 ) {
            note = `order ${status}`;
        }
        notes.push(note)
        this.setState({
            set_order_notes : notes
        })
        this.createOrder( status );
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleSubmit(e) {
        e.preventDefault();
        const {UDID,user_id, user_address,user_city,user_contact,user_email,user_fname,user_lname,user_notes,user_pcode } = this.state;
        const { dispatch } = this.props;
        if (UDID,user_id,user_address && user_city && user_contact && user_email && user_fname && user_lname && user_notes && user_pcode) {
            //dispatch(userActions.login(username, password));
           // console.log("suceess",this.props)
            const update = {
                Id: user_id,
                FirstName: user_fname,
                LAstName: user_lname,
                Contact: user_contact,
                startAmount: 0,
                Email: user_email,
                Udid: UDID,
                Notes: user_notes,
                StreetAddress: user_address,
                Pincode: user_pcode,
                City: user_city
            }
            //console.log("update", update)
            customerActions.save(update);
            $(".close").click();
           
        }
    }

    setPayment( get_order_status ){
        const { user_address, user_city, user_contact, user_email, user_fname, user_lname, user_notes, user_pcode, paying_amount, paying_type, checkList, add_note, paid_amount, location_id, UDID, user_id, lay_Away, history } = this.state;         
        const { dispatch } = this.props;
        let checkoutList = checkList && checkList.customerDetail && checkList.customerDetail.Content;
        let chec;         
        let place_order;
        let order_payments = [];
        let complete_order_payments = [];
        let newList = [];
        let totalPrice = checkList && checkList.totalPrice;
        let order_id = (typeof checkList.order_id !== "undefined") ? checkList.order_id : 0;
        let status = get_order_status;         
        let storeCredit = checkoutList ? checkoutList.StoreCredit : 0;
        let paidAmount = 0;

        // let oliver_order_payments = this.getOrderPayments( order_id );
        let oliver_order_payments = JSON.parse(localStorage.getItem("oliver_order_payments"));        

        if ( typeof oliver_order_payments !== "undefined" ) {
            oliver_order_payments.map(items=>{
                paidAmount += parseFloat(items.payment_amount) ;
                order_payments.push({
                    "Id" : (typeof items.Id !== "undefined") ? items.Id : 0,
                    "amount" : items.payment_amount,
                    "payment_type" : "order",
                    "type" : items.payment_type
                })
            }) 
        }

         
        //after_discount
        checkList && checkList.ListItem.map(items => {  
                 
            newList.push({
                line_item_id : items.line_item_id,
                name : items.Title,
                product_id : (typeof items.product_id !== "undefined") ? items.product_id : items.WPID,
                variation_id : items.variation_id,
                quantity : items.quantity,
                subtotal : (typeof items.subtotalPrice !== "undefined") ? items.subtotalPrice : items.Price,
                subtotal_tax : (typeof items.subtotaltax !== "undefined") ? items.subtotaltax : parseFloat(items.Price) * (parseFloat(checkList.TaxRate) / 100),
                //total : (typeof items.totalPrice !== "undefined") ? items.totalPrice : items.Price,
                total : (typeof items.totalPrice !== "undefined") ? items.totalPrice : ( (typeof items.after_discount !== "undefined") ? items.after_discount : items.Price),
                //total_tax : (typeof items.totaltax !== "undefined") ? items.subtotaltax : parseFloat(items.Price) * (parseFloat(checkList.TaxRate) / 100),
                total_tax : (typeof items.totaltax !== "undefined") ? items.subtotaltax : ( (typeof items.after_discount !== "undefined") ? parseFloat(items.after_discount)  * (parseFloat(checkList.TaxRate) / 100) : parseFloat(items.Price) * (parseFloat(checkList.TaxRate) / 100)),                
            })
        })    
        
        

        place_order = {
            order_id : order_id,
            status : status,
            customer_note : user_notes ? user_notes : 'Add Note',
            customer_id : user_id ? user_id : 0,
            order_tax : checkList && checkList.tax ? checkList.tax : 0,
            order_total : totalPrice,
            order_discount : checkList && checkList.discountCalculated ? checkList.discountCalculated : 0,
            tax_id : checkList && checkList.TaxId,
            line_items : newList,

            billing_address: [{
                    first_name : user_fname ? user_fname : '',
                    last_name : user_lname ? user_lname : '',
                    company : "",
                    email : user_email ? user_email : '',
                    phone: user_contact ? user_contact : '',
                    address_1 :user_address ? user_address : '',
                    address_2 : user_address ? user_address : '',
                    city : user_city ? user_city : '',
                    state : "MP",
                    postcode : user_pcode ? user_pcode : '',
                    country : "IN"
            }],

            shipping_address:[{
                first_name : user_fname ? user_fname : '',
                last_name : user_lname ? user_lname : '',
                company : "NMS",
                email : user_email ? user_email : '',
                phone: user_contact ? user_contact : '',
                address_1 :user_address ? user_address : '',
                address_2 : user_address ? user_address : '',
                city : user_city ? user_city : '',
                state : "MP",
                postcode :user_pcode ? user_pcode : '',
                country : "IN"
            }],
            order_custom_fee : [],
            // order_notes: [add_note],
            order_notes: (typeof this.state.set_order_notes !== "undefined") ? this.state.set_order_notes : [],
            order_payments : order_payments,
            order_meta:[{
                manager_id : 1 ,
                location_id : location_id ,
                register_id : 4 ,
                cash_rounding : 0 ,
                refund_cash_rounding : 0
             }],
            Udid:UDID
        }        
        // console.log("place_order deep", place_order);
        if (status.includes("park_sale") || status.includes("lay_away")) {
            dispatch(checkoutActions.save(place_order, 2));
        } else {            
            dispatch(checkoutActions.save(place_order, 1));        
        }
          
    }

    removeCheckOutList(){
        alert("REMOVE CHECKLIST IN CHECKOUT PAGE")
        localStorage.removeItem('CHECKLIST');
        localStorage.removeItem('oliver_order_payments');
        const { dispatch } = this.props;
        dispatch(cartProductActions.addtoCartProduct(null)); 
    }

    finalAdd(){ 
        alert("cash rounding popup");
        // this.props.dispatch(checkoutActions.save(this.state.final_place_order ,1))
    }

    render() {
        const {checkList, paying_amount, paying_type, UDID, user_id, user_address, user_city, user_contact, user_email, user_fname, user_lname,user_notes,user_pcode } = this.state;
    
        return (
            <div>
                <div className="wrapper">
                    <div id="content">
                        {/* its used for show the loader */}
                        { this.state.isShowLoader ? <LoadingModal/> : '' }
                        <CommonHeaderFirst {...this.props} />
                        <div className="inner_content bg-light-white clearfix">
                            <div className="content_wrapper">
                                <CheckoutViewFirst onRef={ref => (this.orderCart = ref)} checkList={checkList} paying_amount={paying_amount} paying_type={paying_type} setCalculatorRemainingprice={this.setCalculatorRemainingprice}/>
                                <div className="col-lg-9 col-sm-8 col-xs-8 p-0">
                                    <div className="pl-95 clearfix">
                                       <CheckoutViewSecond {...this.props} addPayment={this.layAwayPayment} getPayment={this.getPayment} orderPopup={this.openOrderPopup}/>
                                       <CheckoutViewThird onRef={ref => (this.orderPayments = ref)} {...this.props} checkList={checkList} addPayment={this.getPayment} setOrderPartialPayments={this.setOrderPartialPayments} orderPopup={this.openOrderPopup}/>
                                    </div>
                                </div>
                            </div>
                            {/* <div className="mt-4 text-right pr-0" onClick={this.setPayment.bind(this, user_id)} > shakun */}
                            <div className="mt-4 text-right pr-0" onClick={() => this.getOrderPartialPayments()} >
                                <div className="full_height_button bg-blue" style={{ backgroundImage: 'url(assets/img/text_blue.png)' }}>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div id="edit-info" className="modal modal-wide fade full_height_one">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">
                                    <img src="assets/img/delete-icon.png" />
                                </button>
                                <h4 className="modal-title">Edit Info</h4>
                            </div>
                            <div className="modal-body overflowscroll">
                                <form className="clearfix form_editinfo">
                                    <div className="col-sm-6">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <div className="input-group-addon">
                                                    First Name
                                                 </div>
                                                <input className="form-control" value={user_fname?user_fname:''} id="user_fname" name="user_fname" type="text"  onChange={this.handleChange}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <div className="input-group-addon">
                                                    Last Name
                                                  </div>
                                                <input className="form-control" value={user_lname?user_lname:''} id="user_lname" name="user_lname" type="text"  onChange={this.handleChange}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <div className="input-group-addon">
                                                    Email
                                                  </div>
                                                <input className="form-control" value={user_email?user_email:''} id="user_email" name="user_email" type="text"  onChange={this.handleChange}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <div className="input-group-addon">
                                                    Phone Number
                                                 </div>
                                                <input className="form-control" value={user_contact?user_contact:''} id="user_contact" name="user_contact" type="text" onChange={this.handleChange} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <div className="input-group-addon">
                                                    Street Address&nbsp;
                                                 </div>
                                                <input className="form-control" value={user_address?user_address:''} id="user_address" name="user_address" type="text"  onChange={this.handleChange}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <div className="input-group-addon">
                                                    City&nbsp;&nbsp;
                                                 </div>
                                                <input className="form-control" id="user_city" value={user_city?user_city:''} name="user_city" type="text" onChange={this.handleChange} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <div className="input-group-addon">
                                                    Post Code
                                                </div>
                                                <input className="form-control" id="user_pcode" name="user_pcode" value={user_pcode?user_pcode:''} type="text" onChange={this.handleChange}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <div className="input-group-addon">
                                                    Notes
                                                </div>
                                                <textarea className="form-control" id="user_notes" value={user_notes?user_notes:''} name="user_notes" type="text" onChange={this.handleChange}>Testing</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div className="modal-footer p-0">
                                <button type="button" onClick={this.handleSubmit} className="btn btn-primary btn-block h66">SAVE & UPDATE</button>
                            </div>
                        </div>
                    </div>
                </div>

                {/* this modal used for showing park sale note */}
                <div tabIndex="-1" className="modal fade in mt-5 modal-wide addnoctehere" id="park-sale-and-layaway-modal" data-status="park_sale" role="dialog">
                        <div className="modal-dialog">
                            <div className="modal-content">
                                <div className="modal-header bb-0">
                                    <button type="button" className="close opacity-1 mt-1" data-dismiss="modal" aria-hidden="true">
                                        <img src="assets/img/delete-icon.png"/>
                                    </button>
                                    <h1 className="modal-title" id="park-sale-and-layaway-modal-title">Add Note</h1>
                                </div>
                                <div className="modal-body p-0">
                                    <textarea className="form-control modal-txtArea" id="park-sale-and-layaway-modal-note" placeholder="Enter Note Here" ></textarea>
                                </div>
                                <div className="modal-footer p-0 b-0">
                                    <button onClick={() => this.placeParkLayAwayOrder()} type="button" className="btn btn-primary btn-block btn-primary-cus pt-2 pb-2">SAVE & CLOSE</button>
                                </div>
                            </div>
                        </div>
                    </div>
                {/* this modal used for showing park sale note */}
                
               <div id="popup_cash_rounding" className="modal modal-wide modal-wide1 cancle_payment">
                        <div className="modal-dialog" id="dialog-midle-align">
                            <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">
                                    <img src="assets/img/delete-icon.png"/>
                                </button>
                                <h4 className="modal-title">Cash</h4>
                            </div>
                            <div className="modal-body p-0">
                                <h3 className="popup_payment_error_msg" id="popup_cash_rounding_error_msg">
                                   <div className="row font18 mb-3">
                                      <div className="col-sm-6 text-left"> 
                                        Total
                                       <span className="pull-right">: </span>
                                      </div>
                                   <div className="col-sm-6">  <NumberFormat value={checkList && checkList.totalPrice} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} /> </div>
                                </div>
                                <hr/>
                                <div className="row font18 mb-3" style={{display:'none'}}>
                                    <div className="col-sm-6 text-left">Payment (Cash Rounding)
                                       <span className="pull-right">:</span>
                                    </div>
                                   <div className="col-sm-6">{0.05}</div>
                                </div>
                                <div className="row font18">
                                 <div className="col-sm-6 text-left">Payment (Cash) 
                                    <span className="pull-right">:</span>
                                    </div>
                                  <div className="col-sm-6">{paying_amount}</div>
                                 </div> <hr/><div className="row font18">
                                <div className="col-sm-6 text-left">Balance 
                                <span className="pull-right">:</span>
                                </div>
                                <div className="col-sm-6" id="balance_left"> <NumberFormat value='0.00' displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} /></div>
                                </div>
                                </h3>
                            </div>
                            <div className="modal-footer" style={{borderTop:0}} onClick={()=>this.finalAdd()}>
                                  <button type="button" className="btn btn-primary btn-block h66" style={{height: 70}} data-dismiss="modal" aria-hidden="true" id="popup_cash_rounding_button">Add Payment</button>
                                </div>
                            </div>
                        </div>
                   </div>
                   <div id="StoreCredit" className="modal modal-wide modal-wide1 fade">
                    <div className="modal-dialog" id="dialog-midle-align">
                        <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal" aria-hidden="true">
                            <img src="http://netmagnetic.co.in/wp2/wp-content/plugins/pos/resources/img/delete-icon.png"/>
                            </button>
                            <h4 className="error_model_title modal-title" id="epos_error_model_title">Message</h4>
                        </div>
                        <div className="modal-body p-0">
                             <h3 id="epos_error_model_message" className="popup_payment_error_msg">insufficiant balance please add another way add money
                           </h3>
                        </div>
                        <div className="modal-footer p-0">
                            <button type="button" className="btn btn-primary btn-block h66" data-dismiss="modal" aria-hidden="true">OK</button>
                        </div>
                        </div>
                    </div>
                    </div>

            </div>
        );
    }
}

function mapStateToProps(state,props) {
    const { checkoutlist ,checkout_list } = state;
    return {
        checkoutlist,
        checkout_list:checkout_list.items
    };
}
const connectedCheckoutView = connect(mapStateToProps)(CheckoutView);
export { connectedCheckoutView as CheckoutView };