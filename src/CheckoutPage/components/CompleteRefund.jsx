import React from 'react';
import { connect } from 'react-redux';
import { cartProductActions } from '../../_actions'
import  moment  from 'moment';


const  PrintElem = (checkList,orderList,inovice_Id) =>
{
    //console.log("elemprint",data, getPdfdateTime);
    var mywindow = window.open('', 'my div', 'height=400,width=600');
    mywindow.document.write(`<html><head><meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Dashboard - Oliver</title>
    
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
          <style>
     @media screen {
          @font-face {
            font-family: 'Lato';
            font-style: normal;
            font-weight: 400;
            src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
          }

    body {
            font-family: "Lato", "Lucida Grande", "Lucida Sans Unicode", Tahoma, Sans-Serif;
          }
        }
   
    </style></title>`);
    /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
    mywindow.document.write(`</head><body>`);
    mywindow.document.write(
     ` 
       <div class="printer80" style="width: 100%; display: inline-block; color: #000 ; line-height: normal;">
           <div class="panel panel-default" style="box-shadow: 0 1px 1px rgba(0,0,0,.05); border-radius: 4px;border: 1px solid transparent;background-color: #fff;margin-bottom: 16px;border-color: #ddd;padding: 0px;">
             <div class="panel-heading" style="color: #333;border-color: #ddd;padding: 5px;border-bottom: 1px solid transparent;border-top-left-radius: 3px;
                      border-top-right-radius: 3px;">
               <!-- <p style="margin: 0px; display: inline-block; text-align: center; margin: auto; line-height: 58px;">
                 <img src="img/oliver.png" style="width: 70px; float: left; ">
                 <strong><i></i></strong>
               </p> -->
                <!--  <div style="display: block; text-align: center;">*****************</div> -->
                 <address 
                   style="margin: 0px;padding: 2px 5px 0px;letter-spacing: 0.6px;text-align: center;line-height: normal;font-size: 15px;text-align: center; color: #000;">
                     <!-- <a href="#" style="color: #000; margin-bottom: 2px; line-height: normal;">wptest.creativemaple.com</a><br> -->
                     ${checkList && checkList.customerDetail &&  checkList.customerDetail.Content &&  checkList.customerDetail.Content.FirstName +' '+ checkList.customerDetail.Content.LastName}
                          <p>Oliver POS Oliver POS<p/>
                     <p>St. John's, Newfoundland and Labrador, A1B1G1</p>
                 </address>
                 <div class="clearfix"></div>
            </div>
            <div style="width: 100%; margin: auto; text-align: center; display: block; overflow: hidden; color: #000;">---------------------------------------------------</div>
             <div class="panel-body">
                 <div style="display:inline-block;width: 100%;">
                   <div style="padding-left: 10px;">
                     <h2 style="text-align: left; margin-top: 10px; margin-bottom: 0px;font-size: 17px;margin-bottom: 0px;  ">Reciept / Tax Invoice </h2>
                      <div style="display: inline-block; float: left; text-align: left;">
                       <p style="margin: 2px 0px 0px;color: #000;letter-spacing: 1.2px; font-size: 14px; width:100%">
                            Invoice: #${inovice_Id?inovice_Id:checkList.order_id}
                         <br>
                       </p>
                     </div>
                     <div  style="display: inline-block; float: right; text-align: left; width:100%">
                       <p style="margin: 2px 0px 0px;color: #000;letter-spacing: 1.2px; font-size: 14px; width:100%"> Time: ${checkList.order_date?checkList.order_date:moment().format('DD ,MMMM YYYY, h:mm a')} </p>
                     </div>
                     <div style="display: inline-block; float: left; text-align: left;">
                       <p style="margin: 2px 0px 0px;color: #000;letter-spacing: 1.2px; font-size: 14px width:100%;">
                         Register: "Oliver POS Register"
                       </p>
                     </div>
                  </div>
                 </div>   
                 <table class="table table-printer-price" style="width: 100%; padding-top: 5px; padding-bottom: 5px;border-collapse: separate;
                   border-spacing: 0px; margin-top: 10px;">
                    <tbody>
                    ${checkList && checkList.ListItem.map((item, index)=>
                        `  <tr key=${index}>
                             <td style=" text-align: left; border-bottom: 1px dotted #000 ;color:#000; padding: 9px;">${item.qunatity?item.qunatity:index + 1}</td>
                             <td style="text-align: left;  border-bottom: 1px dotted #000 ;color:#000; padding: 9px;">${item.Title}</td>
                             <td style="text-align: right;  border-bottom: 1px dotted #000 ;color:#000; padding: 9px;">${item.Price}</td>
                          </tr>`
                          )}
                    </tbody> 
               </table>
               <div class="p-0">
                   <div class="widget" style="background-color: #fff;padding: 2px;border-radius: 0px;margin: 5px 0px;padding: 0px 10px; ">
                    <div class="summary-block" style="">
                    <div class="summary-content" style="padding: 2px 0px;text-align: left;     border-bottom: 1px dotted #000;">
                        <div class="summary-head" style="display: inline-block;text-align: left;"> 
                        <h5 class="summary-title" style="font-size: 16px;color: #1c1e22;margin: 0px; padding:5px 0px;">Subtotal</h5>
                        </div>
                        <div class="summary-price" style="display: inline-block; float: right; ">
                            <p class="summary-text" style="color: #000;font-size: 16px;font-weight: 400;letter-spacing: -1px;margin: 0px;">${checkList.subTotal}</p>
                            <!-- <span class="summary-small-text pull-right"></span> -->
                        </div>
                    </div>
                </div> 
                   <div class="summary-block" style="">
                           <div class="summary-content" style="padding: 2px 0px;text-align: left;     border-bottom: 1px dotted #000;">
                              <div class="summary-head" style="display: inline-block;text-align: left;"> 
                               <h5 class="summary-title" style="font-size: 16px;color: #1c1e22;margin: 0px; padding:5px 0px;">Tax</h5>
                               </div>
                               <div class="summary-price" style="display: inline-block; float: right; ">
                                   <p class="summary-text" style="color: #000;font-size: 16px;font-weight: 400;letter-spacing: -1px;margin: 0px;">${checkList.tax}</p>
                                   <!-- <span class="summary-small-text pull-right"></span> -->
                               </div>
                           </div>
                       </div>
                       <div class="summary-block">
                           <div class="summary-content" style="padding: 2px 0px;text-align: left;     border-bottom: 1px dotted #000;">
                              <div class="summary-head" style="display: inline-block; text-align: left;"> 
                               <h5 class="summary-title" style="font-size: 16px; color: #1c1e22; margin: 0px; padding:5px 0px;">Total</h5>
                               </div>
                               <div class="summary-price" style="display: inline-block; float: right; ">
                                   <p class="summary-text" style="color: #000; font-size: 16px; font-weight: 400; letter-spacing: -1px; margin: 0px; ">${checkList.totalPrice}</p>
                                   <!-- <span class="summary-small-text pull-right"></span> -->
                               </div>
                           </div>
                       </div>
                       <div class="summary-block" style="">
                           <div class="summary-content" style="padding: 2px 0px;text-align: left;     border-bottom: 1px dotted #000;">
                              <div class="summary-head" style="display: inline-block;text-align: left;"> 
                               <h5 class="summary-title" style="font-size: 16px;color: #1c1e22;margin: 0px; padding:5px 0px;">Order Payment's :</h5>
                               </div>
                               <table class="table table-printer-price" style="width: 100%; padding-top: 5px; padding-bottom: 5px;border-collapse: separate;
                               border-spacing: 0px; margin-top: 10px;">
                                <tbody>
                                  ${orderList && orderList.map((item, index)=>
                                    `  <tr key=${index}>
                                         <td style=" text-align: left; border-bottom: 1px dotted #000 ;color:#000; padding: 9px;">${item.order_id}</td>
                                         <td style="text-align: left;  border-bottom: 1px dotted #000 ;color:#000; padding: 9px;">${item.payment_type}</td>
                                         <td style="text-align: right;  border-bottom: 1px dotted #000 ;color:#000; padding: 9px;">${item.payment_amount}</td>
                                      </tr>`
                                      )}
                               </tbody> 
                           </table>
                           </div>
                       </div>
                   </div>
                 
               </div>
                </div>
                <div style="width: 100%; margin: auto; text-align: center; display: block; overflow: hidden;">---------------------------------------------------</div>
                <div class="panel-footer" style="padding: 5px;letter-spacing: 0.6px;text-align: center;line-height: normal;font-size: 15px;text-align: center;color: #000;text-transform: capitalize;">
                 <p style="margin: 0px;">
                   Thank You For Shopping With ${checkList && checkList.customerDetail &&  checkList.customerDetail.Content &&  checkList.customerDetail.Content.FirstName +' '+ checkList.customerDetail.Content.LastName} <br>
                </p>
                <div style="width: 100%; margin: auto; text-align: center; display: block; overflow: hidden;">-----------------------------------------------</div>
                 <p style="margin: 0px;">
                   Thank You<br>  
                   <a href="#" style="padding: 5px;letter-spacing: 0.6px;text-align: center;line-height: normal;font-size: 15px;text-align: center;color: #000;text-transform: capitalize; display: inline-block; margin: 2px 0px;">
                      http:www.myshop.com</a></br>
                      Printer With Oliver POS
                 </p><br><br>
                </div>
           </div>
       </div>`
       
   
);
    mywindow.document.write('</body style="text-align: center;></html>');

    mywindow.print();
    mywindow.close();

    return true;
}

class CompleteRefund extends React.Component {
    constructor(props){
        super(props);
        this.state={
            checkList:null
        }
    }

    componentWillMount(){
        let inovice_Id = localStorage.getItem("ORDER_ID");
        let checkList = JSON.parse(localStorage.getItem('CHECKLIST'));
        let orderList = JSON.parse(localStorage.getItem('oliver_order_payments'))
        // console.log("inovice_Id",inovice_Id)
        // console.log("checkList",checkList)
        // console.log("orderList",orderList)
        // PrintElem(checkList,orderList,inovice_Id)
        this.setState({checkList:checkList})
    }

    clear(){
        localStorage.removeItem('ORDER_ID');
        localStorage.removeItem('CHECKLIST');
        localStorage.removeItem('oliver_order_payments');
        localStorage.removeItem('AdCusDetail');
        const { dispatch } = this.props;
        localStorage.removeItem('CARD_PRODUCT_LIST');
         dispatch(cartProductActions.addtoCartProduct(null));
        window.location='/shopview'
    }

    render() {
        const { checkList } = this.state;
         return (
            <div className="bgcolor1">
                <div className="content_main_wapper">
                    <div className="menu">
                        <div className="aerrow pull-left arrowText">
                            <a href="#">
                                <div className="icon icon-backarrow-lftWhite"></div>
                                Cancel Sale
                    </a>
                        </div>
                        <div className="aerrow pull-right arrowText" onClick={()=>this.clear()}>
                            <a href="#">
                                New Sale
                        <div className="icon icon-backarrow-rgtWhite"></div>
                            </a>
                        </div>
                    </div>
                    <div className="dialog-content">
                        <div className="loginBox" style={{ width: 411 }}>
                            <div className="login-form">
                                <div className="login-logo">
                                    <img src="assets/img/accepted.png" className="" />
                                </div>
                                <h2 className="chooseregister optima">Sale Complete</h2>
                                <form className="login-field" action="#">
                                    <div className="input-group">
                                        <span className="input-group-addon group-addon-custom" id="basic-addon1">Email Receipt</span>
                                        <input type="text" 
                                        value={checkList && checkList.customerDetail && checkList.customerDetail.Content && checkList.customerDetail.Content.Email} 
                                        className="form-control form-control-custom mt-0" placeholder="" aria-describedby="basic-addon1" />
                                    </div>
                                    <div className="checkBox-custom">
                                        <div className="checkbox">
                                            <label className="customcheckbox">Remember Customer
                                                  <input type="checkbox" checked={true}  readOnly={true}/>
                                                <span className="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <button type="button" className="btn btn-login bgcolor2">Sent Email Reciept</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

function mapStateToProps(state) {
    const { shop_order } = state;
    return {
        shop_order:shop_order.items
    };
}
const connectedCompleteRefund = connect(mapStateToProps)(CompleteRefund);
export { connectedCompleteRefund as CompleteRefund };