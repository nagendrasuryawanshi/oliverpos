import { checkoutConstants } from '../constants/checkout.constants'
import { checkoutService } from '../services/checkout.service';
import { history } from '../../_helpers';
import { alertActions } from '../../_actions'

const newList = [];

export const checkoutActions = {
    getAll,
    checkItemList,
    save
   
};

function getAll() {
    return dispatch => {
        dispatch(request());

        checkoutService.getAll()
            .then(
                checkoutlist => dispatch(success(checkoutlist)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: checkoutConstants.GETALL_REQUEST } }
    function success(checkoutlist) { return { type: checkoutConstants.GETALL_SUCCESS, checkoutlist } }
    function failure(error) { return { type: checkoutConstants.GETALL_FAILURE, error } }
}

function checkItemList (checkout_list,){
    //const list = JSON.parse(localStorage.getItem("CHECKLIST"))
    // console.log("CHECKLIST",newList);
    // checkout_list.ListItem && checkout_list.ListItem.map(item=>(
    //     console.log("CHECKLIST",newList.push(item))
    // ))
    //  var list ={
    //     ListItem:newList,
    //     totalPrice:checkout_list.totalPrice,
    //     discountAmountTxt:checkout_list.discountAmountTxt,
    //     tax:checkout_list.tax
    //  }
    // localStorage.setItem("CHECKLIST",JSON.stringify(newList))
    return dispatch => {
         dispatch(success(checkout_list))
         localStorage.removeItem("oliver_order_payments")
         window.location='/checkout';
         //history.push('/checkout')
        };

 function success(checkout_list) { return { type: checkoutConstants.GET_CHECKOUT_LIST_SUCCESS, checkout_list } }
}

function save(shop_order, path) {
//    console.log("order action deepsagar", shop_order, path);
    return dispatch => {
        dispatch(request(shop_order));
        checkoutService.save(shop_order)
            .then(
                shop_order => { 
                    dispatch(success(shop_order));
                    
                    //remove order details from localstorage
                    localStorage.removeItem('CARD_PRODUCT_LIST');
                    localStorage.removeItem('CHECKLIST');
                    localStorage.removeItem('oliver_order_payments');

                    if( path == 1 ){
                        window.location = '/complete_refund';
                    } else {                        
                        window.location = '/shopview';
                    }
                   
                    dispatch(alertActions.success('save custmoer order successfully'));
                   
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request() { return { type: checkoutConstants.INSERT_REQUEST } }
    function success(shop_order) { return { type: checkoutConstants.INSERT_SUCCESS, shop_order } }
    function failure(error) { return { type: checkoutConstants.INSERT_FAILURE, error } }
}



