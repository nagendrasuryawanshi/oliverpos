import { combineReducers } from 'redux';

import { authentication } from './authentication.reducer';
import { registration } from './registration.reducer';
import { users } from './users.reducer';
import { alert } from './alert.reducer';
import { activities ,single_Order_list ,filteredListOrder,mail_success } from '../ActivityPage/reducers/activity.reducer';
import { customerlist , single_cutomer_list ,filteredList} from '../CustomerPage/reducers/customer.reducer';
import { checkoutlist , checkout_list ,shop_order } from '../CheckoutPage/reducers/checkout.reducer';
//import { refundlist } from '../RefundPage/reducers/refund.reducer';
import { cash_reportlist } from '../CashReportPage/reducers/cash_report.reducer';
import { settinglist } from '../SettingPage/reducers/setting.reducer';
import { sitelist } from '../SiteLinkPage/reducers/site_link.reducer';
import { registers } from '../LoginRegisterPage/reducers/register.reducer';
import { locations } from '../LoginLocation/reducers/location.reducer';
import { productlist,attributelist ,filteredProduct } from './allProduct.reducer';
import { cartproductlist , selecteditem } from './cartProduct.reducer';
import { favourites , favouritesChildCategoryList,favouritesSubAttributeList ,variationProductList } from '../ShopView/reducers/favouriteList.reducer';
import { categorylist } from './categoies.reducer';
import { discountlist } from './discount.reducer';
import { refundOrder } from '../RefundPage/reducers/refund.reducer';


import { taxratelist } from './taxrate.reducer';
const rootReducer = combineReducers({
  authentication,
  registration,
  users,
  alert,
  activities,
  customerlist,
  single_cutomer_list,
  checkoutlist,
 // refundlist,
  cash_reportlist,
  settinglist,
  sitelist,
  registers,
  locations,
  productlist,
  attributelist,
 
  favourites, //by deepsagar
  productlist, //by deepsagar
  single_Order_list,
  filteredList,
  categorylist,
  cartproductlist,
  discountlist,
  filteredListOrder,
  filteredProduct,
  mail_success,
  refundOrder,  //by deepsagar
  taxratelist,
  checkout_list,
  favouritesChildCategoryList,
  favouritesSubAttributeList,
  variationProductList,
  shop_order,
  selecteditem,

});

export default rootReducer;