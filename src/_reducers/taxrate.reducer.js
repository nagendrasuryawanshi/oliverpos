import { taxRateConstants  } from '../_constants/taxRate.constants';

export function taxratelist(state = {}, action) {    
    switch (action.type) {        
        case taxRateConstants.GETALL_SUCCESS:
            return {
                taxratelist: action.taxratelist
            };
        case taxRateConstants.GETALL_FAILURE:
            return {
                error: action.error
            };
        case taxRateConstants.GETALL_REQUEST:
            return {
                loading: true
            };
        default:
            return state
    }
}