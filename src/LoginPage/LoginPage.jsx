import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
//import { userActions } from '../_actions';
import { userActions } from './action/user.actions';
import { history } from '../_helpers';
class LoginPage extends React.Component {
    constructor(props) {
        super(props);

        // reset login status
        this.props.dispatch(userActions.logout());

        this.state = {
            username: '',
            password: '',
            submitted: false,
            check:true
        };

        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

      
    }
    componentWillMount(){
        console.log("Login sitelist", localStorage.getItem("sitelist"));

        if(localStorage.getItem("sitelist") &&  localStorage.getItem('UDID') &&  localStorage.getItem('Location') &&   localStorage.getItem('register') )
        history.push('/loginpin');
        // history.push('/site_link');
    }
    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleSubmit(e) {
        e.preventDefault();

        this.setState({ submitted: true });
        const { username, password } = this.state;
        const { dispatch } = this.props;
        if (username && password) {
            dispatch(userActions.login(username, password));
           // window.location = '/site_link'
        }
    }

    checkStatus(value){
        console.log("value",value)
        this.setState({check:value})
    }

    render() {
        const { loggedIn } = this.props;
        const { username, password, submitted ,check} = this.state;
        console.log('loggingIn',loggedIn)
         return (
            <div className="bgimg-1">
            <div className="content_main_wapper">
            {/* <div className="dialog-content"> */}
                <div className="onboarding-loginBox">
                    <div className="login-form">
                        <div className="onboarding-pg-heading">
                            <h1>Login To "ShopName”</h1>
                            <p>Login to the WordPress account <br />associated with “ShopName”</p>
                        </div>
                        <form name="form"  className="onboarding-login-field" onSubmit={this.handleSubmit} >
                            <div className={'input-group m-b-20' + (submitted && !username ? ' has-error' : '')}>
                                <span className="input-group-addon ig-name-custom ig_ni-custom" id="basic-addon1">Email</span>
                                <input type="text"  className="form-control ig-input-custom ig_ni-custom" placeholder="example@gmail.com" aria-describedby="basic-addon1" name="username" value={username} onChange={this.handleChange} />
                               
                            </div>
                            <div className={'input-group m-b-20' + (submitted && !password ? ' has-error' : '')}>
                                <span className="input-group-addon ig-name-custom ig_ni-custom" id="basic-addon1">Password</span>
                                <input type="password"  className="form-control ig-input-custom ig_ni-custom" placeholder="password" aria-describedby="basic-addon1"  name="password" value={password} onChange={this.handleChange}/>
                           </div>

                            <div className="checkBox-custom m-b-20">
                                <div className="checkbox">
                                    <label className="customcheckbox">Remember Me
                                        <input type="checkbox" onClick={()=>this.checkStatus(!check)} checked="checked" readOnly={true} />
                                        <span className={'checkmark '+(check==true?' checkUncheck':' ')}></span>
                                    </label>
                                </div>
                            </div>
                            <button type="submit" className="btn btn-login bgcolor2 mt-0"  >Login</button>
                        </form>
                    </div>
                    {
                        submitted && !username && !password ?(
                       <div className="validationErr">Invalid Credentials</div>
                     )
                     :

                     submitted && !username ?(
                      <div className="validationErr">Username is required</div>
                    
                     )
                     :
                    submitted && !password ?(
                       <div className="validationErr">Password is required</div>
                    )
                    :
                     loggedIn == 'undefined'?(
                      <div className="validationErr">Invalid Credentials</div>
                    )
                    :
                    <div className="validationErr"></div>
                    }
                </div>
                <div className="powered-by-oliver">
                    <a href="javascript:void(0)">Powered by Oliver POS</a>
                </div>
            </div>
        </div>
        );
    }
}

function mapStateToProps(state) {
    const { loggedIn } = state.authentication;
    return {
        loggedIn
    };
}

const connectedLoginPage = connect(mapStateToProps)(LoginPage);
export { connectedLoginPage as LoginPage }; 