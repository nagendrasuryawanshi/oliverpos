import React from 'react';
import { connect } from 'react-redux';
import { registerActions } from '../actions/register.action';
import { history } from "../../_helpers";
class LoginRegisterView extends React.Component {
    constructor(props) {
        super(props);
       // var {dispatch}=this.props;
         // dispatch(registerActions.getAll());
        //   $(".chooseregisterLinks").niceScroll({styler: "fb",cursorcolor: "#2BB6E2",cursorwidth: '3',cursorborderradius: '10px',background: '#d5d5d5',spacebarenabled: false,cursorborder: '',scrollspeed: 60,cursorfixedheight: 70});   
      
    }
    componentWillMount(){
        if(localStorage.getItem("sitelist") &&  localStorage.getItem('UDID') &&  localStorage.getItem('Location')&&  localStorage.getItem('register'))
         history.push('/loginpin');

         const {dispatch}=this.props;
         dispatch(registerActions.getAll());
         $(".chooseregisterLinks").niceScroll({styler: "fb",cursorcolor: "#2BB6E2",cursorwidth: '3',cursorborderradius: '10px',background: '#d5d5d5',spacebarenabled: false,cursorborder: '',scrollspeed: 60,cursorfixedheight: 70});   
    }
    handleSubmit(Id) {      
        localStorage.setItem('register', Id);       
        history.push('/loginpin');
        
    }
    windowLocation(location) {
        window.location = location
    }
     componentDidMount(){
    //      const {dispatch}=this.props;
    //    dispatch(registerActions.getAll());
        $(".chooseregisterLinks").niceScroll({styler: "fb",cursorcolor: "#2BB6E2",cursorwidth: '3',cursorborderradius: '10px',background: '#d5d5d5',spacebarenabled: false,cursorborder: '',scrollspeed: 60,cursorfixedheight: 70});   
     }

    render() {
        var {registers} = this.props.registers;
        console.log("Registers",registers);
       return (
        <div className="bgimg-1">
        <div className="content_main_wapper">
        <a href="/login" className="aonboardingbackicon">
              <button className="button onboardingbackicon">&nbsp;</button>&nbsp; Go Back
          </a>
  
        
              <div className="onboarding-loginBox">
              <div className="login-form">
                   <div className="onboarding-pg-heading">
                      <h1>Choose Your Register</h1>
                      <p>
                      You’ve set up the following registers online. Please choose the register to start working on.
                      </p>
                  </div>
                  <form className="onboarding-login-field" action="#">
                    <ul className="list-group chooseregisterLinks m-b-20">
                    {
                       
                       registers && registers.map((item, index) => {
                                    return (
                                        // <li  key={index}>{item.Name}</li>
                                        <li key={index} className="list-group-item" onClick={()=>this.handleSubmit(item.Id)} ><a href="#">{item.Name}<img src="assets/img/green-arrow.png"/></a></li>
                                    )
                                })
                              
                                } 
                       
                     </ul>
                      <div className="checkBox-custom m-b-20">
                         <div className="checkbox">
                             <label className="customcheckbox">Remember location
                               <input type="checkbox" checked="checked" readOnly={true}/>
                               <span className="checkmark"></span>
                             </label>
                         </div>
                     </div> 
               </form>
              </div>
          </div>
                <div className="powered-by-oliver">
                    <a href="javascript:void(0)">Powered by Oliver POS</a>
                </div>
        
      </div>
      </div>



        );
    }
}

function mapStateToProps(state) {
    const { registers } = state;
    return {
        registers
    };
}

const connectedLoginLoginRegisterView = connect(mapStateToProps)(LoginRegisterView);
export { connectedLoginLoginRegisterView as LoginRegisterView };