import { registerConstants } from "../constants/register.constants";
import { registerService } from "../services/register.service";

export const registerActions = {
  
  getAll,
 
  };

  function getAll() {
    return dispatch => {
    //  dispatch(request());
  
      registerService
        .getAll()
        .then(
          registers => dispatch(success(registers)),
  
          error => dispatch(failure(error.toString()))
        );
    };
  
    function request() {
      return { type: registerConstants.GETALL_REQUEST };
    }
    function success(registers) {
      return { type: registerConstants.GETALL_SUCCESS, registers };
    }
    function failure(error) {
      return { type: registerConstants.GETALL_FAILURE, error };
    }
  }