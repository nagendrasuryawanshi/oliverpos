import Config from '../../Config'

export const registerService = {    
    getAll,  
  };

  const API_URL=Config.key.OP_API_URL

  function getAll() {    
    const requestOptions = {
      method: 'GET',           
    //  mode: 'no-cors',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    }
  };

  console.log("UDID",localStorage.getItem('UDID'));
  console.log("Location",localStorage.getItem('Location'));

  //return fetch(`${config.apiUrl}/users/authenticate`, requestOptions)
  return fetch(`${API_URL}/ShopAccess/GetRegisters?Udid=`+localStorage.getItem('UDID')+'&LocId='+localStorage.getItem('Location'), requestOptions)
      .then(handleResponse)
      .then(registers => {
              console.log("Register Response",registers.Content); 
              let _registers=registers.Content;
              return _registers;
        
          
      });
  }
  
function handleResponse(response) {
  return response.text().then(text => {
      const data = text && JSON.parse(text);
      if (!response.ok) {
          if (response.status === 401) {
              // auto logout if 401 response returned from api
              logout();
              location.reload(true);
          }

          const error = (data && data.message) || response.statusText;
          return Promise.reject(error);
      }

      return data;
  });
}