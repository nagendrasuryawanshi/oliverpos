import React from 'react';
import { connect } from 'react-redux';
import { refundActions } from '../actions/refund.action';


class RefundViewThird extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            refundingAmount : 0,
            refundPayments : 0,
            emptyPaymentFlag : true,
            refundingPaidAmount : 0,
        };

        this.setRefundPayment = this.setRefundPayment.bind(this);
        this.calcInp = this.calcInp.bind(this);
    }

    componentDidMount() {
        this.props.onRef(this)
    }
    
    componentWillUnmount() {
        this.props.onRef(null)
    }

    setRefundPayment( order_id ){
        let paymentType = $('input[name=refund-payments]:checked').val();
        if ( typeof paymentType !== 'undefined' ) {
            let paymentAmount = parseFloat( $("#refund_calc_output").text() );
            if (paymentAmount <= 0) {
                alert("Paying amount should be greater than 0.");
            } else {
                this.props.addPayment( order_id, paymentType, paymentAmount );
                this.setState({
                    emptyPaymentFlag : true,
                });
                return true;
            }
        } else {
            alert("Please select a valid payment mode.");
        }    
        
        return false;
    }

    setRefundAmount( amount ){
        this.setState({
            refundingAmount : amount,
        });
    }

    setrefundingPaidAmount( amount ){ 
        this.setState({
            refundingPaidAmount : amount,
        });
    }

    SetValue(eleT, val) {
        eleT.html(eleT.html().replace(' ', '') + val + ' ');
    }

    calcInp(input) {
        var elemJ = jQuery('#refund_calc_output');   
        if ( this.state.emptyPaymentFlag || elemJ.html() == "0") {
            elemJ.html('');
            this.setState({
                emptyPaymentFlag : false,
            });
        }

        switch (input) {
            case 1:
                this.SetValue(elemJ, '1');
                break;
            case 2:
                this.SetValue(elemJ, '2');
                break;
            case 3:
                this.SetValue(elemJ, '3');
                break;
            case 4:
                this.SetValue(elemJ, '4');
                break;
            case 5:
                this.SetValue(elemJ, '5');
                break;
            case 6:
                this.SetValue(elemJ, '6');
                break;
            case 7:
                this.SetValue(elemJ, '7');
                break;
            case 8:
                this.SetValue(elemJ, '8');
                break;
            case 9:
                this.SetValue(elemJ, '9');
                break;
            case 0:
                this.SetValue(elemJ, '0');
                break;
            case '.':
                if (elemJ.html() == "") {
                        this.SetValue(elemJ, '0');
                } else {
                    if (elemJ.html().includes(".")) {

                    } else {
                        this.SetValue(elemJ, '.');
                    }
                }
                break;
            //alert('n');
            // default code block
        }
    }

    rmvInp() {
        let str = $('#refund_calc_output').html();
        str = str.substring(0, str.length - 2);
        $('#refund_calc_output').html(str + ' ');
        if (str == "" || str == " ") {
            $('#refund_calc_output').html('0');
        } else { }
    }
    render() {
        return (
            <div className="col-lg-6 col-sm-6 col-xs-12 pt-4 plr-8">
                <div className="items preson_info">
                    <div className="item-heading text-center font24">Payment Type</div>
                    <div className="panel panel-default panel-product-list bt-0 p-0 relDiv" id="buttonGroupPanel" style={{ borderColor: '#979797' }}>
                        <div className="p-3 pt-0 clearfix overflowscroll" id="buttonGroup">
                            <form action="#">
                                <div className="w-50-block button_with_checkbox">
                                  <input type="radio" id="cash-payment" value="cash" name="refund-payments"  />
                                    <label htmlFor="cash-payment" className="label_select_button">Cash</label>
                                </div>
                                <div className="w-50-block button_with_checkbox">
                                    <input type="radio" id="card-payment" value="card" name="refund-payments" />
                                    <label htmlFor="card-payment" className="label_select_button">Debit / Credit Card</label>
                                </div>
                                <div className="w-100-block button_with_checkbox">
                                    <input type="radio" id="others-payment" value="other" name="refund-payments" />
                                    <label htmlFor="others-payment" className="label_select_button">others</label>
                                </div>
                                <div className="w-100-block text-center mt-1 notes">
                                    <div className="line-copy"></div>
                                    add customer to use follwing for transaction
                                            </div>
                                <div className="w-100-block button_with_checkbox">
                                    <input type="radio" id="store-credit-payment" value='store-credit' name="refund-payments" />
                                    <label htmlFor="store-credit-payment" className="label_select_button">Store Credit</label>
                                </div>
                            </form>
                        </div>
                        <div className="panel panelCalculator">
                            <div className="panel-body p-0">
                                <table className="table table table-bordered calculatorBlockTable">
                                    <tbody>
                                        <tr>
                                            <td colSpan="2" className="text-right br-1">
                                                <p className="total_checkout_val" id="refund_calc_output">{ parseFloat(this.state.refundingAmount - this.state.refundingPaidAmount).toFixed(2) }</p>
                                            </td>
                                            <td className="text-center pointer" onClick={() => this.rmvInp()}>
                                                <button className="button icon icon-backarrow"></button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td className="td-calc-padding br-1 p-0"><button type="button" onClick={() => this.calcInp(1)} className="btn btn-default calculate">1</button></td>
                                            <td className="td-calc-padding br-1 p-0"><button type="button" onClick={() => this.calcInp(2)} className="btn btn-default calculate">2</button></td>
                                            <td className="td-calc-padding p-0"><button type="button" onClick={() => this.calcInp(3)} className="btn btn-default calculate">3</button></td>
                                        </tr>
                                        <tr>
                                            <td className="td-calc-padding br-1 p-0"><button type="button" onClick={() => this.calcInp(4)} className="btn btn-default calculate">4</button></td>
                                            <td className="td-calc-padding br-1 p-0"><button type="button" onClick={() => this.calcInp(5)} className="btn btn-default calculate">5</button></td>
                                            <td className="td-calc-padding p-0"><button type="button" onClick={() => this.calcInp(6)} className="btn btn-default calculate">6</button></td>
                                        </tr>
                                        <tr>
                                            <td className="td-calc-padding br-1 p-0"><button type="button" onClick={() => this.calcInp(7)} className="btn btn-default calculate">7</button></td>
                                            <td className="td-calc-padding br-1 p-0"><button type="button" onClick={() => this.calcInp(8)} className="btn btn-default calculate">8</button></td>
                                            <td className="td-calc-padding p-0"><button type="button" onClick={() => this.calcInp(9)} className="btn btn-default calculate">9</button></td>
                                        </tr>
                                        <tr>
                                            <td className="td-calc-padding br-1" onClick={() => this.minplus()}>
                                                <button className="icon icon-plus button"></button>
                                                <button className="icon icon-minus button"></button>
                                            </td>
                                            <td className="td-calc-padding br-1 p-0"><button type="button" onClick={() => this.calcInp('.')} className="btn btn-default calculate">,</button></td>
                                            <td className="td-calc-padding p-0"><button type="button" onClick={() => this.calcInp(0)} className="btn btn-default calculate">0</button></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>

            
        )
    }

}

function mapStateToProps(state) {
    const { refundlist } = state;
    return {
        refundlist
    };
}

const connectedRefundViewThird = connect(mapStateToProps, refundActions)(RefundViewThird);
export { connectedRefundViewThird as RefundViewThird };