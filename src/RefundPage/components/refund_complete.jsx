import React from 'react';
import { connect } from 'react-redux';
import { cartProductActions } from '../../_actions'
import { history } from '../../_helpers';


class RefundComplete extends React.Component {
    constructor(props){
        super(props);
        this.goToShopview = this.goToShopview.bind(this);
        console.log("deep history", history);
    }

    goToShopview(){
        history.push('/shopview');
    }


    render() {
         return (
            <div className="bgcolor1">
                <div className="content_main_wapper">
                    <div className="menu">
                        <div className="aerrow pull-left arrowText">
                            <a href="javascript:void(0)">
                                <div className="icon icon-backarrow-lftWhite"></div>
                                Cancel Sale
                    </a>
                        </div>
                        <div className="aerrow pull-right arrowText">
                            <a href="javascript:void(0)" onClick={() => this.goToShopview()}>
                                New Sale
                        <div className="icon icon-backarrow-rgtWhite"></div>
                            </a>
                        </div>
                    </div>
                    <div className="dialog-content">
                        <div className="loginBox" style={{ width: 411 }}>
                            <div className="login-form">
                                <div className="login-logo">
                                    <img src="assets/img/accepted.png" className="" />
                                </div>
                                <h2 className="chooseregister optima">Refund Complete</h2>
                                <form className="login-field" action="#">
                                    <div className="input-group">
                                        <span className="input-group-addon group-addon-custom" id="basic-addon1">Email Receipt</span>
                                        <input type="text" value={ (history.location.state.customer_email) ? history.location.state.customer_email : ""} className="form-control form-control-custom mt-0" placeholder="Customer Email" aria-describedby="basic-addon1" />
                                    </div>
                                    <div className="checkBox-custom">
                                        <div className="checkbox">
                                            <label className="customcheckbox">Remember Customer
                                                  <input type="checkbox" checked={true}  readOnly={true}/>
                                                <span className="checkmark"></span>
                                            </label>
                                        </div>
                                    </div>
                                    <button type="button" className="btn btn-login bgcolor2">Sent Email Reciept</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }

}

function mapStateToProps(state) {
    const { shop_order } = state;
    return {
        shop_order:shop_order.items
    };
}
const connectedRefundComplete = connect(mapStateToProps)(RefundComplete);
export { connectedRefundComplete as RefundComplete };