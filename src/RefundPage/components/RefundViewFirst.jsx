import React from 'react';
import { connect } from 'react-redux';
import { refundActions } from '../actions/refund.action';
import { default as NumberFormat } from 'react-number-format'

class RefundViewFirst extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            refund_subtotal : 0,
            refund_tax : 0,
            refund_total : 0,
        }

       this.handleRefundingItems = this.handleRefundingItems.bind(this);
    }

    componentDidMount() {
        this.props.onRef(this)
    }
    
    componentWillUnmount() {
        this.props.onRef(null)
    }

    handleRefundingItems(){
        let refund_subtotal = 0;
        let refund_tax = 0;
        let refund_total = 0;

        $( ".refunndingItem" ).each(function() { 
            let item_id = $(this).attr('data-id'); 
            let quantity =  parseInt( $(`#counter_show_${item_id}`).text() ) ;
            let price = parseFloat( $(`#refunditem_${item_id}`).attr('data-amount') );
            let tax = parseFloat( $(`#refunditem_${item_id}`).attr('data-perprdtax') );  

            //calculation part
            refund_subtotal += ( parseFloat( price ) * quantity);
            refund_tax += ( parseFloat( tax ) * quantity);
            refund_total += ( refund_subtotal + refund_tax );
            //calculation part           
        });

        this.props.refundingAmount( refund_total );
        this.setState({
            refund_subtotal : refund_subtotal,
            refund_tax : refund_tax,
            refund_total : refund_total,
        });
    }

    toggleRefundPanel( item_id ){
        $(`#refundpanel_${item_id}`).toggle();
        $(`#refunditem_${item_id}`).toggleClass('refunndingItem');
        this.handleRefundingItems();
    }

    increaseitemRefundQty( item_id ){
        let count = parseFloat( $(`#counter_show_${item_id}`).text() );
        let maxQty = parseFloat( $(`#check_counter_${item_id}`).text() );
        if ( count < maxQty ) {
            count++;
            $(`#counter_show_${item_id}`).text(count);
            this.handleRefundingItems();
        }
    }

    decreaseitemRefundQty( item_id ){
        let count = parseFloat( $(`#counter_show_${item_id}`).text() );
        if ( count > 1 ) {
            count--;
            $(`#counter_show_${item_id}`).text(count);
            this.handleRefundingItems();
        }
    }

    refund( order_id ){
        
        let refund_subtotal = 0;
        let refund_tax = 0;
        let refund_total = 0;
        var items = new Array();
        var payments = new Array();

        $( ".refunndingItem" ).each(function() { 
            let item_id = $(this).attr('data-id'); 
            let quantity =  parseInt( $(`#counter_show_${item_id}`).text() ) ;
            let price = parseFloat( $(`#refunditem_${item_id}`).attr('data-amount') );
            let tax = parseFloat( $(`#refunditem_${item_id}`).attr('data-perprdtax') );  

            //create the refund items array
            items.push({
                'Quantity'  : quantity,
                'amount'    : price * quantity,
                'tax'       : tax * quantity,
                'item_id'   : item_id,
            });

            //calculation part
            refund_subtotal += ( parseFloat( price ) * quantity);
            refund_tax += ( parseFloat( tax ) * quantity);
            refund_total += ( refund_subtotal + refund_tax );
            //calculation part    
        });


        if ( localStorage.oliver_refund_order_payments ) {  
            var date = new Date();

            JSON.parse(localStorage.getItem("oliver_refund_order_payments")).forEach(paid_payments => {
                if (order_id == paid_payments.order_id) {                    
                    payments.push({
                        'amount'         : paid_payments.payment_amount,
                        'payment_type'   : paid_payments.payment_type,
                        'type'           : paid_payments.payment_type,
                        'payment_date'   : `${date.getDate()}-${date.getMonth()+1}-${date.getFullYear()}`,
                    });
                }                
            });

        }

        let requestData = {
            'order_id'                  : order_id,
            'refund_tax'                : refund_tax,
            'refund_amount'             : refund_total,
            'RefundItems'               : items,
            'order_refund_payments'     : payments,
            'udid'                      : localStorage.getItem('UDID'),
            'customer_email'            : $("#hidden_customer_email_id").text(),            
        }

        // console.log("refund requestData", requestData);
        //call the action function        
        this.props.dispatch(refundActions.refundOrder( requestData ) );
    }

    render() {          
        const { single_Order_list } = this.props;
        let getorder = single_Order_list.items.Content;      

        return (
            <div className="col-lg-3 col-sm-4 col-xs-4 pl-0 plr-8">
            
                <div className="panel panel-default panel-right-side r0 br-1 bb-0">
                    <div className="panel-heading bg-white">
                        { (getorder.orderCustomerInfo != null) ? getorder.orderCustomerInfo.customer_name : "---" }
                        <p style={{display : "none"}} id="hidden_customer_email_id">{ (getorder.orderCustomerInfo != null) ? getorder.orderCustomerInfo.customer_email : "" }</p>
                    </div>
                   <div className="panel-body p-0 overflowscroll bg-white" id="cart_product_list">
                        <div className="table-responsive">
                            <table className="table ListViewCartProductTable">
                                <colgroup>
                                    <col width="10" />
                                    <col width="*" />
                                    <col width="*" />
                                    <col width="*" />
                                </colgroup>
                                <tbody>
                               
                                    {/* display all order items here */}
                                    {
                                      getorder.line_items.map((item,index)=> {
                                          return( 
                                            <tr key={index}>
                                             <td>
                                              <table className="table mb-0">
                                              <tbody>
                                                <tr key={item.line_item_id} id={`refunditem_${item.line_item_id}`} title={item.name} data-id={item.line_item_id} className="prdtr" price={item.total} data-amount={item.total / item.quantity} data-productid={item.product_id} data-varproductid={item.variation_id} data-orderid="6747" data-quantity={item.quantity} data-taxrate={item.total_tax} data-perprdtax={item.total_tax / item.quantity}>
                                                    <td> {item.quantity + item.quantity_refunded} </td>
                                                    <td>
                                                        {item.name}
                                                        <span className="comman_subtitle"></span>
                                                    </td>
                                                    <td align="right">
                                                        <NumberFormat value={item.total - item.amount_refunded} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />
                                                    </td>
                                                    <td> 
                                                        <div className="refund_radio_button" data-id={item.line_item_id}>
                                                            <label className="customcheckbox">
                                                                <input type="checkbox" onChange={()=>this.toggleRefundPanel(item.line_item_id)} />
                                                                <span className="checkmark"></span>
                                                               
                                                            </label>
                                                        </div>
                                                    </td>
                                                </tr>

                                                {/* display refund panel */}
                                                <tr key={`panel_${item.line_item_id}`} id={`refundpanel_${item.line_item_id}`} style={{display : "none"}}>
                                                <th colSpan="4" className="bt-0">
                                                    <div className="btn-group btn-group-justified refund-range" role="group" aria-label="">
                                                        <div className="btn-group" role="group">
                                                            <button type="button" className="btn btn-default" onClick={() => this.decreaseitemRefundQty(item.line_item_id)}><img src="assets/img/minus.png" /></button>
                                                        </div>
                                                        <div className="btn-group" role="group">
                                                            <button type="button" className="btn btn-default" id="countable">
                                                                <input className="blank_input" type="hidden" size="25" value="0" id="count" />
                                                                <span id={`counter_show_${item.line_item_id}`}>1</span>
                                                                    /
                                                                <span id={`check_counter_${item.line_item_id}`}>{item.quantity + item.quantity_refunded}</span>
                                                            </button>
                                                        </div>
                                                        <div className="btn-group" role="group">
                                                            <button type="button" className="btn btn-default" onClick={() => this.increaseitemRefundQty(item.line_item_id)}><img src="assets/img/plus.png" /></button>
                                                        </div>
                                                    </div>
                                                </th>
                                                </tr>
                                                {/* display refund panel */}
                                                </tbody>
                                            </table>
                                           </td>
                                          </tr>
                                          )
                                      })
                                    }                          

                                    {/* display all order items here */}


                                </tbody>
                            </table>
                        </div>
                    </div> 
                
                    <div className="panel-footer p-0 bg-white">
                        <div className="table-calculate-price">
                            <table className="table table ShopViewCalculator mb-0">
                                <tbody>
                                    <tr>
                                        <th className="bt-0">Sub-Total</th>
                                        <td align="right">
                                            <span className="value">{this.state.refund_subtotal}</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th> Tax:
                                            <span className="value pull-right"> { parseFloat(this.state.refund_tax).toFixed(2) } </span>
                                        </th>
                                        <th className="bl-1" align="right"> Discount:
                                            <span className="value pull-right" style={{ color: '#46A9D4' }}> N/A </span>
                                        </th>
                                    </tr>
                                    <tr id="paymentTr">
                                        <th>
                                            <div className="relDiv show_payment_box">
                                                <span className="value pointer" style={{ color: '#46A9D4' }} id="totalPayment"> Payments</span>

                                              
                                                <div className="absDiv">
                                                    <div className="payment_box">
                                                        <h1 className="m-0">Payments</h1>
                                                            {
                                                                getorder.order_payments.map(function(payment, index) {
                                                                    return( 
                                                                        <div key={index} className="row">
                                                                            <div className="col-sm-12">
                                                                                <label className="col-sm-6">{payment.type}</label>
                                                                                <div className="col-sm-6">{payment.amount}</div>
                                                                            </div>
                                                                        </div>
                                                                    )
                                                                })
                                                            }
                                                    </div>
                                                </div>
                                            </div>
                                        </th>
                                        <th className="text-right" id="paymentLeft">
                                            {parseFloat( getorder.total_amount ).toFixed(2)}
                                          </th>
                                    </tr>
                                    <tr>
                                        <th colSpan="2" className="p-0">
                                            <button className="btn btn-block btn-primary total_checkout">
                                                <span className="pull-left">Total Refund</span>
                                                <span className="pull-right">{ parseFloat(this.state.refund_total).toFixed(2) }</span>
                                            </button>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
             </div> 
        )
    }

}

function mapStateToProps(state) {
   // console.log("refundfirstState",state)
    const { refundlist } = state;
    return {
        refundlist
    };
}

const connectedRefundViewFirst = connect(mapStateToProps, refundActions)(RefundViewFirst);
export { connectedRefundViewFirst as RefundViewFirst };