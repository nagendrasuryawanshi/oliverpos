import React from 'react';
import { connect } from 'react-redux';
import { CommonHeaderFirst } from '../../_components';
import { RefundViewFirst } from './RefundViewFirst';
import { RefundViewSecond } from './RefundViewSecond';
import { RefundViewThird } from './RefundViewThird';
import { LoadingModal } from '../../_components';
import { history } from '../../_helpers';



class RefundView extends React.Component {
    constructor(props) {
        super(props);

        this.refundAmount = React.createRef();
        //deepsagar code
        let udid = localStorage.getItem('UDID');
        let getOrderId = this.props.location.search;
        let order_id = 0;
        if (typeof getOrderId !== 'undefined') {
            getOrderId = getOrderId.split("=");
            if(typeof getOrderId[1] !== 'undefined') {
                order_id = getOrderId[1];
            }
        }
        

        this.state = {
            globalUDID: udid,
            setOrderId : 0, 
            refundingAmount : 0,
            isShowLoader : false,
        }                     

        this.setRefundingAmount = this.setRefundingAmount.bind(this);
        this.getRefundPayment = this.getRefundPayment.bind(this);
        this.setRefundPayment = this.setRefundPayment.bind(this);
        this.isRefundPaymentComplete = this.isRefundPaymentComplete.bind(this);
        this.setrefundingPaidAmount = this.setrefundingPaidAmount.bind(this);
        this.showLoader = this.showLoader.bind(this);
        this.removeRefundPayments();
        //deepsagar code 
    }


    setRefundingAmount( amount ){
        this.refundpayments.setRefundAmount( amount );
        this.setState({
            refundingAmount : amount
        });   
    }

    getRefundPayment(order_id, type, amount){
        this.setState({
            paying_type : type,
            paying_amount : amount,
        });  
        this.setRefundPayment( order_id, type, amount );
    }

    addPayment( order_id ){
        let payment = this.refundpayments.setRefundPayment( order_id );
    }

    setRefundPayment( order_id, paying_type, paying_amount ){ 
        // let paying_amount = this.state.paying_amount;
        // let paying_type = this.state.paying_type;                
        
        // set order payments in localstorage
        if(typeof(Storage) !== "undefined") {   

            if ( localStorage.oliver_refund_order_payments ) { 
                var payments = JSON.parse(localStorage.getItem("oliver_refund_order_payments") );
                payments.push({
                    "payment_type"   : paying_type,
                    "payment_amount" : paying_amount,
                    "order_id"       : order_id,
                });
                localStorage.setItem("oliver_refund_order_payments", JSON.stringify(payments));                
            } else { 
                var payments = new Array();
                payments.push({
                    "payment_type"   : paying_type,
                    "payment_amount" : paying_amount,
                    "order_id"       : order_id,
                });
                localStorage.setItem("oliver_refund_order_payments", JSON.stringify(payments));                
            }                  
        } else {
            alert( "Your browser not support local storage" );
        }
        // set order payments in localstorage

        this.isRefundPaymentComplete( order_id );
    }

    isRefundPaymentComplete( order_id ){        
        let payments = JSON.parse(localStorage.getItem("oliver_refund_order_payments") );
        let refundAmount = this.state.refundingAmount;
        let paidAmount = 0;

        //get total paid amount
        if ( localStorage.oliver_refund_order_payments ) {         

            JSON.parse(localStorage.getItem("oliver_refund_order_payments")).forEach(paid_payments => {
                if (order_id == paid_payments.order_id) {
                    paidAmount += parseFloat(paid_payments.payment_amount);
                }                
            });
 
        } else {
            alert( "Your browser not support local storage" );
        }

        this.setrefundingPaidAmount( paidAmount );

        if ( parseFloat(refundAmount).toFixed(2) == parseFloat(paidAmount).toFixed(2) ) {
            this.refundcart.refund( order_id );
            this.removeRefundPayments();
            
            this.showLoader()          
        } else if (parseFloat(refundAmount.toFixed(2)) < parseFloat(paidAmount.toFixed(2))) {
            alert("Can't pay more amount then refund amount.");
            this.removeRefundPayments();
            history.push('/activity');
        }
    }

    showLoader(){
        this.setState({
            isShowLoader : true,
        });
    }

    setrefundingPaidAmount( amount ){ 
        this.refundpayments.setrefundingPaidAmount( amount );
    }

    removeRefundPayments(){
        if ( localStorage.oliver_refund_order_payments ) {
            localStorage.removeItem("oliver_refund_order_payments");
        }
    }

    componentWillMount(){
        setTimeout(function(){
 
            //Put All Your Code Here, Which You Want To Execute After Some Delay Time.
            setHeightDesktop();
       
          }, 1000);
    }


    render() {

        // check if Order_list empty
        if (Object.keys( this.props.single_Order_list ).length === 0 && this.props.single_Order_list.constructor === Object) {            
            window.location = '/activity';
        }

        const { single_Order_list } = this.props;
        
        return (
            <div>
                <div className="wrapper">
                    <div id="content">
                        { this.state.isShowLoader ? <LoadingModal/> : '' }
                        {/* <CommonHeaderFirst {...this.props} /> */}
                        <CommonHeaderFirst {...this.props} />
                        <div className="inner_content bg-light-white clearfix">
                            <div className="content_wrapper">
                                <RefundViewFirst onRef={ref => (this.refundcart = ref)} {...this.props} refundingAmount={this.setRefundingAmount} />
                                <div className="col-lg-9 col-sm-8 col-xs-8 p-0">
                                    <div className="pl-95 clearfix">
                                        <RefundViewSecond {...this.props} />
                                        <RefundViewThird onRef={ref => (this.refundpayments = ref)} {...this.props} addPayment={this.getRefundPayment}/>
                                    </div>
                                </div>
                            </div>
                            <div className="mt-4 text-right pr-0" onClick={this.addPayment.bind(this, single_Order_list.items.Content.order_id)}>
                                <div className="full_height_button" style={{ backgroundImage: 'url(assets/img/text_danger.png)' }}>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

function mapStateToProps(state) {
    const { single_Order_list } = state;
    return {
        single_Order_list
    };
}

const connectedRefundView = connect(mapStateToProps)(RefundView);
export { connectedRefundView as RefundView };