import { refundConstants } from '../constant/refund.constants'
import { refundService } from '../services/refund.service';
import { history } from '../../_helpers';


export const refundActions = {
    refundOrder,      
};

function refundOrder( data ) {
    return dispatch => {
        dispatch(request());

        refundService.refundOrder( data )
            .then(
                // refundOrderResponse => dispatch(success( refundOrderResponse )),
                refundOrderResponse => (refundOrderResponse.IsSuccess) ? setTimeout(function() {  history.push('/refund_complete', data); }, 5000) : alert(refundOrderResponse.Message), 
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: refundConstants.REFUND_GETALL_REQUEST } }
    function success( refundOrderResponse ) { return { type: refundConstants.REFUND_GETALL_SUCCESS, refundOrderResponse } }
    function failure( error ) { return { type: refundConstants.REFUND_GETALL_FAILURE, error } }
}



