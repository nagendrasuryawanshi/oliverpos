import React from 'react';
import { connect } from 'react-redux';
//import { categoriesActions}  from '../_actions/categories.action'
import { default as NumberFormat } from 'react-number-format'
import { history } from '../_helpers';
class  SubAttributes extends  React.Component {
          
        constructor(props) {
            super(props);   
            this.state={
                subattributelist:this.props.subattributelist
            }
        }

        loadSubCategory(subcatlist){

            this.setState({          
                subattributelist:subcatlist            
              })        
            console.log("subcategoryList",subcatlist)       
          
          }
        
          Back(){
            history.go();       
          }

        render() {            
            const { subattributelist} = this.state;                   
                console.log("subcategoryList",history)                  
             return (
                <div className="col-lg-9 col-sm-8 col-xs-8 pr-0">
                             <div className="items pt-3">
                               <div className="item-heading text-center">Library</div>
                                <div className="panel panel-default panel-product-list overflowscroll p-0" id="allProductHeight">
                                   <div className="searchDiv" style={{display:'none'}}>
                                         <input type="search" className="form-control nameSearch" placeholder="Search Product / Scan"/>
                                     </div>   
                                    
                                     <div className="pl-4 pr-4 previews_setting">
                                        <a  onClick={()=>this.Back()} className="back-button " id="mainBack">
                                           <img className="pushnext ml-2 mr-3 mCS_img_loaded" src="assets/img/back_modal.png" />
                                          <span>Back</span>
                                      </a>
                                  </div>
                                      
                                             <table className="table ShopProductTable  table-striped table-hover table-borderless paddignI mb-0 font">
                                         <colgroup>
                                             <col style={{width:'*'}}/>
                                                 <col style={{width:40}}/>
                                         </colgroup>
                                           
                
                                <tbody>
                                         
                                        {
                                            (subattributelist && subattributelist.length>0? subattributelist.map((item,index)=>{
                                                 return(
                                                    item.SubAttributes && item.SubAttributes.length>0?  
                                                   <tr key={index} data-toggle="modal" onClick={()=>this.loadSubCategory(item.SubAttributes)}> 
                                                   
                                                         <td>{item.Value?item.Value:'N/A'}</td>
                                                       
                                                         <td  className="text-right"><a> <img src="assets/img/next.png" /></a></td>
                                                     </tr> 
                                                     :
                                                     <tr key={index} data-toggle="modal" > 
                                                     <td>{item.Value?item.Value:'N/A'}</td>
                                                     <td  className="text-right"></td>
                                                 </tr> 
                                                )
                                            })
                                            :
                                            <tr data-toggle="modal"><td>No Data Found</td></tr>)                                        
                                        }
                                         </tbody>
                                         </table>
                                         </div>
                                         </div>
                                         </div>
                                         
            )}
    
    }
    
   // export  default SubAttributes;

    function mapStateToProps(state) {
        console.log("state",state);
        console.log("props",this.props);
     
        const { attributelist } = state.attributelist;    
        
        return {
            attributelist
        };
    }
    
    const connectedList = connect(mapStateToProps)(SubAttributes);
    export { connectedList as SubAttributes };