import React from 'react';
import { connect } from 'react-redux';
import { categoriesActions}  from '../_actions/categories.action'
import { default as NumberFormat } from 'react-number-format'
import { history } from '../_helpers';
import { SubCategories , LoadingModal } from './'


class  Categories extends  React.Component {
          
        constructor(props) {
            super(props);
    
            // reset login status
            this.props.dispatch(categoriesActions.refresh());
            this.state={
                active:false,            
                isLoading: true,             
                subcategorylist:[]
                } 

               const { dispatch } = this.props;  
               console.log("RenderProductList1", this.props);
                dispatch(categoriesActions.getAll());
        }
        // goBack(){
        //     history.push('/listview');
            
        //   }

         loadSubCategory(subcategorylist){
            this.setState({
                active:true,
                subcategorylist:subcategorylist            
              })        
            // this.state.active=true;
             this.state.subcategorylist=subcategorylist;
             console.log("subcategorylist1",subcategorylist);
          }
        render() {
           
                     
           
            const { active ,subcategorylist} = this.state;
            const{categorylist}=this.props;
            console.log("CurrentState",active);
            console.log("categorylist",categorylist);
            console.log("subcategorylist",subcategorylist);
        
             return (                 
                <div>

                        {active==false?
                       
                        <div className="col-lg-9 col-sm-8 col-xs-8 pr-0">
                            {/* {!this.props.categorylist  ?<LoadingModal/>:''}  */}
                    
                                <div className="items pt-3">
                                    <div className="item-heading text-center">Library</div>
                                        <div className="panel panel-default panel-product-list overflowscroll p-0" id="allProductHeight">
                                        <div className="searchDiv" style={{display:'none'}}>
                                                <input type="search" className="form-control nameSearch" placeholder="Search Category / Scan"/>
                                            </div>   
                                            
                                            <div className="pl-4 pr-4 previews_setting">
                                                <a href="/listview" className="back-button " id="mainBack">
                                                <img className="pushnext ml-2 mr-3 mCS_img_loaded" src="assets/img/back_modal.png" />
                                                <span>Back</span>
                                                </a>
                                            </div>
                                            
                                                <table className="table ShopProductTable  table-striped table-hover table-borderless paddignI mb-0 font">
                                                            {/* <colgroup>
                                                                <col style={{width:'*'}}/>
                                                                    <col style={{width:40}}/>
                                                            </colgroup>                                                    */}
                                    
                                                        <tbody>
                                                        {      
                                                            categorylist? //&& categorylist.lenght>0
                                                            (  categorylist.map((item,index)=>{
                                                                    return(                                                                      
                                                                        item.Subcategories && item.Subcategories.length>0?  
                                                                        <tr  key={index} onClick={()=>this.loadSubCategory(item.Subcategories)}>  
                                                                            <td>{item.Value?item.Value:'N/A'}</td>
                                                                            <td  className="text-right"><a> <img src="assets/img/next.png" /></a></td>
                                                                        </tr> 
                                                                        :
                                                                        <tr  key={index} > 
                                                                            <td>{item.Value?item.Value:'N/A'}</td>                                                                    
                                                                            <td  className="text-right"></td>
                                                                        </tr> 
                                                                    )
                                                                })
                                                                )
                                                                :   <tr data-toggle="modal"><td>No Data Found</td><td></td></tr>                                        
                                                            } 
                                                            
                                                    </tbody>
                                                </table>
                                        </div>
                                    </div>
                        </div>           
                    
                    
                   :  <SubCategories subcategorylist={subcategorylist}/>
                          
                    }   
                              
                </div>
                                         
            )}
    
    }
    
    
    function mapStateToProps(state) {
        console.log("state",state);
        console.log("props",this.props);
     
        const { categorylist } = state.categorylist;    
        
        return {
            categorylist
        };
    }
    
    const connectedList = connect(mapStateToProps)(Categories);
    export { connectedList as Categories };