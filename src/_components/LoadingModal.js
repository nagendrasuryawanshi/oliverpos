import React from 'react';


export const LoadingModal = () => {
    return(
           <div className='overlay active' style={{backgroundColor:'rgba(81, 80, 80, .51)',display:'block',zIndex:3}}>
                <img src="assets/img/frame02-03s.gif" style={{border: 'none', marginLeft: '40%',marginTop:'20%', display: 'block',width:150,height:150}} />
             </div>
        
    );
}