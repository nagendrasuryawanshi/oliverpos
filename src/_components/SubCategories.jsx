import React from 'react';
import { connect } from 'react-redux';
import { categoriesActions}  from '../_actions/categories.action'
import { default as NumberFormat } from 'react-number-format'
import { history } from '../_helpers';
class  SubCategories extends  React.Component {
          
        constructor(props) {
            super(props);   
            this.state={
                subcategorylist:this.props.subcategorylist
            }
        }
        loadSubCategory(subcatlist){
            this.setState({          
                subcategorylist:subcatlist            
              })        
            
          
          }
        render() {            
            const { subcategorylist} = this.state;                   
console.log("subcategoryList1234",subcategorylist)                  
             return (
                <div className="col-lg-9 col-sm-8 col-xs-8 pr-0">
                             <div className="items pt-3">
                               <div className="item-heading text-center">Library</div>
                                <div className="panel panel-default panel-product-list overflowscroll p-0" id="allProductHeight">
                                   <div className="searchDiv" style={{display:'none'}}>
                                         <input type="search" className="form-control nameSearch" placeholder="Search Product / Scan"/>
                                     </div>   
                                    
                                     <div className="pl-4 pr-4 previews_setting">
                                                <a href="/listview" className="back-button " id="mainBack">
                                                <img className="pushnext ml-2 mr-3 mCS_img_loaded" src="assets/img/back_modal.png" />
                                                <span>Back</span>
                                                </a>
                                            </div>

                            <table className="table ShopProductTable  table-striped table-hover table-borderless paddignI mb-0 font">
                                         {/* <colgroup>
                                             <col style={{width:'*'}}/>
                                                 <col style={{width:40}}/>
                                         </colgroup> */}
                                                           
                                <tbody>
                                         
                                        {( subcategorylist && subcategorylist.length>0? subcategorylist.map((item,index)=>{
                                                 return(
                                                    item.Subcategories && item.Subcategories.length>0?  
                                                   <tr key={index} data-toggle="modal" onClick={()=>this.loadSubCategory(item.Subcategories)}>                                                   
                                                         <td>{item.Value?item.Value:'N/A'}</td>                                                       
                                                         <td  className="text-right"><a> <img src="assets/img/next.png" /></a></td>
                                                     </tr> 
                                                     : 
                                                     <tr key={index} data-toggle="modal" >                                                    
                                                        <td>{item.Value?item.Value:'N/A'}</td>                                                   
                                                        <td  className="text-right"></td>
                                                    </tr> 
                                                )
                                            })
                                            :<tr data-toggle="modal"><td>No Data Found</td></tr>)                                        
                                        }
                                         </tbody>
                                         </table>
                                         </div>
                                         </div>
                                         </div>
                                         
            )}
    
    }
    
   // export  default SubCategories;

    function mapStateToProps(state) {
        console.log("state",state);
        console.log("props",this.props);
     
        const { categorylist } = state.categorylist;    
        
        return {
            categorylist
        };
    }
    
    const connectedList = connect(mapStateToProps)(SubCategories);
    export { connectedList as SubCategories };