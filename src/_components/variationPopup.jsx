import React from 'react';
import { connect } from 'react-redux';
import { allProductActions}  from '../_actions/allProduct.action'
import { default as NumberFormat } from 'react-number-format'
import { history } from '../_helpers';
class variationPopup extends  React.Component {
          
        constructor(props) {
            super(props);            
        }

        render() {
             return (
                <div id="addProduct" tabIndex="-1" className="modal modal-wide fade full_height_modal">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">
                                    <img src="assets/img/delete-icon.png"/>
                                </button>
                                <h4 className="modal-title">Head Phone 6</h4>
                            </div>
                            <div className="modal-body overflowscroll" id="scroll_mdl_body">
                                <div className="row">
                                    <div className="col-md-3 text-center">
                                        <img src="assets/img/sheos.png" id="prdImg" style={{width: 154}}/>
                                    </div>
                                    <div className="col-md-9">
                                        <div className="col-md-2">
                                            <div className="modal_wide_field">
                                                <input type="text" value="1" className="form-control h_eight70"/>
                                            </div>
                                        </div>
                                        <div className="col-md-4">
                                            <div className="btn-group btn-group-justified" role="group" aria-label="...">
                                                <div className="btn-group" role="group">
                                                    <a onClick={this.incdec('inc')} className="btn btn-default bd-r h_eight70 btn-plus">
                                                        <button className="icon icon-plus button"></button>
                                                    </a>
                                                </div>
                                                <div className="btn-group" role="group">
                                                    <a onClick={this.incdec('dec')} className="btn btn-default h_eight70 btn-plus">
                                                        <button className="icon icon-minus button"></button>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="input-group">
                                                <div className="input-group-btn">
                                                    <button type="button" className="btn btn-default br-0 h_eight70">In Stock</button>
                                                </div>
                                                <div className="modal_wide_field">
                                                    <input type="text" className="form-control h_eight70" readOnly={true} value="12"/>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-12 mt-4">
                                            <div className="input-group">
                                                <div className="input-group-btn">
                                                    <button type="button" className="btn btn-default bd-r h_eight70">Price</button>
                                                </div>
                                                <div className="modal_wide_field">
                                                    <input type="text" readOnly={true} className="form-control bl-0 h_eight70" value="1236,00 $"/>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div className="col-md-12 text-center">
                                    <div className="line-2"></div>
                                    <label className="color mb-2">Colour</label>
                                    <div className="row">
                                        <div className="col-sm-3">
                                            <div className="button_with_checkbox">
                                                <input type="radio" id="a" name="radio-group"/>
                                                <label htmlFor="a" className="label_select_button">Blue</label>
                                            </div>
                                        </div>
                                        <div className="col-sm-3">
                                            <div className="button_with_checkbox">
                                                <input type="radio" id="b" name="radio-group"/>
                                                <label htmlFor="b" className="label_select_button">Black</label>
                                            </div>
                                        </div>
                                        <div className="col-sm-3">
                                            <div className="button_with_checkbox">
                                                <input type="radio" id="c" name="radio-group"/>
                                                <label htmlFor="c" className="label_select_button">Green</label>
                                            </div>
                                        </div>
                                        <div className="col-sm-3">
                                            <div className="button_with_checkbox">
                                                <input type="radio" id="d" name="radio-group"/>
                                                <label htmlFor="d" className="label_select_button">Red</label>
                                            </div>
                                        </div>
                                        <div className="clearfix"></div>
                                    </div>
                                </div>
                                <div className="col-md-12 text-center">
                                    <div className="line-2"></div>
                                    <label className="color mb-2">Size</label>
                                    <div className="row">
                                        <div className="col-sm-3">
                                            <div className="button_with_checkbox">
                                                <input type="radio" id="test4" name="radio-group"/>
                                                <label htmlFor="test4" className="label_select_button">12</label>
                                            </div>
                                        </div>
                                        <div className="col-sm-3">
                                            <div className="button_with_checkbox">
                                                <input type="radio" id="test5" name="radio-group"/>
                                                <label htmlFor="test5" className="label_select_button">24</label>
                                            </div>
                                        </div>
                                        <div className="col-sm-3">
                                            <div className="button_with_checkbox">
                                                <input type="radio" id="test6" name="radio-group"/>
                                                <label htmlFor="test6" className="label_select_button">36</label>
                                            </div>
                                        </div>
                                        <div className="col-sm-3">
                                            <div className="button_with_checkbox">
                                                <input type="radio" id="test7" name="radio-group"/>
                                                <label htmlFor="test7" className="label_select_button">48</label>
                                            </div>
                                        </div>
                                        <div className="col-sm-3">
                                            <div className="button_with_checkbox">
                                                <input type="radio" id="test8" name="radio-group"/>
                                                <label htmlFor="test8" className="label_select_button">60</label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="clearfix"></div>
                                </div>
                            </div>

                            <div className="modal-footer p-0">
                                <button type="button" className="btn btn-primary btn-block h66">Add Product</button>
                            </div>
                        </div>
                    </div>
                </div>
             )
        }
    
    }
    
    
export default variationPopup;