
import React from 'react';
import { connect } from 'react-redux';
import { categoriesActions}  from '../_actions/categories.action'
import { default as NumberFormat } from 'react-number-format'

import { attributesActions } from '../_actions/allAttributes.action'
import { favouriteListActions} from '../ShopView/action/favouritelist.actions'

class  TileModel extends  React.Component {
          
    constructor(props) {
        super(props);
        this.state = {
            activeTile:true,
            active: false,
            subattributelist: [],
            subcategorylist:[],
            item: 0
        }
        console.log("Renderattributelist1", this.props); 
       
        this.props.dispatch(attributesActions.getAll());
       
        this.handleIsVariationProduct = this.handleIsVariationProduct.bind(this);
    }

addToFavourite()
{
    // $("#setFavoriteBtn").click(function(){ 
     var   type=$('input[name=setFavorite]:checked').data('type');
     var  id  =$('input[name=setFavorite]:checked').data('id');
     var slug  =$('input[name=setFavorite]:checked').data('slug');

        console.log("Type",type,id,slug);
       
        if(id && type){
            this.props.dispatch(favouriteListActions.addToFavourite(type,id,slug) );
           
        }

    $(".close").trigger("click")
}

    componentWillMount()
    {
        this.props.dispatch(categoriesActions.getAll());
    }
    handleIsVariationProduct( type, product ){
        if (type == "simple") {
            this.addSimpleProducttoCart( product );
        } else {
            this.addvariableProducttoCart( product );
        }
    }

    addSimpleProducttoCart( product ){
          const { dispatch ,checkout_list,cartproductlist } = this.props; 
        
             var cartlist=cartproductlist?cartproductlist:[];//this.state.cartproductlist;
                cartlist.push(product)           
           
            dispatch(cartProductActions.addtoCartProduct(cartlist));  // this.state.cartproductlist
        
    
    }

    ActiveList(item) {
        // console.log("list",list)
        this.setState({   
            activeTile:false,        
            item: item,
            active: false,           
        })

    }

  
    loadSubAttribute(subattributelist){
        console.log("loadSubAttribute",subattributelist); 
         this.state.activeTile=false;    
          this.state.active=true;    
          this.state.subattributelist=subattributelist ;
        
        this.setState({
            activeTile:false,  
            active:true,
            subattributelist:subattributelist            
          })  
           //  console.log("loadSubAttribute",this.state.subattributelist);
      }   

    loadSubCategory(subcategorylist){
        console.log("loadsubcategorylist",subcategorylist); 
         this.state.activeTile=false;    
          this.state.active=true;    
          this.state.subcategorylist=subcategorylist ;
        
        this.setState({
            activeTile:false,  
            active:true,
            subcategorylist:subcategorylist            
          })  
             console.log("subcategorylist",this.state.subcategorylist);
      } 

      closeTile()
      {
       
            this.state.activeTile=true,
            this.state.active= false,
            this.state.subattributelist= [],
            this.state.item= 0

            this.setState({
                activeTile:true,  
                active:false,
                subcategorylist:[] ,
                item: 0           
              })  
       
        console.log("CloseTile" ,this.state);
      }

    render() {           
        console.log("register",localStorage.getItem('register'));    
            const { active, activeTile, item } = this.state; 
            const { productlist } = this.props.productlist;
            const { attributelist } = this.props.attributelist;
            const {subattributelist} = this.state.subattributelist;
          
            const { categorylist } = this.props.categorylist;
            console.log("active",active); 
            console.log("categorylist",categorylist); 
           console.log("productlist",productlist);
            console.log("attributelist",attributelist)       
            console.log("subattributelist",this.state.subattributelist,this.props.subattributelist)                  
 return (          
         
               
 <div id="tallModal" tabIndex="-1" className="modal modal-wide fade full_height_one">
        <div className="modal-dialog">
            <div className="modal-content">
           
                  <div className="modal-header">
                     <button type="button" className="close" data-dismiss="modal" aria-hidden="true" >
                         <img src="assets/img/delete-icon.png"/>
                     </button>
                     <h4 className="modal-title">Add To Tile</h4>
                 </div>
                 <div className="modal-body pl-0 pr-0 pt-0" id="scroll_mdl_body">
                    <div className="all_product">
                       
                        
                         <div className="overflowscroll" id="scroll_mdl_body">
                          {/* <List /> */}
                         
                {activeTile == true ?
                 
                    <div id="viewTile">
                    <div className="clearfix">
                                        <div className="searchDiv relDiv">
                                            <input className="form-control nameSearch search_customer_input backbutton bb-0 noshadow pl-5" placeholder="Search Product" type="search"/>
                                            <svg className="search_customer_input2" style={{right:24}} width="23" version="1.1" xmlns="http://www.w3.org/2000/svg" height="64" viewBox="0 0 64 64" xmlns="http://www.w3.org/1999/xlink" enableBackground="new 0 0 64 64">
                                                <g>
                                                    <path fill="#C5BFBF" d="M28.941,31.786L0.613,60.114c-0.787,0.787-0.787,2.062,0,2.849c0.393,0.394,0.909,0.59,1.424,0.59   c0.516,0,1.031-0.196,1.424-0.59l28.541-28.541l28.541,28.541c0.394,0.394,0.909,0.59,1.424,0.59c0.515,0,1.031-0.196,1.424-0.59   c0.787-0.787,0.787-2.062,0-2.849L35.064,31.786L63.41,3.438c0.787-0.787,0.787-2.062,0-2.849c-0.787-0.786-2.062-0.786-2.848,0   L32.003,29.15L3.441,0.59c-0.787-0.786-2.061-0.786-2.848,0c-0.787,0.787-0.787,2.062,0,2.849L28.941,31.786z"></path>
                                                </g>
                                            </svg>
                                        </div>
                                    </div>
                                <table className="table ShopProductTable">
                                    <colgroup>
                                        <col style={{ width: '*' }} />
                                        <col style={{ width: 40 }} />
                                    </colgroup>
                                    <tbody>
                                        <tr key='1' onClick={() => this.ActiveList(1)}>
                                            <td>All Items</td>
                                            <td><a><img src="assets/img/next.png" /></a></td>
                                        </tr>
                                        <tr key='2' onClick={() => this.ActiveList(2)}>
                                            <td>Attributes</td>
                                            <td><a><img src="assets/img/next.png" /></a></td>
                                        </tr>
                                        <tr key='3' onClick={() => this.ActiveList(3)}>
                                            <td>Categories</td>
                                            <td><a><img src="assets/img/next.png" /></a></td>
                                        </tr>
                                    </tbody>
                                </table>
                                </div>
                  
                    :
                    (
                        item == 1 ?
                      
                                <div>
                                       <div className="clearfix">
                                            <div className="searchDiv relDiv">
                                            <input className="form-control nameSearch bb-0 backbutton" value="Back" type="search" readOnly={true} data-toggle="modal" onClick={()=>this.closeTile()} />  {/* href="#viewTile" */}
                                            </div>
                                    </div>
                                  <table className="table ShopProductTable">
                                         <colgroup>
                                             <col style={{width:'*'}}/>
                                                 <col style={{width:40}}/>
                                         </colgroup>                                         
                
                                            <tbody>
                                                
                                                    { (this.state.isLoading && ! productlist)  ?       
                                                    <tr>
                                                    <td>  <i className="fa fa-spinner fa-spin">loading...</i></td>
                                                    </tr> 
                                                    :
                                                
                                                    this.props.filteredProduct && this.props.filteredProduct.length > 0 ?(
                                                        this.props.filteredProduct.map((item,index)=>{
                                                            let isSimpleProduct = (item.Type != "simple") ? true : false ;  
                                                            return(                                                    
                                                              
                                                                <tr   key={index}  class="pointer">
                                                                <td align="right" class="p-0">
                                                                     <div class="custom_radio">
                                                                        <input type="radio" name="setFavorite" id={item.WPID} value={item.WPID} data-type="product" data-slug={item.Type} data-id={item.WPID} />
                                                                        <label htmlFor={item.WPID} class="pl-5">{item.Title ? item.Title:'N/A'}</label>
                                                                     </div>
                                                                 </td>
                                                               </tr>
                                                          
                                                            )
                                                        })
                                                    ):
                                                    productlist && productlist.map((item,index)=>{
                                                            let isSimpleProduct = (item.Type != "simple") ? true : false ;  
                                                            return(                                                    
                                                               
                                                                <tr   key={index}  className="pointer">
                                                                <td align="right" className="p-0">
                                                                     <div className="custom_radio">
                                                                        <input type="radio" name="setFavorite" id={item.WPID} value={item.WPID} data-type="product" data-slug={item.Type} data-id={item.WPID} />
                                                                        <label htmlFor={item.WPID} className="pl-3">{item.Title ? item.Title:'N/A'}</label>
                                                                     </div>
                                                                 </td>
                                                               </tr>
                                                            )
                                                        })
                                                                                                
                                                    } 
                                            </tbody>
                                </table>
                                </div>
                        :item==2?
                                <div>
                                            <div className="clearfix">
                                                <div className="searchDiv relDiv">
                                                <input className="form-control nameSearch bb-0 backbutton" value="Back" type="search" readOnly={true} data-toggle="modal" onClick={()=>this.closeTile()} />  {/* href="#viewTile" */}
                                                </div>
                                            </div>
                                        {
                                      this.state.active != true?
                                        <div >
                                        {!attributelist || attributelist.length == 0 ?<LoadingModal/>:''}
                                               
                                                    <div>
                                                    <table className="table ShopProductTable">
                                                      
                                                        <tbody>
                                                            {(this.state.isLoading && !attributelist) ?
                                                            <tr  >     
                                                            <td>  <i className="fa fa-spinner fa-spin">loading...</i></td>
                                                        </tr>                            
                                                                :
                                                              
                                                                (attributelist.map((item, index) => {


                                                                    return (
                                                                        <tr  key={index} className="pointer">
                                                                        <td style={divStyle}>
                                                                             <div className="custom_radio custom_radio_set">
                                                                                <input type="radio" name="setFavorite" id={item.Id} value={item.Id} data-slug={item.Value} data-type="attribute" data-id={item.Id} />
                                                                              <label htmlFor={item.Id}  className="pl-3">&nbsp;</label>
                                                                             </div>
                                                                         </td>
                                                                        <td align="left" onClick={()=>this.loadSubAttribute(item.SubAttributes)}>
                                                                        {item.Value}       </td>
                                                                          
                                                                        <td style={divStyle2} onClick={()=>this.loadSubAttribute(item.SubAttributes)} > </td>
                                                                        <td  className="text-right"><a> <img src="assets/img/next.png" /></a></td>
                                                                       
                                                                       </tr> 
                                                                    
                                                                   )
                                                                })
                                                                )


                                                            }

                                                        </tbody>
                                                        </table>
                                                        </div>                       
                                            </div>
                                         
                                         : //<SubAttributes subattributelist={subattributelist}/>
                                                <div>
                                                   <table className="table ShopProductTable">
                                                                                  
                                                        <tbody>
                                                                
                                                                {
                                                                    (this.state.subattributelist && this.state.subattributelist.length>0? this.state.subattributelist.map((item,index)=>{
                                                                        return(
                                                                            <tr  key={index} className="pointer">
                                                                            <td align="left" className="p-0" style={divStyle}>
                                                                            <div className="custom_radio custom_radio_set">
                                                                                    <input type="radio" name="setFavorite" id={item.Id} value={item.Id} data-slug={item.Value} data-type="attribute" data-id={item.Id} />
                                                                                  <label htmlFor={item.Id}  className="pl-3">&nbsp;</label>
                                                                                 </div>
                                                                             </td>
                                                                             
                                                                             {item.SubAttributes?
                                                                             <td align="left" className="p-0 pl-4" onClick={()=>this.loadSubAttribute(item.SubAttributes)}>  {item.Value }      </td>:
                                                                             <td align="left" className="p-0 pl-4"> {item.Value} </td>
                                                                              } 
                                                                          
                                                                          {item.SubAttributes?
                                                                            <td className="p-0" style={divStyle2} onClick={()=>this.loadSubAttribute(item.SubAttributes)} ><a> <img src="assets/img/next.png" /></a> </td>
                                                                           : <td  className="text-right"></td>
                                                                          }
                                                                         
                                                                           </tr> 
                                                                      
                                                                        )
                                                                    })
                                                                    :
                                                                    <tr data-toggle="modal"><td>No Data Found</td></tr>)                                        
                                                                }
                                                                </tbody>
                                                    </table>
                                            </div>
                                        }
                                </div>
                        : item == 3 ?
                                    // <Categories />
                                    <div>
                                         <div className="clearfix">
                                            <div className="searchDiv relDiv">
                                            <input className="form-control nameSearch bb-0 backbutton" value="Back" type="search" readOnly={true} data-toggle="modal" onClick={()=>this.closeTile()} />  {/* href="#viewTile" */}
                                            </div>
                                        </div>
                                     {active!=true ? 
                                   
                                                  <table className="table ShopProductTable ">
                                                         
                                                   <tbody>
                                                             { categorylist &&  categorylist.map((item,index)=>{
                                                                     return(
                                                                         
                                                                            <tr  key={index}  className="pointer">
                                                                                    <td align="left" className="p-0" style={divStyle}>
                                                                                    <div className="custom_radio custom_radio_set">
                                                                                            <input type="radio" name="setFavorite" id={item.id} value={item.id} data-slug={item.Value?item.Value:'N/A'} data-type="category" data-id={item.id}/>
                                                                                            <label htmlFor={item.id}  className="pl-3">&nbsp;</label>
                                                                                        </div>
                                                                                    </td>

                                                                                    {item.Subcategories && item.Subcategories.length>0?
                                                                                         <td align="left" className="p-0 pl-4" onClick={()=>this.loadSubCategory(item.Subcategories)}>  {item.Value }      </td>:
                                                                                         <td align="left" className="p-0 pl-4"> {item.Value} </td>
                                                                                          } 
                                                                                      
                                                                                      {item.Subcategories && item.Subcategories.length>0?
                                                                                        <td className="p-0" style={divStyle2} onClick={()=>this.loadSubCategory(item.Subcategories)} ><a> <img src="assets/img/next.png" /></a> </td>
                                                                                       : <td  className="text-right"></td>
                                                                                      }
                                                                                        
                                                                                </tr>
                                                                      
                                                                    )
                                                                })
                                                                                                                                                                  
                                                              } 
                                                            
                                                             </tbody>
                                                             </table>
                                                           
                                                    :
                                                           <div>                                
                                                               <table className="table ShopProductTable">
                                                                {/* <colgroup>
                                                                    <col style={{width:'*'}}/>
                                                                        <col style={{width:40}}/>
                                                                </colgroup>                                                */}
                                                                    <tbody>
                                                                            
                                                                            {
                                                                                (this.state.subcategorylist && this.state.subcategorylist.length>0? this.state.subcategorylist.map((item,index)=>{
                                                                                    return(
                                                                                        <tr  key={index} className="pointer">
                                                                                        <td align="left" className="p-0" style={divStyle}>
                                                                                             <div className="custom_radio custom_radio_set">
                                                                                                <input type="radio" name="setFavorite" id={item.Id} value={item.Id} data-slug={item.Value} data-type="category" data-id={item.Id} />
                                                                                              <label htmlFor={item.Id}  className="pl-3">&nbsp;</label>
                                                                                             </div>
                                                                                         </td>
                                                                                         
                                                                                         {item.Subcategories && item.Subcategories.length>0?
                                                                                         <td align="left" className="p-0 pl-4" onClick={()=>this.loadSubCategory(item.Subcategories)}>  {item.Value }      </td>:
                                                                                         <td align="left" className="p-0 pl-4"> {item.Value} </td>
                                                                                          } 
                                                                                      
                                                                                      {item.Subcategories && item.Subcategories.length>0?
                                                                                        <td className="p-0" style={divStyle2} onClick={()=>this.loadSubCategory(item.Subcategories)} ><a> <img src="assets/img/next.png" /></a> </td>
                                                                                       : <td  className="text-right"></td>
                                                                                      }
                                                                                     
                                                                                       </tr> 
                                                                                 
                                                                                    )
                                                                                })
                                                                                :
                                                                                <tr data-toggle="modal"><td>No Data Found</td></tr>)                                        
                                                                            }
                                                                            </tbody>
                                                                </table>
                                                        </div>
                                                                  
                                                                 
                                                          }
                                                            </div>
                                    : null

                    )
                }
            </div>
            <div>


                         </div>
                     </div>
                 </div>
                 <div className="modal-footer p-0">
                     <button type="button" className="btn btn-primary btn-block h66"  onClick={()=>this.addToFavourite()}>SAVE & UPDATE</button>
                 </div>
             </div>
         </div>
     </div>     


)}
}
const divStyle = {
    width:"56px"
  };

  const divStyle2 = {
    width: "14px"
  };

// export  default SubCategories;

function mapStateToProps(state) {
     console.log("TileViewState",state);
    // console.log("props",this.props);
 
    const { categorylist,productlist,attributelist } = state;    
    
    return {
        categorylist:categorylist,
        productlist:productlist,
        attributelist:attributelist
    };
}

const connectedList = connect(mapStateToProps)(TileModel);
export { connectedList as TileModel };