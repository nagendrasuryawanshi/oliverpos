import React from 'react';
import { connect } from 'react-redux';
import { userActions } from '../_actions/user.actions';


class NavbarPage extends React.Component {

    componentDidMount(){
       // const page  = this.props;
       
    }

    logout(){
        this.props.dispatch(userActions.logout())
    }
    render() {
      //  console.log("authentication",this.props)
       const { match , user }  =  this.props;
       console.log("authentication",user)
       console.log("siteList",localStorage.getItem('Location'));
       console.log("UserLocations",localStorage.getItem('UserLocations'));
       
        return (
            <nav id="sidebar" className="sidebarChanges active cm-menus">
                <div className="cm-switcher clearfix">
                  {user  && user.user && 
                    <div className="cm-user">
                      <h4>
                          {/* Younique Shoes    */}
                         {user.user.display_name}
                      </h4>
                        <p>{user.user.shop_name?user.user.shop_name:''}</p> 
                        {/* '/'Register 1 St. John´s/Register 1 */}
                           </div>
                  }

                    {/* <div className="cm-user-switcher">
                        <div className="flat-toggle cm-flat-toggle">
                            <span data-open="Open" data-close="Close"></span>
                            <input value="0" type="hidden" />
                        </div>
                    </div> */}
                </div>
                <div className="cm-body">
                    <div className="cm-body-init-scroll overflowscroll">
                        <ul className="list-unstyled ulsidebar mb-0 pl-0">
                            <li className={'pt-2 pb-2' + (match.path=='/register'||match.path=='/shopview'?' active ': null)} >
                                <a href="/shopview">
                                    <span className="strip_icon register"></span>
                                    <span className="txt_menu animated fadeInUp">Register</span>
                                </a>
                            </li>
                            <li className={'pt-2 pb-2' + (match.path=='/activity'?' active ': null )}>
                                <a href="/activity">
                                    <span className="strip_icon activity"></span>
                                    <span className="txt_menu animated fadeInUp">Activity View</span>
                                </a>
                            </li>
                            {/* <li className={'pt-2 pb-2' + (match.path=='/cash_report'?' active ': null )}>
                                <a href="/cash_report">
                                    <span className="strip_icon cashdrable"></span>
                                    <span className="txt_menu animated fadeInUp">Cash Drawer</span>
                                </a>
                            </li> */}
                            {/* <li className={'pt-2 pb-2' + (match.path=='/setting'?' active ': null )}>
                                <a href="/setting">
                                    <span className="strip_icon setting"></span>
                                    <span className="txt_menu animated fadeInUp">Settings</span>
                                </a>
                            </li> */}
                        </ul>
                        <div className="clearfix"></div>
                    </div>
                </div>
                <div className="cm-sec-acc clearfix">
                    <div className="cm-sec-accTop">
                        <ul className="list-unstyled ulsidebar mb-0 pl-0">
                            {/* <li className="">
                                <a href="/login">
                                    <span className="strip_icon wp_panel"></span>
                                    <span className="txt_menu animated fadeInUp">WP Panel</span>
                                </a>
                            </li> */}
                            <li className='w-50'>
                                <a href="javascript:void(0)">
                                    <span className="strip_icon support"></span>
                                    <span className="txt_menu animated fadeInUp">Support</span>
                                </a>
                            </li>
                            <li className='w-50' onClick={()=>this.logout()}>
                                <a href="javascript:void(0)">
                                    <span className="strip_icon logout"></span>
                                    <span className="txt_menu animated fadeInUp">Log Out</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                  {/*   <div className="cm-sec-accBottom clearfix">
                        <div className="secmk secmka">
                            <button className="swipclock">
                                MK
                            </button>
                            <div className="simply-countdown-one"></div>
                        </div> 
                    </div>*/}
                </div>
            </nav>

        )
    }
}


function mapStateToProps(state) {
    const { authentication} = state;
    return {
        user:authentication,
    };
}

const connectedNavbarPage = connect(mapStateToProps)(NavbarPage);
export { connectedNavbarPage as NavbarPage };