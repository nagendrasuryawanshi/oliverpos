import React from 'react';
import { connect } from 'react-redux';
import { allProductActions}  from '../_actions/allProduct.action'
import { cartProductActions}  from '../_actions/cartProduct.action'
import { default as NumberFormat } from 'react-number-format'
import { history } from '../_helpers';
import { variationPopup } from './variationPopup';
import { LoadingModal } from './'
import { ProductOutOfStockPopupModel } from './ProductOutOfStockPopupModel'



class  AllProduct extends  React.Component {
          
        constructor(props) {
            super(props);
    
            // reset login status
            this.props.dispatch(allProductActions.refresh());
            this.state = {
                active : false,            
                isLoading : true,
                showVariationPopup : false,
                variationPopupQuantity : 1,
                cartproductlist:localStorage.getItem("CARD_PRODUCT_LIST") ? JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST") ): []
            } 

               const { dispatch } = this.props;
               dispatch(allProductActions.getAll());   

                // This binding is necessary to make `this` work in the callback 
                this.handleIsVariationProduct = this.handleIsVariationProduct.bind(this);
                this.addSimpleProducttoCart   = this.addSimpleProducttoCart.bind(this);
                this.addvariableProducttoCart = this.addvariableProducttoCart.bind(this);   
                this.productOutOfStock = this.productOutOfStock.bind(this);   
        }

        componentDidMount() {
           this.props.onRef(this)
        }
        
        componentWillUnmount() {
            this.props.onRef(null)
        }//

        filterProductByTile(type, item){ 
            switch ( type ) {
                case "attribute":
                  this.filterProductForAttribute( item );
                  break;
                case "sub-attribute":
                  this.filterProductForSubAttribute( item );
                  break;
                case "category":
                  this.filterProductForCateGory( item );
                  break;
                case "sub-category":
                  this.filterProductForSubCateGory( item );
                  break;
                case "inner-sub-attribute":
                  this.productTableFilter( item, 4 );
                  break;
                case "inner-sub-category":
                  this.productTableFilter( item, 5 );
                  break;
                case "product-search":
                  this.productTableFilter( item, 0 );
                  break;
              }
        }

        filterProductForAttribute( item ){ 
            this.productTableFilter(item.attribute_code, 3);
        }

        filterProductForSubAttribute( item ){
            this.productTableFilter(item.attribute_slug, 4);
        }

        filterProductForCateGory( item ){
            this.productTableFilter(item.category_slug, 5);
        }

        filterProductForSubCateGory( item ){
            this.productTableFilter(item.category_slug, 5);
        }

        productTableFilter( filter, td_index ){ 
              // Declare variables 
            var input, table, tr, td, i, txtValue;                        
            table = document.getElementById("all-product-list");
            tr = table.getElementsByTagName("tr");

            // Loop through all table rows, and hide those who don't match the search query
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[td_index];
                if (td) {
                    txtValue = td.textContent || td.innerText;
                    if (txtValue.toUpperCase().indexOf(filter.toUpperCase()) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                } 
            }
        }

        handleIsVariationProduct( type, product ){
            if (type == "simple") {
                this.addSimpleProducttoCart( product );
            } else {
                this.addvariableProducttoCart( product );
            }
        }

        productOutOfStock( title ){ 
          //  alert(`${title} is out of stock.`);
          
                $('#outOfStockModal').modal('show');
            //   <ProductOutOfStockPopupModel />
           
        }

        addSimpleProducttoCart( product ){
            const { dispatch ,checkout_list,cartproductlist } = this.props; 
        
            var cartlist = cartproductlist ? cartproductlist : [];
            let setQunatity = 1;
            var data = {
                line_item_id : 0,
                quantity : setQunatity,
                Title : product.Title,
                Price : setQunatity * parseFloat(product.Price),
                product_id : product.WPID,
                variation_id : 0,
                isTaxable : product.Taxable,
            }
            
            cartlist.push(data);            
            dispatch(cartProductActions.addtoCartProduct(cartlist));  // this.state.cartproductlist
                      
        }

        addvariableProducttoCart( product ){ 
            this.props.productData( product );
        }

        goBack(){
            console.log("GoBack");
            history.push('/listview');
        }
        
        render() {                                
            const { active } = this.state;
            const { cartproductlist } = this.props;
           // console.log("1NagendracartList", cartproductlist);
            console.log("filteredProduct", history);
            console.log("list",this.props);
              return (        
                //  <div className={history.location.pathname == '/shopview'?"col-lg-4 col-sm-5 col-xs-5 p-0":"col-lg-9 col-sm-8 col-xs-8 pr-0"}>
            
                             <div className="items pt-3">
                             {!this.props.productlist || this.props.productlist.length == 0 ?<LoadingModal/>:''}
                               <div className="item-heading text-center">PRODUCT LIST</div>
                                <div className="panel panel-default panel-product-list overflowscroll p-0" id="allProductHeight">
                                   <div className="searchDiv" style={{display:'none'}}>
                                         <input type="search" className="form-control nameSearch" placeholder="Search Product / Scan"/>
                                     </div>   
                                    {this.props.parantPage && this.props.parantPage=='list'? <div className="pl-4 pr-4 previews_setting">
                                        <a href="/listview" className="back-button " id="mainBack" onClick={()=>this.goBack()}>
                                           <img className="pushnext ml-2 mr-3 mCS_img_loaded" src="assets/img/back_modal.png" />
                                          <span>Back</span>
                                      </a>
                                  </div>:""}
                                 
                                <table className="table ShopProductTable" id="all-product-list">
                                         <colgroup>
                                             <col style={{width:'*'}}/>
                                                 <col style={{width:40}}/>
                                         </colgroup>
                                           
                
                                <tbody>
                                    
                                         { (this.state.isLoading && ! this.props.productlist)  ?       
                                         <tr>
                                         <td>  <i className="fa fa-spinner fa-spin">loading...</i></td>
                                         </tr> 
                                         :
                                        //  }
                                        //  {this.props.productlist &&
                                        //     this.props.productlist &&
                                        this.props.filteredProduct && this.props.filteredProduct.length > 0 ?(
                                            this.props.filteredProduct.map((item,index)=>{
                                                console.log("filteredProduct11",  item);
                                                let isSimpleProduct = (item.Type != "simple") ? true : false ;  
                                                 return(                                                   
                                                    <tr key={index} data-toggle={isSimpleProduct ? "modal" : ""} href={isSimpleProduct ? "#variationProductPopup" : "javascript:void(0)"} onClick={this.handleIsVariationProduct.bind(this, item.Type, item)}>
                                                         <td>{item.Title ? item.Title:'N/A'}</td>
                                                         <td className="text-right"><NumberFormat value= {item.Price} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} /> 
                                                         </td>
                                                         <td  className="text-right"><a>  <img src="assets/img/add_29.png" className="pointer" /></a></td>
                                                         <td style={{display : "none"}}>{item.has_attribute.toString().replace(",", " ")}</td> 
                                                         <td style={{display : "none"}}>{item.has_subattribute.toString().replace(",", " ")}</td>
                                                         <td style={{display : "none"}}>{item.Categories.toString().replace(",", " ")}</td>
                                                     </tr> 
                                                )
                                              })
                                        ):
                                        this.props.productlist.map((item,index) => {
                                           // console.log("filteredProduct12",  item);

                                                let isSimpleProduct = (item.Type != "simple") ? true : false ;  
                                                 return(                                                    
                                                    <tr key={index}>
                                                         <td>{item.Title ? item.Title:'N/A'}</td>
                                                         <td className="text-right"><NumberFormat value= {item.Price} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} /> 
                                                         </td>
                                                         {
                                                             (item.InStock) ? (
                                                                <td className="text-right" data-toggle={isSimpleProduct ? "modal" : ""} href={isSimpleProduct ? "#variationProductPopup" : "javascript:void(0)"} onClick={this.handleIsVariationProduct.bind(this, item.Type, item)} style={{padding:20}}>
                                                                    <a>  
                                                                        <img src="assets/img/add_29.png" className="pointer" />
                                                                    </a>
                                                                </td>
                                                             ) : (
                                                                <td className="text-right" onClick={() => this.productOutOfStock(item.Title)} style={{padding:20}}>
                                                                    <a href="#outOfStockModal">  
                                                                        <img src="assets/img/add_29.png"  className="pointer" />
                                                                    </a>
                                                                </td>
                                                             )
                                                         }
                                                         
                                                         <td style={{display : "none"}}>{item.has_attribute.toString().replace(",", " ")}</td> 
                                                         <td style={{display : "none"}}>{item.has_subattribute.toString().replace(",", " ")}</td>
                                                         <td style={{display : "none"}}>{item.Categories.toString().replace(",", " ")}</td>
                                                     </tr> 
                                                )
                                              })
                                                                                    
                                          } 
                                 </tbody>
                                </table>
                             </div>
                             {/* <ProductOutOfStockPopupModel /> */}
                         </div>    
                    // </div>                                                        
            )}
    
    }
    
    
    function mapStateToProps(state) {
       // console.log("state",state);
       // console.log("props",this.props);
        const {productlist ,cartproductlist ,filteredProduct ,checkout_list } = state;
        // const { productlist } = state.productlist;    
        // const { cartproductlist } = state.cartproductlist;    
        console.log("Nagendracartproductlist",cartproductlist );
        return {
            productlist:productlist.productlist,
            cartproductlist:localStorage.getItem("CARD_PRODUCT_LIST") ? JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST") ): [],//cartproductlist.cartproductlist,
            filteredProduct:filteredProduct.items,
            checkout_list:checkout_list.items
        };
    }
    
    const connectedList = connect(mapStateToProps)(AllProduct);
    export { connectedList as AllProduct };