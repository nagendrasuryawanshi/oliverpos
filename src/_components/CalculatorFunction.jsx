// rate like is % and rupees
// discount_amount 
// product is product list
// type is diffrenciate product and cart
import { cartProductActions } from '../_actions'

export const CalculatorFunction = {

    discount_calculate

}

export const discount_calculate = (rate, discount_amount, product_list, total_price, tax ,Props ,type) => {
    var price = 0, discount = 0, afterDiscount = 0,Tax = 0, total_tax = 0 ,discount_amount_is = 0 ,new_discount = 0,product_id=0 ,newArray_afterDiscount = [] ,newArray_discountAmount = [];
        if(type =='card'){
            product_list.map(item => {
                price = parseFloat(item.Price);
                discount = parseFloat(discount_amount)
                Tax = parseFloat(tax);

                if (rate == 'Percentage' && discount < 100) {
                    discount_amount_is = (price * discount / 100);
                    afterDiscount = price - discount_amount_is;        
                    Props.onDiscountAmountChange(discount_amount_is);            
                    total_tax = afterDiscount + (afterDiscount * Tax / 100);            
                    item["after_discount"] = afterDiscount;
                    item["discount_Amount"] = discount_amount_is;                         
                    Props.dispatch(cartProductActions.addtoCartProduct(product_list));                    
                } else if(rate == 'Percentage' && discount > 100){
                    alert("plaese not use 100% discount")
                } else {                        
                    new_discount = parseFloat(discount*100/total_price);   
                    if(new_discount >= 100){
                        alert("plaese not use 100% discount")
                    }else{         
                    discount_amount_is = (price * new_discount / 100);            
                    afterDiscount = price - discount_amount_is;            
                    Props.onDiscountAmountChange(discount);
                    total_tax = afterDiscount + (afterDiscount * Tax / 100);
                    item["after_discount"] = afterDiscount;
                    item["discount_Amount"] = discount_amount_is;                         
                    Props.dispatch(cartProductActions.addtoCartProduct(product_list));                       
                }
              }
            })
        }

      if(type == 'product'){
           var item =  product_list
           product_id = item.product_id;
           price = parseFloat(item.Price);
           discount = parseFloat(discount_amount)
           Tax = parseFloat(tax);
           
         if (rate == 'Percentage' && discount < 100) {

           discount_amount_is = (price * discount / 100)
           afterDiscount = price - discount_amount_is
           total_tax = (typeof afterDiscount !== "undefined") ? afterDiscount + ( afterDiscount * Tax / 100) : price + ( price * Tax / 100);
           var updateProductList = JSON.parse(localStorage.getItem('CARD_PRODUCT_LIST'));            
           updateProductList.map((item, index) => {                 
              if(item.product_id == product_id){                   
                   updateProductList[index]["after_discount"] = afterDiscount
                   updateProductList[index]["discount_Amount"] = discount_amount_is

                //    updateProductList[index]["product_after_discount"] = afterDiscount
                //    updateProductList[index]["product_discount_Amount"] = discount_amount_is
              }
           })
         
          Props.dispatch(cartProductActions.addtoCartProduct(updateProductList));  
          


       } else if(rate == 'Percentage' && discount >= 100){
          // alert("plaese not use 100% discount")
       } else {
           //alert("product inside rupees")
           
           new_discount = parseFloat( discount * 100 / price );
            if(new_discount >= 100){
               // alert("plaese not use 100% discount")
            }else{
           discount_amount_is = (price * new_discount / 100)
           afterDiscount = price - discount_amount_is
           Props.onDiscountAmountChange(discount)
           total_tax = afterDiscount + (afterDiscount * Tax / 100)
           console.log("product AFTER TOTAL_TAX", total_tax);

            var updateProductList = JSON.parse(localStorage.getItem('CARD_PRODUCT_LIST'));            
            updateProductList.map((item, index) => {                 
               if(item.product_id == product_id){                   
                    updateProductList[index]["after_discount"] = afterDiscount
                    updateProductList[index]["discount_Amount"] = discount_amount_is
               }
            })
          
           Props.dispatch(cartProductActions.addtoCartProduct(updateProductList));          
          
       }
    }

    }

}


export default CalculatorFunction;