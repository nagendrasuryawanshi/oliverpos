import React from 'react';
import { connect } from 'react-redux';
import { cartProductActions } from '../_actions';


class CustomerAddFee extends React.Component {

    constructor(props){
        super(props);
        this.state={
            extra_product:[],
            amount:'',
            add_title:''
        }
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange(e) {
        const { name, value } = e.target;
        console.log("%cCONSOLE" ,'color:green',name,value)
        this.setState({ [name]: value });
    }

    SetValue(eleT, val) {
        let v1 = this.state.amount + val;
        eleT.html(eleT.html().replace(' ', '') + val + ' ');
        this.setState({ amount: v1 })

    }

    calcInp(input) {
        var elemJ = jQuery('#txtdisAmount');
        if (this.state.emptyPaymentFlag || elemJ.html() == "0") {
            elemJ.html('');
            this.setState({
                emptyPaymentFlag: false,
            });
        }

        switch (input) {
            case 1:
                this.SetValue(elemJ, '1');
                break;
            case 2:
                this.SetValue(elemJ, '2');
                break;
            case 3:
                this.SetValue(elemJ, '3');
                break;
            case 4:
                this.SetValue(elemJ, '4');
                break;
            case 5:
                this.SetValue(elemJ, '5');
                break;
            case 6:
                this.SetValue(elemJ, '6');
                break;
            case 7:
                this.SetValue(elemJ, '7');
                break;
            case 8:
                this.SetValue(elemJ, '8');
                break;
            case 9:
                this.SetValue(elemJ, '9');
                break;
            case 0:
                this.SetValue(elemJ, '0');
                break;
            case '.':
                if (elemJ.html() == "") {
                    this.SetValue(elemJ, '0');
                } else {
                    if (elemJ.html().includes(".")) {

                    } else {
                        this.SetValue(elemJ, '.');
                    }
                }
                break;
            //alert('n');
            // default code block
        }
    }


    rmvInp() {

        let str = $('#txtdisAmount').html();
        str = str.substring(0, str.length - 2);
        $('#txtdisAmount').html(str + ' ');
        if (str == "" || str == " ") {
            $('#txtdisAmount').html('0');
        } else { }
        //console.log("str",this.state.paidAmount)
        this.setState({ amount: str })
      
    }

    AddFee(){
        const {amount ,add_title } = this.state;
        if(amount!=0){
            var data={
                Title:add_title?add_title:'Custome Fee',
                Price:parseInt(amount)
  
            }
             var  cartlist= localStorage.getItem("CARD_PRODUCT_LIST") ? JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST") ): []
             cartlist=cartlist==null?[]:cartlist;
             cartlist.push(data)
             this.props.dispatch(cartProductActions.addtoCartProduct(cartlist));
            $('.close').trigger('click')
          
        }
        
    }

    render(){
        const { amount ,add_title } = this.state;
        return(
            <div className="modal-dialog" id="dialog-midle-align">
            <div className="modal-content">
                <div className="modal-header">
                    <button type="button" className="close" data-dismiss="modal" aria-hidden="true">
                        <img src="assets/img/delete-icon.png"/>
                    </button>
                    <h4 className="modal-title">Add Fee</h4>
                </div>
                <div className="modal-body p-0">
                    <form className="clearfix">
                        <div className="col-sm-12 p-0">
                            <div className="panel-product-list" id="panelCalculatorpopUp">
                                <div className="panel panelCalculator">
                                    <div className="panel-body p-0">
                                        <table className="table table-bordered shopViewPopUpCalculator">
                                            <tbody>
                                                <tr>
                                                    <td className="text-right br-1 bl-1 bt-0">
                                                        <div className="input-group discount-input-group">
                                                        <input className="form-control" name="add_title" value={add_title} placeholder="Custom Fee" type="text" onChange={this.handleChange} />
                                                        </div>
                                                    </td>
                                                    <td className="text-right br-1 bl-1 bt-0">
                                                        <div className="input-group discount-input-group">
                                                            <input type="text" id="txtdisAmount" className="form-control text-right"  value={amount} text='' aria-describedby="basic-addon1"/>
                                                        </div>
                                                    </td>
                                                    <td className="text-center pointer bt-0" onClick={()=>this.rmvInp()}>
                                                  
                                                        <button type="button" className="btn btn-default calculate">
                                                            <img width="36" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMS4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDMxLjA1OSAzMS4wNTkiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDMxLjA1OSAzMS4wNTk7IiB4bWw6c3BhY2U9InByZXNlcnZlIiB3aWR0aD0iNTEycHgiIGhlaWdodD0iNTEycHgiPgo8Zz4KCTxnPgoJCTxwYXRoIGQ9Ik0zMC4xNzEsMTYuNDE2SDAuODg4QzAuMzk4LDE2LjQxNiwwLDE2LjAyLDAsMTUuNTI5YzAtMC40OSwwLjM5OC0wLjg4OCwwLjg4OC0wLjg4OGgyOS4yODMgICAgYzAuNDksMCwwLjg4OCwwLjM5OCwwLjg4OCwwLjg4OEMzMS4wNTksMTYuMDIsMzAuNjYxLDE2LjQxNiwzMC4xNzEsMTYuNDE2eiIgZmlsbD0iIzRiNGI0YiIvPgoJPC9nPgoJPGc+CgkJPHBhdGggZD0iTTE2LjAxNywzMS4wNTljLTAuMjIyLDAtMC40NDUtMC4wODMtMC42MTctMC4yNUwwLjI3MSwxNi4xNjZDMC4wOTgsMTUuOTk5LDAsMTUuNzcsMCwxNS41MjkgICAgYzAtMC4yNCwwLjA5OC0wLjQ3MSwwLjI3MS0wLjYzOEwxNS40LDAuMjVjMC4zNTItMC4zNDEsMC45MTQtMC4zMzIsMS4yNTUsMC4wMmMwLjM0LDAuMzUzLDAuMzMxLDAuOTE1LTAuMDIxLDEuMjU1TDIuMTYzLDE1LjUyOSAgICBsMTQuNDcxLDE0LjAwNGMwLjM1MiwwLjM0MSwwLjM2MSwwLjkwMiwwLjAyMSwxLjI1NUMxNi40OCwzMC45NjgsMTYuMjQ5LDMxLjA1OSwxNi4wMTcsMzEuMDU5eiIgZmlsbD0iIzRiNGI0YiIvPgoJPC9nPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo="></img>
                                                        </button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td className="td-calc-padding br-1 bl-1">
                                                        <button type="button" onClick={()=>this.calcInp(1)} className="btn btn-default calculate">1</button>
                                                    </td>
                                                    <td className="td-calc-padding br-1">
                                                        <button type="button" onClick={()=>this.calcInp(2)} className="btn btn-default calculate">2</button>
                                                    </td>
                                                    <td className="td-calc-padding">
                                                        <button type="button" onClick={()=>this.calcInp(3)} className="btn btn-default calculate">3</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td className="td-calc-padding br-1 bl-1">
                                                        <button type="button" onClick={()=>this.calcInp(4)} className="btn btn-default calculate">4</button>
                                                    </td>
                                                    <td className="td-calc-padding br-1">
                                                        <button type="button" onClick={()=>this.calcInp(5)} className="btn btn-default calculate">5</button>
                                                    </td>
                                                    <td className="td-calc-padding">
                                                        <button type="button" onClick={()=>this.calcInp(6)} className="btn btn-default calculate">6</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td className="td-calc-padding br-1 bl-1">
                                                        <button type="button" onClick={()=>this.calcInp(7)} className="btn btn-default calculate">7</button>
                                                    </td>
                                                    <td className="td-calc-padding br-1">
                                                        <button type="button" onClick={()=>this.calcInp(8)} className="btn btn-default calculate">8</button>
                                                    </td>
                                                    <td className="td-calc-padding">
                                                        <button type="button" onClick={()=>this.calcInp(9)} className="btn btn-default calculate">9</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                   <td colSpan="2" className="td-calc-padding br-1 bl-1" > 
                                                    <button type="button" onClick={()=>this.calcInp('.')} className="btn btn-default calculate"> . </button>
                                                       
                                                    </td>
                                                    <td className="td-calc-padding">
                                                        <button type="button" onClick={()=>this.calcInp(0)} className="btn btn-default calculate">0</button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div className="modal-footer p-0">
                    <button type="button" onClick={()=>this.AddFee()} className="btn btn-primary btn-block h66">ADD FEE</button>
                </div>
            </div>
        </div>
        )
    }
}
function mapStateToProps(state){
    return{

    }
}

const connectedCustomerAddFee = connect(mapStateToProps)(CustomerAddFee);
export { connectedCustomerAddFee as CustomerAddFee };