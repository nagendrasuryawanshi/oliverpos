import React from 'react';
import { connect } from 'react-redux';
import { allProductActions } from '../_actions'
import { cartProductActions}  from '../_actions/cartProduct.action';


class CommonHeaderTwo extends React.Component {
   constructor(props){
       super(props);
       this.state={ItemCount:0}    
        console.log("history LOcation header",history);      

       this.filterProduct = this.filterProduct.bind(this)
   }

    filterProduct(){
       let input = $("#product_search_field").val();
       this.props.searchProductFilter( input, "product-search");
    }

    removeCheckOutList(){      
        localStorage.removeItem('CHECKLIST');
        localStorage.removeItem('oliver_order_payments');
       
       // localStorage.removeItem("CARD_PRODUCT_LIST");
       console.log("CartDeleted");
       const { dispatch } = this.props;
       localStorage.removeItem("CART") 
       localStorage.removeItem('CARD_PRODUCT_LIST');
       localStorage.removeItem("PRODUCT")
       localStorage.removeItem("SINGLE_PRODUCT")
        dispatch(cartProductActions.addtoCartProduct(null)); 
    }

   componentDidUpdate()
   {
       console.log("3534533453")
   }

   handleItemCount()  {
        console.log("callbackAmount");  
        this.setState({discountAmount : amt,discountType : type});      
        let CartItem =JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST"));   
        if (CartItem)
        {
            this.setState({ItemCount:CartItem.length}) ;
        }
    }

    render() {
        const { match ,productlist ,attributelist , categorylist,cartproductlist} = this.props;
       
        return (

            <nav className="navbar navbar-default" id="colorFullHeader">
                <div className="col-lg-9 col-sm-8 col-xs-8 p-0">
                    <div className="container-fluid p-0">
                        <div className="navbar-header">
                            <button type="button" id="sidebarCollapse" className="navbar-btn active">
                                <span></span>
                                <span></span>
                                <span></span>
                            </button>
                        </div>
                        
                        <div className="mobile_menu clearfix">
                            <ul>
                                <li className={match.path == '/shopview'? "active" : null }>
                                    <a href="/shopview">tile View</a>
                                </li>
                                <li className={match.path == '/listview'? "active" : null }>
                                    <a href="/listview">list View</a>
                                </li>
                                <li className={match.path == '/customerview'? "active" : null }>
                                    <a href="/customerview">customer View</a>
                                </li>
                            </ul>
                        </div>
                         <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                            <ul className="nav navbar-nav navbar-left">
                                <li className={match.path == '/shopview'? "active" : null }><a href="/shopview">Tile View</a></li>
                                <li className={match.path == '/listview'? "active" : null }><a href="/listview">List View</a></li>
                                <li className={match.path == '/customerview'? "active" : null }><a href="/customerview">Customer View</a></li>
                            </ul>
                            {match.path != '/customerview' ?
                            <ul className="nav navbar-nav navbar-right">
                                <li>
                                    <div className="search_block">
                                        <div className="sample one">
                                            <input type="text" id="product_search_field" className="search_field" name="search" onChange={() => this.filterProduct()} placeholder="search" />
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            :null}
                        </div>
                    </div>
                </div>
                {match.path != '/customerview' ?
                // <CartItemCount  itemcounthandler={this.handleItemCount()}/>
                <div className="col-lg-3 col-sm-4 col-xs-4 pr-0 cart_header_overlap">
                    <div className="cart_header">
                        <a href="javascript:void(0);" className="ch_icon">
                            <button className="button icon icon-delete" onClick={()=>this.removeCheckOutList()}></button>
                        </a>
                        <div className="ch_heading">
                        Cart({
                            cartproductlist?cartproductlist.length:0})
                        </div>
                        <div className="dropdown ch_icon">
                            <a className="dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">
                                <button className="button icon icon-plus-white"></button>
                            </a>
                            <ul className="dropdown-menu custom-drpbox dropdown-menu-right" role="menu" aria-labelledby="menu1">
                                <li role="presentation"><a role="menuitem" tabIndex="-1" data-toggle="modal" data-target="#addnotehere">Add Notes</a></li>
                                <li role="presentation"><a role="menuitem" tabIndex="-1" href="#"  data-toggle="modal"  data-target='#popup_oliver_add_fee'>Add Fee</a></li>
                            </ul>

                        </div>
                    </div>
                </div>
                :null}
            </nav>

        )
    }
}



function mapStateToProps(state) {
  
    const { productlist , attributelist ,categorylist,cartproductlist} = state;
    return {
        productlist:productlist.productlist,
        attributelist:attributelist.attributelist,
        categorylist:categorylist.categorylist,
        cartproductlist:localStorage.getItem("CARD_PRODUCT_LIST") ? JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST") ): [],//cartproductlist.cartproductlist
    };
}

const connectedCommonHeaderTwo = connect(mapStateToProps)(CommonHeaderTwo);
export { connectedCommonHeaderTwo as CommonHeaderTwo };