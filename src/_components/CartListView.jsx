import React from 'react';
import { connect } from 'react-redux';
import { default as NumberFormat } from 'react-number-format'
import { cartProductActions } from '../_actions/cartProduct.action'
import { history } from '../_helpers';
import { checkoutActions } from '../CheckoutPage/actions/checkout.action';
import { discount_calculate } from '../_components/CalculatorFunction';

class CartListView extends React.Component {
    constructor(props) {
        super(props);
        // const {cartproductlist}= this.props
        this.state = {
            //  cartItem: cartproductlist,   //localStorage.getItem("CARD_PRODUCT_LIST") ? localStorage.getItem("CARD_PRODUCT_LIST") : [],
            discountCalculated: 0.0,
            discountAmount: 0,
            discountType: "",
            subTotal: 0.00,
            totalAmount: 0.00,

            // http://app.oliverpos.com/api/ShopSetting/GetRate
            isTaxPercet: false,
            taxRate: "",
            taxAmount: 0.0,
            locaUrl: history.location.pathname,
            addcust: localStorage.getItem('AdCusDetail') ? localStorage.getItem('AdCusDetail') : [],
            TaxId: null
        }
    }



    checkout(ListItem) {
        console.log("ListItem", ListItem)
        const { dispatch } = this.props;
        const { addcust, TaxId, taxRate } = this.state;
        const CheckoutList = {
            ListItem: ListItem,
            customerDetail: addcust,
            totalPrice: this.state.totalAmount,
            discountCalculated: this.state.discountCalculated,
            tax: this.state.taxAmount,
            subTotal: this.state.subTotal,
            TaxId: TaxId,
            TaxRate: taxRate
        }
        if (ListItem.length == 0) {
            $('#checkout1').modal('show');
        } else {
            localStorage.setItem("CHECKLIST", JSON.stringify(CheckoutList))
            dispatch(checkoutActions.checkItemList(CheckoutList))
        }
        //history.push('/checkout')
        //window.location.href = 'checkout'
        //console.log("checkout")
    }

    addCustomer() {
        //  console.log("history LOcation",this.state.locaUrl);      

        var param = this.state.locaUrl;
        var url = '/customerview';
        history.push(url, param)
        window.location = '/customerview'



        //history.push('/customerview');

    }

    deleteAddCust() {
        /// console.log("null");
        localStorage.setItem('AdCusDetail', null)
        console.log("addcust", this.state.addcust);
        this.setState({ addcust: [] })
        //location.reload('true');
    }


    componentWillMount() {
        // this.reload_cart_descount()
        console.log("this.props Add fees", this.props);
        let AdCusDetail = localStorage.getItem('AdCusDetail');
        if (AdCusDetail != null) {
            this.setState({ addcust: JSON.parse(AdCusDetail) })
        }
        setTimeout(function () {

            //Put All Your Code Here, Which You Want To Execute After Some Delay Time.
            setHeightDesktop();

        }, 1000);


    }

    // reload_cart_descount(){
    //     var totalPrice = 0;
    //     let ListItem =  this.props.cartproductlist ? this.props.cartproductlist:[];
    //     ListItem.map((item, index) => {
    //       totalPrice += item.Price
    //   })   

    //     let cart = localStorage.getItem("CART")?JSON.parse(localStorage.getItem("CART")):null
    //     if(cart!==null){
    //    discount_calculate(cart.discountType, cart.discount_amount,ListItem,totalPrice,cart.Tax_rate,this.props,cart.type)
    //     }
    // }

    deleteProduct(item) {
        localStorage.removeItem("PRODUCT")
        var product = this.props.cartproductlist;// JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST"));//

        var i = 0;
        var index;
        for (i = 0; i < product.length; i++) {
            if (product[i].WPID == item.WPID) {
                index = i;
            }
        }

        product.splice(index, 1);
        console.log("product when remove", product)
        if (product.length == 0) {
            localStorage.removeItem("CART");
            localStorage.removeItem("PRODUCT")
        }
        const { dispatch } = this.props;
        dispatch(cartProductActions.addtoCartProduct(product));

        // this.props.handleItemCount()
    }

    componentWillReceiveProps(nextProps) {
        const { taxratelist } = this.props;
        var _subtotal = 0.0;
        var _total = 0.0;
        var _taxAmount = 0.0;
        var _taxRate = 0.0;
        var _totalDiscountedAmount = 0.0;


        if (typeof (taxratelist) !== 'undefined' && (taxratelist) !== null) {

            if (typeof (taxratelist.taxratelist) !== 'undefined') {

                if (taxratelist.taxratelist.TaxRate.indexOf("%") >= -1) {
                    console.log("taxratelist.taxratelist", taxratelist.taxratelist);
                    this.setState({
                        TaxId: taxratelist.taxratelist.TaxId,
                        taxRate: taxratelist.taxratelist.TaxRate
                    })

                    var _taxratelist = "";
                    if (taxratelist.taxratelist != undefined) {
                        _taxratelist = taxratelist.taxratelist.TaxRate.toString();
                        this.state.taxRate = parseFloat(_taxratelist.substring(-1, _taxratelist.length - 1));
                    }

                }
            }
        }
        _taxRate = this.state.taxRate;
        nextProps.cartproductlist && nextProps.cartproductlist.map((item, index) => {
            if (item.Price) {
                _subtotal += parseFloat((typeof item.after_discount !== "undefined") ? item.after_discount : item.Price);
                _totalDiscountedAmount += parseFloat((typeof item.discount_Amount !== "undefined") ? item.discount_Amount : 0);
            }
        })
        //Calculate Discount-----------------------------------
        console.log("after change discount amount", _totalDiscountedAmount)
        // if  (_subtotal ==0)
        // {
        //     _totalDiscountedAmount =0.0;
        // }
        // else
        //  if(nextProps.discountType == "Percentage") {
        //      console.log("SETP 1",nextProps.discountAmount)
        //     if(parseFloat(nextProps.discountAmount)==0){
        //        console.log("SETP 2",_totalDiscountedAmount);
        //         _totalDiscountedAmount;
        //     }else{

        //         _totalDiscountedAmount = ((_subtotal) * parseFloat(nextProps.discountAmount)) / 100 ;
        //         console.log("SETP 3",_totalDiscountedAmount);
        //     }
        // } else {
        //     console.log("SETP 4",nextProps.discountAmount)
        //     if(parseFloat(nextProps.discountAmount)==0){
        //         console.log("SETP 5",_totalDiscountedAmount);
        //     _totalDiscountedAmount;
        //     }
        //    else{
        //     console.log("SETP 6",_totalDiscountedAmount);
        //      _totalDiscountedAmount = ((_subtotal) * (parseFloat(nextProps.discountAmount) * 100 / _subtotal) )/ 100  ;
        //      console.log("SETP 7",_totalDiscountedAmount);
        //     }
        // }

        if (_taxRate) {
            _taxAmount = (_subtotal * _taxRate) / 100.0;

            console.log("_taxAmount", _taxAmount);
            // var perc = 0
            // _taxAmount = (parseFloat(_subtotal) * parseFloat(_taxRate)) / 100.0
            // console.log("_taxAmount", _taxAmount);
        }

        //--------------------------------------------

        _total = _subtotal + _taxAmount;
        // if (nextProps.discountType == "Percentage") {
        //     perc = ((_subtotal + _taxAmount) * parseFloat(nextProps.discountAmount)) / 100
        //     _total = _subtotal + _taxAmount - parseFloat(perc);
        // }
        // else {
        //     _total = _subtotal + _taxAmount - parseFloat(nextProps.discountAmount);
        // }



        this.setState({
            subTotal: _subtotal,
            totalAmount: _total,// parseFloat(_subtotal) - parseFloat(nextProps.discountAmount),           
            discountAmount: nextProps.discountAmount,
            discountType: nextProps.discountType,
            taxAmount: _taxAmount, //(( parseFloat(_subtotal) - parseFloat(nextProps.discountAmount))% parseFloat(this.state.taxRate))*100.0           
            discountCalculated: _totalDiscountedAmount > 0 ? _totalDiscountedAmount : 0

        })

        console.log("taxAmount", this.state.taxAmount);
        // this.props.handleItemCount()

    }

    singlProductDiscount(item) {
        $('#popup_discount').modal('show');
        var data = {
            product: 'product',
            item: item
        }
        this.props.dispatch(cartProductActions.selectedProductDis(data))
    }

    cardProductDiscount() {
        let ListItem = this.props.cartproductlist ? this.props.cartproductlist : [];
        console.log("LENGHT", ListItem)

        if (ListItem.length !== 0) {
            $('#popup_discount').modal('show');
            var data = {
                card: 'card',
            }

            this.props.dispatch(cartProductActions.selectedProductDis(data))
        }else{
            $('#checkout1').modal('show')
        }

    }



    render() {
        var totalPrice = 0;
        let ListItem = this.props.cartproductlist ? this.props.cartproductlist : [];
        console.log("CARTLISTVIEWITEM", ListItem);

        var totalPrice = 0;
        let Addcust = this.state.addcust;
        return (


            <div className="col-lg-3 col-sm-4 col-xs-4 pr-0  plr-8">
                <div className="panel panel-default panel-right-side bb-0 r0 bg-white">
                    {Addcust && Addcust.Content != null ?
                        <div className="panel-heading bg-white">
                            {Addcust.Content.FirstName}{" "} {Addcust.Content.LastName}
                            <span className="pull-right">
                                <img src="assets/img/delete-32.png" className="pointer" onClick={() => this.deleteAddCust()} />
                            </span>
                        </div>
                        : <div className="panel-heading bg-white">
                            Add Customer
                            <span className="pull-right" onClick={(e) => this.addCustomer()}>
                                <img src="assets/img/add_29.png" className="pointer" />
                            </span>
                        </div>
                    }
                    <div className="panel-body p-0 overflowscroll bg-white" id="cart_product_list">
                        <div className="table-responsive">
                            <table className="table CartProductTable">
                                <tbody>
                                    {ListItem && ListItem.map((item, index) => { //ListItem.length > 0 &&
                                        return (
                                            <tr className="" key={index}>
                                                <td>{item.quantity ? item.quantity : 1}</td>
                                                <td>
                                                    {item.Title}
                                                    {/* <span className="comman_subtitle">Red</span> */}
                                                </td>
                                                <td align="right" onClick={() => this.singlProductDiscount(item)}>
                                                    <span>{item.after_discount ? <NumberFormat value={item.discount_Amount == 0 ? null : item.after_discount} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} /> : null}</span>

                                                    <NumberFormat className={item.discount_Amount == 0 ? null : item.after_discount ? 'comman_delete' : ''} value={item.Price} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />
                                                </td>
                                                <td className="pr-0" align="right">
                                                    <img onClick={() => this.deleteProduct(item)} src="assets/img/c_delete.png" />
                                                </td>
                                            </tr>
                                        )
                                    })}
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div className="panel-footer p-0 bg-white">
                        <div className="table-calculate-price">
                            <table className="table ShopViewCalculator">
                                <tbody>
                                    <tr>
                                        <th className="">Sub-Total</th>
                                        <td align="right" className="">
                                            <span className="value pull-right">
                                                {

                                                    ListItem && ListItem.length > 0 && ListItem.map((item, index) => {
                                                        totalPrice += item.Price
                                                    })
                                                }
                                                <NumberFormat value={this.state.subTotal} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th className='w-50'> Tax:
                                        <span className="value pull-right">
                                                <NumberFormat value={this.state.taxAmount} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />
                                            </span>
                                        </th>
                                        <th className="bl-1" align="right">
                                            Discount:
                                            <span className="value pull-right pointer" style={{ color: '#46A9D4' }} data-toggle="modal" onClick={() => this.cardProductDiscount()}>
                                                {
                                                    (this.state.discountCalculated > 0) ? (
                                                        <NumberFormat value={this.state.discountCalculated} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />) : 'ADD'
                                                }
                                            </span>

                                        </th>

                                    </tr>
                                    <tr>
                                        <th colSpan="2" className="p-0">
                                            {/* <button className="btn btn-block btn-primary checkout-items" onClick="window.location.href = 'checkout.html';"> */}
                                            <button className="btn btn-block btn-primary checkout-items" onClick={() => this.checkout(ListItem)}>
                                                <span className="pull-left">
                                                    Checkout
                                                  </span>
                                                <span className="pull-right">
                                                    <NumberFormat value={this.state.totalAmount} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />
                                                </span>
                                            </button>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        )
    }

}




function mapStateToProps(state) {
    console.log("cartViewState", state);
    // const { taxratelist } = state.taxratelist;
    const { cartproductlist, checkout_list, taxratelist, selecteditem } = state;
    console.log("NMS", cartproductlist);
    console.log("NMS1", localStorage.getItem("CARD_PRODUCT_LIST"));
    return {
        cartproductlist: localStorage.getItem("CARD_PRODUCT_LIST") ? JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST")) : [],
        checkout_list: checkout_list.items,
        taxratelist: taxratelist,
        selecteditem: selecteditem
    };
}

const connectedCartListView = connect(mapStateToProps)(CartListView);
export { connectedCartListView as CartListView };