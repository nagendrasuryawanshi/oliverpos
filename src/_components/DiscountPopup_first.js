import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { userActions } from '../_actions';
import { NavbarPage ,CommonHeaderTwo } from '../_components';

import { history } from '../_helpers';
import { discountActions } from '../_actions/discount.action'
import { favouriteListService } from '../ShopView/services/favouriteList.service';

class DiscountPopup extends React.Component {
    constructor(props) {
        super(props);    
     console.log("currentState", this.state);
        this.state = {         
             discountAmount:"",
             discountType:""
              }    

              this.props.onDiscountAmountChange(0,"Number");     
              const { dispatch } = this.props;
              dispatch(discountActions.getAll());  

          
    }    
    
   //============  calculator secion start ================
   calcInp(e) {
   // console.log('The link was clicked.',e);
   
   console.log("clickedValue",e);
   var stateValue=this.state.discountAmount?this.state.discountAmount.toString():"0";
   console.log("State before", stateValue);
        if (e == "c" ) {       
            console.log("length before",stateValue.length);
            if(stateValue.toString().length>-1)
            {
             console.log("lenght2",stateValue.length);
                const txtValue = stateValue.substring(0, stateValue.length-1);
                console.log("txtValue ", txtValue);
              //  alert(txtValue);
                stateValue=txtValue==""?"0":txtValue;   
                    
            }     
        }
        else
        {   console.log("1");
            if(e=="." && stateValue.indexOf(".")>=0)
            {
                console.log("2");
                console.log("donothing",e);
                //do nothing
            }else
            {
                console.log("3");
                stateValue= stateValue=="0"?e : stateValue.toString()+e.toString(); 
            }
           // this.state.discountType= "Number";
        } 
        this.state.discountAmount=stateValue;
      var   cartlist= localStorage.getItem("CARD_PRODUCT_LIST") ? JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST") ):[]

        
        jQuery('#txtdisAmount').val(this.state.discountAmount);
        this.props.onDiscountAmountChange(this.state.discountAmount,this.state.discountType);          
  }
   
handleDiscount() {   
    //   var  cartlist= localStorage.getItem("CARD_PRODUCT_LIST") ? JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST") ):[]
    // console.log("Discountcartlist",cartlist);
    console.log("handleDiscountCancle",this.state.discountType);
    this.props.onDiscountAmountChange(this.state.discountAmount,this.state.discountType);            
}
handleDiscountCancle(e) {   
   //  console.log("handleDiscountCancle");
     
    this.setState({discountAmount:"0",discountType:"Number"})
    this.state.discountAmount="0";
    this.state.discountType="Number";
    jQuery('#txtdisAmount').val(this.state.discountAmount);
   this.props.onDiscountAmountChange('0',"Number");   
   $('#panelCalculatorpopUp :input').removeAttr('disabled');
}

applyfixDiscount(item) {  
   
        $('#panelCalculatorpopUp :input').attr('disabled', true);
    
    jQuery('#panelCalculatorpopUp').val("0"); 
    jQuery('#txtdisAmount').val("0"); 
    if(item)
    {
       this.setState({discountType:item.Type,
        discountAmount:item.Amount
     })
       this.props.onDiscountAmountChange(this.state.discountAmount,this.state.discountType);    //Percentage / Number   
    }   
   
}


    // }
minplus(){
    console.log(jQuery('#spnCalcType').text());
    if(jQuery('#spnCalcType').text() =="%")
    {
        jQuery('#spnCalcType').text("$");
        jQuery('#lblPercent').text("%");
        this.setState({discountAmount:"0",discountType:"Number"})
        this.state.discountType="Number";
    }
    else
    {
        jQuery('#lblPercent').text("$");
        jQuery('#spnCalcType').text("%");
       //this.setState({discountAmount:"0.00",discountType:"Percentage"})
      // this.state.discountType= "Percentage";
       this.setState({discountType:"Percentage"})
       this.state.discountType="Percentage";
       this.props.onDiscountAmountChange(this.state.discountAmount,"Percentage"); 

       console.log("displayPercentage", this.state.discountType);
    }
    
}



    render() {
      //  console.log("this.props", this.props);
        const { discountAmount } = this.props;    
        const { taxratelist } = this.props;    
       
        return (
         
    
    <div id="popup_discount" tabIndex="-1" className="modal modal-wide modal-wide1 fade">
        <div className="modal-dialog" id="dialog-midle-align">
            <div className="modal-content">
                <div className="modal-header">
                    <button type="button" className="close" data-dismiss="modal" aria-hidden="true" onClick={this.handleDiscount()}>
                        <img src="assets/img/delete-icon.png"/>
                    </button>
                    <h4 className="modal-title">Add Discount (Mittens)</h4>
                </div>
                <div className="modal-body p-0">
                    <form className="clearfix">
                        <div className="col-sm-5">
                            <div className="fixedinCalHeight overflowscroll">
                            <div className="pt-3">
                           {
                              
                                this.props.discountlist ?
                                
                                 this.props.discountlist.map((item,index)=>{
                                   // let isSimpleProduct = (item.Type != "simple") ? true : false ;  
                                     return(                                                    
                                        <div key={index} className="button_with_checkbox">
                                        <a type="button" onClick={()=>this.applyfixDiscount(item)} id="oliver_discount" name="radio-group">
                                        <input type="radio" id={"oliver_discount"+index} name="radio-group"/>
                                        <label htmlFor={"oliver_discount"+index} className="label_select_button">{(item.Name.length>15 ? item.Name.substring(0,15)+'...':item.Name )+( item.Type=="Percentage"? item.Amount+"%": item.Type=="Number"?item.Amount:"") }</label>
                                        </a>
                                         </div>
                                    )
                                  })
                                  :<div></div>             
                            
                            } 
                                <div  className="button_with_checkbox">
                                <a type="button" onClick={()=>this.handleDiscountCancle('c')} id="oliver_discount" name="radio-group">
                                    <input type="radio" id="oliver_discount_clear" name="radio-group"/>
                                    <label htmlFor="oliver_discount__clear" className="label_select_button">Clear Discount</label>                           
                                </a>
                                </div>         
                            
                            </div>
                              

                            </div>
                        </div>
                        <div className="col-sm-7 p-0">
                            <div className="panel-product-list" id="panelCalculatorpopUp">
                                <div className="panel panelCalculator">
                                    <div className="panel-body p-0">
                                        <table className="table table-bordered shopViewPopUpCalculator">
                                            <tbody>
                                                <tr>
                                                    <td colSpan="2" className="text-right br-1 bl-1 bt-0">
                                                        <div className="input-group discount-input-group">
                                                            <input type="text" id="txtdisAmount" className="form-control text-right" placeholder="0.00" aria-describedby="basic-addon1"/>
                                                            <span className="input-group-addon AmoutType" id="spnCalcType" name="spnCalcType">$</span>
                                                        </div>
                                                    </td>
                                                    <td className="text-center pointer bt-0" onClick={()=>this.calcInp('c')}>
                                                  
                                                        <button type="button" className="btn btn-default calculate">
                                                            <img width="36" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMS4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDMxLjA1OSAzMS4wNTkiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDMxLjA1OSAzMS4wNTk7IiB4bWw6c3BhY2U9InByZXNlcnZlIiB3aWR0aD0iNTEycHgiIGhlaWdodD0iNTEycHgiPgo8Zz4KCTxnPgoJCTxwYXRoIGQ9Ik0zMC4xNzEsMTYuNDE2SDAuODg4QzAuMzk4LDE2LjQxNiwwLDE2LjAyLDAsMTUuNTI5YzAtMC40OSwwLjM5OC0wLjg4OCwwLjg4OC0wLjg4OGgyOS4yODMgICAgYzAuNDksMCwwLjg4OCwwLjM5OCwwLjg4OCwwLjg4OEMzMS4wNTksMTYuMDIsMzAuNjYxLDE2LjQxNiwzMC4xNzEsMTYuNDE2eiIgZmlsbD0iIzRiNGI0YiIvPgoJPC9nPgoJPGc+CgkJPHBhdGggZD0iTTE2LjAxNywzMS4wNTljLTAuMjIyLDAtMC40NDUtMC4wODMtMC42MTctMC4yNUwwLjI3MSwxNi4xNjZDMC4wOTgsMTUuOTk5LDAsMTUuNzcsMCwxNS41MjkgICAgYzAtMC4yNCwwLjA5OC0wLjQ3MSwwLjI3MS0wLjYzOEwxNS40LDAuMjVjMC4zNTItMC4zNDEsMC45MTQtMC4zMzIsMS4yNTUsMC4wMmMwLjM0LDAuMzUzLDAuMzMxLDAuOTE1LTAuMDIxLDEuMjU1TDIuMTYzLDE1LjUyOSAgICBsMTQuNDcxLDE0LjAwNGMwLjM1MiwwLjM0MSwwLjM2MSwwLjkwMiwwLjAyMSwxLjI1NUMxNi40OCwzMC45NjgsMTYuMjQ5LDMxLjA1OSwxNi4wMTcsMzEuMDU5eiIgZmlsbD0iIzRiNGI0YiIvPgoJPC9nPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo="></img>
                                                        </button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td className="td-calc-padding br-1 bl-1">
                                                        <button type="button" onClick={()=>this.calcInp(1)} className="btn btn-default calculate">1</button>
                                                    </td>
                                                    <td className="td-calc-padding br-1">
                                                        <button type="button" onClick={()=>this.calcInp(2)} className="btn btn-default calculate">2</button>
                                                    </td>
                                                    <td className="td-calc-padding">
                                                        <button type="button" onClick={()=>this.calcInp(3)} className="btn btn-default calculate">3</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td className="td-calc-padding br-1 bl-1">
                                                        <button type="button" onClick={()=>this.calcInp(4)} className="btn btn-default calculate">4</button>
                                                    </td>
                                                    <td className="td-calc-padding br-1">
                                                        <button type="button" onClick={()=>this.calcInp(5)} className="btn btn-default calculate">5</button>
                                                    </td>
                                                    <td className="td-calc-padding">
                                                        <button type="button" onClick={()=>this.calcInp(6)} className="btn btn-default calculate">6</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td className="td-calc-padding br-1 bl-1">
                                                        <button type="button" onClick={()=>this.calcInp(7)} className="btn btn-default calculate">7</button>
                                                    </td>
                                                    <td className="td-calc-padding br-1">
                                                        <button type="button" onClick={()=>this.calcInp(8)} className="btn btn-default calculate">8</button>
                                                    </td>
                                                    <td className="td-calc-padding">
                                                        <button type="button" onClick={()=>this.calcInp(9)} className="btn btn-default calculate">9</button>
                                                    </td>
                                                </tr>
                                                <tr>
                                                   <td className="td-calc-padding br-1 bl-1" > 
                                                    <button type="button" onClick={()=>this.minplus()} className="btn btn-default calculate"> <label id="lblPercent">%</label></button>
                                                       
                                                    </td>
                                                    <td className="td-calc-padding br-1">
                                                        <button type="button" onClick={()=>this.calcInp('.')} className="btn btn-default calculate">,</button>
                                                    </td>
                                                    <td className="td-calc-padding">
                                                        <button type="button" onClick={()=>this.calcInp(0)} className="btn btn-default calculate">0</button>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div className="modal-footer p-0">
                    <button type="button" className="btn btn-primary btn-block h66" data-dismiss="modal" onClick={this.handleDiscount()}>ADD DISCOUNT</button>
                </div>
            </div>
        </div>
    </div>
   
         
        );
    }
}

function mapStateToProps(state) {
    console.log("shopview state",state);
    const { discountlist } = state.discountlist;
    return {
        discountlist
    };
}

const connectedShopView = connect(mapStateToProps)(DiscountPopup);
export { connectedShopView as DiscountPopup };