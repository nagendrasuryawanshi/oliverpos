import React from 'react';
import { connect } from 'react-redux';
import { default as NumberFormat } from 'react-number-format';
import { getOrderPayments } from '../Checkout';
import { history } from '../../_helpers';


class CheckoutViewFirst extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            totalPrice:'',
            payments:null,
            count:null,
            paid_amount:null
           
        }
    }

    componentWillMount() {
        let checkList = this.props.checkList;
        this.setState({
            totalPrice: checkList.totalPrice
        })
        let oreder_id=0;
        if(localStorage.getItem("oliver_order_payments") && checkList && checkList.totalPrice){
        
        let chec = getOrderPayments(oreder_id ,checkList.totalPrice) 
           if(chec!=null){
            this.setState({
                totalPrice: chec.remainning_amount,
                payments: chec.payments,
                count:chec.number_of_payments,
                paid_amount:chec.paid_amount
            
            })
    }
    }
    }

    AddCustomer(){
        var  param='checkout';
        var  url='/customerview';
         history.push(url,param)
    }

    RemoveCustomer() {
        localStorage.removeItem('AdCusDetail')
        let list = localStorage.getItem('CHECKLIST')?JSON.parse(localStorage.getItem('CHECKLIST')):null;
        if(list!=null){
        const CheckoutList = {
            ListItem: list.ListItem,
            customerDetail: null,
            totalPrice: list.totalPrice,
            discountCalculated: list.discountCalculated,
            tax: list.taxAmount,
            subTotal: list.subTotal,
            TaxId: list.tax
        }
        localStorage.setItem('CHECKLIST', JSON.stringify(CheckoutList))
     }
    }


    render() {
        const { checkout_list, checkList, paying_amount, paying_type } = this.props;
        const { totalPrice ,payments ,count ,paid_amount } = this.state;
        return (
            <div className="col-lg-3 col-sm-4 col-xs-4 pl-0">
                <div className="panel panel-default panel-right-side r0 br-1 bb-0">
                    {checkList && checkList.customerDetail && checkList.customerDetail.Content ?
                        <div className="panel-heading pr-0">
                            {checkList.customerDetail.Content.FirstName}{" "}{checkList.customerDetail.Content.LastName}
                            <span className="pull-right pointer pl-15 pr-15" onClick={()=>this.RemoveCustomer()}>
                                <img src="assets/img/delete-32.png" className="" />
                            </span>
                        </div>
                        :
                        <div className="panel-heading pr-0">
                            Add Customer
                        <span className="pull-right pointer pl-15 pr-15" onClick={()=>this.AddCustomer()}>
                                <img src="assets/img/add_32.png" className="" />
                            </span>
                        </div>
                    }


                    <div className="panel-body p-0 overflowscroll" id="cart_product_list">
                        <div className="table-responsive">
                            <table className="table ListViewCartProductTable">
                                <colgroup>
                                    <col width="10" />
                                    <col width="*" />
                                    <col width="*" />
                                </colgroup>
                                <tbody>

                                    {checkList && checkList.ListItem &&
                                        checkList.ListItem.map((product, index) => {
                                            return (
                                                <tr key={index}>
                                                    <td>{index + 1}</td>
                                                    <td>{product.Title}
                                                        <span className="comman_subtitle">{product.color}  {product.size ? ',' + product.size : null}</span>
                                                    </td>
                                                    <td align="right">
                                                        {product.Price}
                                                        {/* <del><span className="comman_delete">{product.RegularPrice == product.Price?'':product.RegularPrice}</span></del> */}
                                                    </td>
                                                </tr>
                                            )
                                        })
                                    }
                                </tbody>
                            </table>

                        </div>
                    </div>
                    <div className="panel-footer p-0 bg-white">
                        <div className="table-calculate-price">
                            <table className="table ShopViewCalculator">
                                <tbody>
                                    <tr>
                                        <th className="bt-0">Sub-Total</th>
                                        <td align="right" className="bt-0">
                                            <span className="value">
                                                <NumberFormat value={checkList && checkList.subTotal} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />
                                            </span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>
                                            Tax:
                                           <span className="value pull-right">
                                            <NumberFormat value={checkList && checkList.tax} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />
                                             </span>
                                        </th>
                                        <th className="bl-1" align="right">
                                            Discount:
                                           <span className="value pull-right pointer" style={{ color: '#46A9D4' }}>
                                                {checkList && checkList.discountCalculated}
                                            </span>
                                        </th>
                                    </tr>
                                    {/*  add payment */}
                                    {payments && count && paid_amount &&
                                    <tr id="paymentTr">
                                        <th>
                                            <div className="relDiv show_payment_box">
                                                <span className="value pointer" style={{ color: '#46A9D4' }} id="totalPayment">
                                                  {count} Payments   </span>
                                                <div className="absDiv">
                                                    <div className="payment_box">
                                                        <h1 className="m-0">Payments</h1>
                                                        <div className="row" id="totalPaymentSrc">
                                                        {payments.map((item,index)=>{
                                                            return(
                                                                <div key={index} className="col-sm-12 p-0">
                                                                  <label className="col-sm-6">{item.payment_type}</label>
                                                                   <div className="col-sm-6"><NumberFormat value={item.payment_amount} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} /></div>
                                                               </div>
                                                            )
                                                        })}
                                                           
                                                       </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </th>
                                        <td align="right" id="paymentLeft">
                                            {paid_amount}                                
                                       </td>
                                    </tr>
                                    }
                                    {/* End payment */}
                                    <tr>
                                        <th colSpan="2" className="p-0">
                                            <button className="btn btn-block btn-primary total_checkout bg-blue">
                                                <span className="pull-left">
                                                    Balance Due
                                                 </span>
                                                <span className="pull-right">
                                                    <NumberFormat value={totalPrice} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} />

                                                </span>
                                            </button>
                                        </th>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}


function mapStateToProps(state) {
    const { checkoutlist, checkout_list } = state;
    return {
        checkoutlist,
        checkout_list: checkout_list.items
    };
}
const connectedCheckoutViewFirst = connect(mapStateToProps)(CheckoutViewFirst);
export { connectedCheckoutViewFirst as CheckoutViewFirst };