import React from 'react';
import { connect } from 'react-redux';
import { checkoutActions } from '../actions/checkout.action';
import { CommonHeaderFirst } from '../../_components';
import { CheckoutViewFirst } from './CheckoutViewFirst';
import { CheckoutViewSecond } from './CheckoutViewSecond';
import { CheckoutViewThird } from './CheckoutViewThird';
import { customerActions } from  '../../CustomerPage/actions/customer.action';
import { setOrderPayments ,getOrderPayments } from '../Checkout';
import { cartProductActions } from '../../_actions';
import { default as NumberFormat } from 'react-number-format';


class CheckoutView extends React.Component {
    constructor(props) {
        super(props);
          this.state={
            user_fname:'',
            user_lname:'',
            user_address:'',
            user_city:'',
            user_contact:'',
            user_notes:'',
            user_email:'',
            user_pcode:'',
            user_id:'',
            UDID:'',
            paying_type:'',
            paying_amount:'',
            checkList:null,
            add_note:null,
            paid_amount:null,
            location_id:null,
            lay_Away:null,
            final_place_order:null,
            store_credit:null,
            
           }

          this.handleChange = this.handleChange.bind(this);
          this.handleSubmit = this.handleSubmit.bind(this);
          this.getPayment = this.getPayment.bind(this);
          this.setPayment = this.setPayment.bind(this);
          this.handleSubmitNote = this.handleSubmitNote.bind(this);
          this.layAwayPayment = this.layAwayPayment.bind(this);
         
    }

   

    handleSubmitNote() {
        this.setState({add_note:this.state.add_note})
        this.setPayment(this.state.user_id)
        $(".close").click();
    }

    getPayment(type, amount){
        this.setState({
            paying_type : type,
            paying_amount : amount,
        });        
    }

    layAwayPayment(type ,amount){
         this.setState({
            lay_Away:type,
            store_credit:amount
        })
       
      
        if(type == 'lay_away'){
              $('.addnoctehere').modal();
           }  
        
        
    }

    componentWillMount(){
        let checkList = localStorage.getItem('CHECKLIST');
        let location_id = localStorage.getItem('Location')
        console.log("CHECKLIST",JSON.parse(checkList))
        this.setState({
            checkList:JSON.parse(checkList),
            location_id:location_id,
           })
       
    }
   
    componentDidMount(){
        setTimeout(function(){

            //Put All Your Code Here, Which You Want To Execute After Some Delay Time.
            setHeightDesktop();
        
          }, 1000);

        let AdCusDetail = localStorage.getItem('AdCusDetail');
        const UID = localStorage.getItem('UDID');
        this.setState({UDID:UID})
        if (AdCusDetail != null) {
             let checkoutList = JSON.parse(AdCusDetail) && JSON.parse(AdCusDetail).Content;
          //  console.log("AdCusDetail",checkoutList)
            if(checkoutList!=null){
            this.setState({
                user_fname:checkoutList.FirstName?checkoutList.FirstName:'',
                user_lname:checkoutList.LastName,
                user_address:checkoutList.StreetAddress,
                user_city:checkoutList.city_name,
                user_contact:checkoutList.Phone,
                user_notes:checkoutList.Notes,
                user_email:checkoutList.Email,
                user_pcode:checkoutList.Pincode,
                user_id:checkoutList.Id,
               
            })
        }
        }

         
           
      
    }

    handleChange(e) {
        const { name, value } = e.target;
        this.setState({ [name]: value });
    }

    handleSubmit(e) {
        e.preventDefault();
        const {UDID,user_id, user_address,user_city,user_contact,user_email,user_fname,user_lname,user_notes,user_pcode } = this.state;
        const { dispatch } = this.props;
        if (UDID,user_id,user_address && user_city && user_contact && user_email && user_fname && user_lname && user_notes && user_pcode) {
            //dispatch(userActions.login(username, password));
           // console.log("suceess",this.props)
            const update = {
                Id: user_id,
                FirstName: user_fname,
                LAstName: user_lname,
                Contact: user_contact,
                startAmount: 0,
                Email: user_email,
                Udid: UDID,
                Notes: user_notes,
                StreetAddress: user_address,
                Pincode: user_pcode,
                City: user_city
            }
            //console.log("update", update)
            customerActions.save(update);
            $(".close").click();
           
        }
    }

    setPayment(user_id){
        const { 
            user_address,user_city,user_contact,user_email,user_fname,user_lname,user_notes,user_pcode ,
            paying_amount , paying_type ,checkList ,add_note ,paid_amount,location_id,UDID ,lay_Away,history
         } = this.state;
         const { dispatch } = this.props;
         let checkoutList = checkList && checkList.customerDetail && checkList.customerDetail.Content;
         let chec;
         let oliver_order_payments ;
         let place_order;
         let order_payments = [] ;
         let complete_order_payments = [];
         let newList = [] ;
         let totalPrice = checkList.totalPrice;
         let oreder_id = 0;
         let status ;
         let storeCredit = checkoutList?checkoutList.StoreCredit:'';
        

         console.log("paying_amount" ,paying_amount)
         console.log("totalPrice" ,totalPrice )
         console.log("id" , user_id)
        console.log("this.props" , checkList)
        
         status = lay_Away?lay_Away:'park_sale';
         console.log("lay_Away",status)
        //  if(lay_Away == 'store_credit' && paying_amount!=null && paying_amount!=0){
        //     console.log("STEP 0",paying_amount)
        //     console.log("STEP 1" ,storeCredit)
        //     if(storeCredit >= paying_amount){ 
        //         console.log("STEP 2")
        //         setOrderPayments(oreder_id,paying_type,paying_amount,totalPrice)
        //         oliver_order_payments = JSON.parse(localStorage.getItem("oliver_order_payments"))
        //          console.log("oliver_order_payments12",oliver_order_payments) 
            
        //             oliver_order_payments.map(items=>{
        //                 order_payments.push(
        //                     {"amount":items.payment_amount,"payment_type":"order","type":items.payment_type}
        //                 )
        //             })
        //             if(!order_payments.length == 0){
        //                 let CHECKLIST = {
        //                     ListItem:checkList.ListItem,
        //                     TaxId:checkList.TaxId,
        //                     customerDetail:{
        //                     Content : {
        //                         AccountBalance:checkoutList.AccountBalance?checkoutList.AccountBalance:'',
        //                         City:checkoutList.City?checkoutList.City:'',
        //                         Email:checkoutList.Email?checkoutList.Email:'',
        //                         FirstName:checkoutList.FirstName?checkoutList.FirstName:'',
        //                         Id:user_id?user_id:'',
        //                         LastName:checkoutList.LastName?checkoutList.LastName:'',
        //                         Notes:checkoutList.Notes?checkoutList.Notes:'',
        //                         Phone:checkoutList.Phone?checkoutList.Phone:'',
        //                         Pin:checkoutList.Pin?checkoutList.Pin:'',
        //                         Pincode:checkoutList.Pincode?checkoutList.Pincode:'',
        //                         StoreCredit:storeCredit-paying_amount,
        //                         StreetAddress:checkoutList.StreetAddress?checkoutList.StreetAddress:'',
        //                         UID:checkoutList.UID 
        //                     }
        //                 },
        //                     discountCalculated:checkList.discountCalculated,
        //                     subTotal:checkList.subTotal,
        //                     tax:checkList.tax,
        //                     totalPrice:checkList.totalPrice,
        //                 }
        //                 console.log("STEP CHECKLIST",CHECKLIST)
        //                 localStorage.setItem("CHECKLIST",JSON.stringify(CHECKLIST));
        //               this.props.dispatch(customerActions.removeStoreCredit(UDID,checkoutList.UID,storeCredit-paying_amount))
        //             //window.location = '/checkout'; 
        //             }
        //         }
        //    if(storeCredit < paying_amount) {
        //     console.log("STEP 3")
        //     setOrderPayments(oreder_id,paying_type,paying_amount-storeCredit,totalPrice)
        //     oliver_order_payments = JSON.parse(localStorage.getItem("oliver_order_payments"))
        //      console.log("oliver_order_payments12",oliver_order_payments) 
        
        //         oliver_order_payments.map(items=>{
        //             order_payments.push(
        //                 {"amount":items.payment_amount,"payment_type":"order","type":items.payment_type}
        //             )
        //         })
        //         if(!order_payments.length == 0){
                  
                   
        //             let CHECKLIST = {
        //                 ListItem:checkList.ListItem,
        //                 TaxId:checkList.TaxId,
        //                 customerDetail:{
        //                 Content : {
        //                     AccountBalance:checkoutList.AccountBalance?checkoutList.AccountBalance:'',
        //                     City:user_city?user_city:'',
        //                     Email:user_email?user_email:'',
        //                     FirstName:user_fname?user_fname:'',
        //                     Id:user_id?user_id:'',
        //                     LastName:user_lname?user_lname:'',
        //                     Notes:user_notes?user_notes:'',
        //                     Phone:user_contact?user_contact:'',
        //                     Pin:checkoutList.Pin?checkoutList.Pin:'',
        //                     Pincode:user_pcode?user_pcode:'',
        //                     StoreCredit:0,
        //                     StreetAddress:user_address?user_address:'',
        //                     UID:checkoutList.UID 
        //                 }
        //             },
        //                 discountCalculated:checkList.discountCalculated,
        //                 subTotal:checkList.subTotal,
        //                 tax:checkList.tax,
        //                 totalPrice:checkList.totalPrice,
        //             }
        //             localStorage.setItem("CHECKLIST",JSON.stringify(CHECKLIST));
        //             this.props.dispatch(customerActions.removeStoreCredit(UDID,checkoutList.UID,0))
        //          }
        //    }     
        // }

         if(paying_type!='' && paying_amount != 0 ){ 

            setOrderPayments(oreder_id,paying_type,paying_amount,totalPrice)
            oliver_order_payments = JSON.parse(localStorage.getItem("oliver_order_payments"))
             console.log("oliver_order_payments12",oliver_order_payments) 
        
                oliver_order_payments.map(items=>{
                    order_payments.push(
                        {"amount":items.payment_amount,"payment_type":"order","type":items.payment_type}
                    )
                }) 
               
         // window.location = '/checkout';
         if(!oliver_order_payments.length == 0 && totalPrice){
        
            chec = getOrderPayments(oreder_id ,totalPrice) 
            console.log("oliver_order_payments firdt",chec) 
                if(paying_type!='' && chec.remainning_amount == 0 ){
                    oliver_order_payments.map(items=>{
                        complete_order_payments.push({
                           "amount":items.payment_amount,"payment_type":"refund","type":items.payment_type
                        })
                      }) 
                   // console.log("full pay amount");
                    this.setState({lay_Away:'completed'})
                    status = 'completed'
                    //window.location = '/checkout';
                    // localStorage.removeItem('CHECKLIST');
                    // localStorage.removeItem('oliver_order_payments');
                }
          }
         }
        oliver_order_payments = localStorage.getItem("oliver_order_payments")!=null ?JSON.parse(localStorage.getItem("oliver_order_payments") ):[]
        console.log("oliver_order_payments123",oliver_order_payments) 
        oliver_order_payments.map(items=>{
            order_payments.push(
                {"amount":items.payment_amount,"payment_type":"order","type":items.payment_type}
            )
        }) 
        chec = getOrderPayments(oreder_id ,totalPrice) 
    console.log("REmain Amounr " ,chec)
     console.log("order_payments original",order_payments)
     console.log("paid_amount " ,paid_amount)
       checkList.ListItem.map(items=>{
            newList.push({
            line_item_id:0,
            name:items.Title,
            product_id:items.WPID,
            quantity:1,
            subtotal:items.Price,
            subtotal_tax : parseFloat(items.Price) * (parseFloat(checkList.TaxRate) / 100),
            total:items.Price,
            total_tax : parseFloat(items.Price) * (parseFloat(checkList.TaxRate) / 100),
            variation_id:0
            })
        })
        console.log("newList " ,newList)
        place_order = {
            order_id:oreder_id,
            status:status,
            customer_note:user_notes?user_notes:'Add Note',
            customer_id:user_id?user_id:null,
            order_tax:checkList.tax?checkList.tax:'',
            order_total:totalPrice,
            order_discount:checkList.discountCalculated?checkList.discountCalculated:'',
            tax_id:checkList.TaxId,
            line_items:newList,
            billing_address:
               [{
                    first_name : user_fname?user_fname:'',
                    last_name : user_lname?user_lname:'',
                    company : "NMS",
                    email : user_email?user_email:'',
                    phone: user_contact?user_contact:'',
                    address_1 :user_address?user_address:'',
                    address_2 : user_address?user_address:'',
                    city : user_city?user_city:'',
                    state : "MP",
                    postcode :user_pcode?user_pcode:'',
                    country : "IN"
            }],
            shipping_address:[{
                first_name : user_fname?user_fname:'',
                last_name : user_lname?user_lname:'',
                company : "NMS",
                email : user_email?user_email:'',
                phone: user_contact?user_contact:'',
                address_1 :user_address?user_address:'',
                address_2 : user_address?user_address:'',
                city : user_city?user_city:'',
                state : "MP",
                postcode :user_pcode?user_pcode:'',
                country : "IN"
            }],
            order_custom_fee:[],
            order_notes:[add_note],
            order_payments:complete_order_payments.length>0?complete_order_payments:order_payments,
            order_meta:[{
                manager_id : 1 ,
                location_id : location_id ,
                register_id : 4 ,
                cash_rounding : 0 ,
                refund_cash_rounding : 0
        }],
            Udid:UDID
        }
       
        if(paying_type =='' && paying_amount == '' && add_note!=''){
          
            console.log("when park sale" ,place_order)
            dispatch(checkoutActions.save(place_order ,3))
           
          }
          
       
        if(paying_type!='' && !paying_amount == 0  && !add_note == 0){
            console.log("when add amount and park sale" ,place_order)
             dispatch(checkoutActions.save(place_order,2))
          
        }
        if(paying_type!='' && paying_amount == totalPrice || chec.remainning_amount == 0){
            console.log("when paying_amount equal totalPrice" ,place_order)
            this.setState({final_place_order:place_order})
             // window.location='/complete_refund';
            // $('popup_cash_rounding').click();
            //alert();
            $('#popup_cash_rounding').modal('show');
        }

         if(paying_type!='' && paying_amount > 0 && chec.remainning_amount == 0){
           console.log("when full payment" ,place_order)
            this.setState({final_place_order:place_order})
           // window.location='/complete_refund';
            // $('popup_cash_rounding').click();
           // alert();
            $('#popup_cash_rounding').modal('show');

            //dispatch(checkoutActions.save(place_order ,1))
           // this.removeCheckOutList()
         }
         if(paying_type!='' && !paying_amount == 0 && chec.remainning_amount != 0){
            console.log("when add amount diffrent type" ,place_order)
            window.location='/checkout'
           // history.push('/checkout');
        }
       
          
    }

    removeCheckOutList(){
        alert("REMOVE CHECKLIST IN CHECKOUT PAGE")
        localStorage.removeItem('CHECKLIST');
        localStorage.removeItem('oliver_order_payments');
        const { dispatch } = this.props;
        dispatch(cartProductActions.addtoCartProduct(null)); 
    }

    finalAdd(){
        this.props.dispatch(checkoutActions.save(this.state.final_place_order ,1))
    }

    render() {
        const {checkList,paying_amount,paying_type, UDID,user_id,user_address,user_city,user_contact,user_email,user_fname,user_lname,user_notes,user_pcode } = this.state;
    
        return (
            <div>
                <div className="wrapper">
                    <div id="content">
                        <CommonHeaderFirst {...this.props} />
                        <div className="inner_content bg-light-white clearfix">
                            <div className="content_wrapper">
                                <CheckoutViewFirst checkList={checkList} paying_amount={paying_amount} paying_type={paying_type}/>
                                <div className="col-lg-9 col-sm-8 col-xs-8 p-0">
                                    <div className="pl-95 clearfix">
                                       <CheckoutViewSecond {...this.props} addPayment={this.layAwayPayment} getPayment={this.getPayment}/>
                                       <CheckoutViewThird {...this.props} checkList={checkList} addPayment={this.getPayment}/>
                                    </div>
                                </div>
                            </div>
                            <div className="mt-4 text-right pr-0" onClick={this.setPayment.bind(this, user_id)}>
                                <div className="full_height_button bg-blue" style={{ backgroundImage: 'url(assets/img/text_blue.png)' }}>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                
                <div id="edit-info" className="modal modal-wide fade full_height_one">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">
                                    <img src="assets/img/delete-icon.png" />
                                </button>
                                <h4 className="modal-title">Edit Info</h4>
                            </div>
                            <div className="modal-body overflowscroll">
                                <form className="clearfix form_editinfo">
                                    <div className="col-sm-6">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <div className="input-group-addon">
                                                    First Name
                                                 </div>
                                                <input className="form-control" value={user_fname?user_fname:''} id="user_fname" name="user_fname" type="text"  onChange={this.handleChange}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <div className="input-group-addon">
                                                    Last Name
                                                  </div>
                                                <input className="form-control" value={user_lname?user_lname:''} id="user_lname" name="user_lname" type="text"  onChange={this.handleChange}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <div className="input-group-addon">
                                                    Email
                                                  </div>
                                                <input className="form-control" value={user_email?user_email:''} id="user_email" name="user_email" type="text"  onChange={this.handleChange}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <div className="input-group-addon">
                                                    Phone Number
                                                 </div>
                                                <input className="form-control" value={user_contact?user_contact:''} id="user_contact" name="user_contact" type="text" onChange={this.handleChange} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <div className="input-group-addon">
                                                    Street Address&nbsp;
                                                 </div>
                                                <input className="form-control" value={user_address?user_address:''} id="user_address" name="user_address" type="text"  onChange={this.handleChange}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <div className="input-group-addon">
                                                    City&nbsp;&nbsp;
                                                 </div>
                                                <input className="form-control" id="user_city" value={user_city?user_city:''} name="user_city" type="text" onChange={this.handleChange} />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-6">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <div className="input-group-addon">
                                                    Post Code
                                                </div>
                                                <input className="form-control" id="user_pcode" name="user_pcode" value={user_pcode?user_pcode:''} type="text" onChange={this.handleChange}/>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm-12">
                                        <div className="form-group">
                                            <div className="input-group">
                                                <div className="input-group-addon">
                                                    Notes
                                                </div>
                                                <textarea className="form-control" id="user_notes" value={user_notes?user_notes:''} name="user_notes" type="text" onChange={this.handleChange}>Testing</textarea>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div className="modal-footer p-0">
                                <button type="button" onClick={this.handleSubmit} className="btn btn-primary btn-block h66">SAVE & UPDATE</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div tabIndex="-1" className="modal fade in mt-5 modal-wide addnoctehere" id="addnotehere" role="dialog">
                        <div className="modal-dialog">
                            <div className="modal-content">
                                <div className="modal-header bb-0">
                                    <button type="button" className="close opacity-1 mt-1" data-dismiss="modal" aria-hidden="true">
                                        <img src="assets/img/delete-icon.png"/>
                                    </button>
                                    <h1 className="modal-title">Add Note</h1>
                                </div>
                                <div className="modal-body p-0">
                                    <textarea className="form-control modal-txtArea" name="add_note" onChange={this.handleChange} placeholder="Enter Note Here" ></textarea>
                                </div>
                                <div className="modal-footer p-0 b-0">
                                    <button onClick={this.handleSubmitNote} type="button" className="btn btn-primary btn-block btn-primary-cus pt-2 pb-2">SAVE & CLOSE</button>
                                </div>
                            </div>
                        </div>
                    </div>
                
               <div id="popup_cash_rounding" className="modal modal-wide modal-wide1 cancle_payment">
                        <div className="modal-dialog" id="dialog-midle-align">
                            <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">
                                    <img src="assets/img/delete-icon.png"/>
                                </button>
                                <h4 className="modal-title">Cash</h4>
                            </div>
                            <div className="modal-body p-0">
                                <h3 className="popup_payment_error_msg" id="popup_cash_rounding_error_msg">
                                   <div className="row font18 mb-3">
                                      <div className="col-sm-6 text-left"> 
                                        Total
                                       <span className="pull-right">: </span>
                                      </div>
                                   <div className="col-sm-6">  <NumberFormat value={checkList && checkList.totalPrice} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} /> </div>
                                </div>
                                <hr/>
                                <div className="row font18 mb-3" style={{display:'none'}}>
                                    <div className="col-sm-6 text-left">Payment (Cash Rounding)
                                       <span className="pull-right">:</span>
                                    </div>
                                   <div className="col-sm-6">{0.05}</div>
                                </div>
                                <div className="row font18">
                                 <div className="col-sm-6 text-left">Payment (Cash) 
                                    <span className="pull-right">:</span>
                                    </div>
                                  <div className="col-sm-6">{paying_amount}</div>
                                 </div> <hr/><div className="row font18">
                                <div className="col-sm-6 text-left">Balance 
                                <span className="pull-right">:</span>
                                </div>
                                <div className="col-sm-6" id="balance_left"> <NumberFormat value='0.00' displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} /></div>
                                </div>
                                </h3>
                            </div>
                            <div className="modal-footer" style={{borderTop:0}} onClick={()=>this.finalAdd()}>
                                  <button type="button" className="btn btn-primary btn-block h66" style={{height: 70}} data-dismiss="modal" aria-hidden="true" id="popup_cash_rounding_button">Add Payment</button>
                                </div>
                            </div>
                        </div>
                   </div>
          
            </div>
        );
    }
}

function mapStateToProps(state,props) {
    const { checkoutlist ,checkout_list } = state;
    return {
        checkoutlist,
        checkout_list:checkout_list.items
    };
}
const connectedCheckoutView = connect(mapStateToProps)(CheckoutView);
export { connectedCheckoutView as CheckoutView };