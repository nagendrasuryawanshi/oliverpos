import React from 'react';
import { connect } from 'react-redux';
import { default as NumberFormat } from 'react-number-format'
import { getOrderPayments } from '../Checkout'




class CheckoutViewThird extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            payingAmount: 0,
            refundPayments: 0,
            emptyPaymentFlag: true,
            checkList: null,
            paidAmount: null
        }
        this.addPayment = this.addPayment.bind(this);
        this.calcInp = this.calcInp.bind(this);
    }


    SetValue(eleT, val) {
        let v1 = this.state.paidAmount + val;
        eleT.html(eleT.html().replace(' ', '') + val + ' ');
        this.setState({ paidAmount: v1 })

    }

    calcInp(input) {
        var elemJ = jQuery('#calc_output');
        if (this.state.emptyPaymentFlag || elemJ.html() == "0") {
            elemJ.html('');
            this.setState({
                emptyPaymentFlag: false,
            });
        }

        switch (input) {
            case 1:
                this.SetValue(elemJ, '1');
                break;
            case 2:
                this.SetValue(elemJ, '2');
                break;
            case 3:
                this.SetValue(elemJ, '3');
                break;
            case 4:
                this.SetValue(elemJ, '4');
                break;
            case 5:
                this.SetValue(elemJ, '5');
                break;
            case 6:
                this.SetValue(elemJ, '6');
                break;
            case 7:
                this.SetValue(elemJ, '7');
                break;
            case 8:
                this.SetValue(elemJ, '8');
                break;
            case 9:
                this.SetValue(elemJ, '9');
                break;
            case 0:
                this.SetValue(elemJ, '0');
                break;
            case '.':
                if (elemJ.html() == "") {
                    this.SetValue(elemJ, '0');
                } else {
                    if (elemJ.html().includes(".")) {

                    } else {
                        this.SetValue(elemJ, '.');
                    }
                }
                break;
            //alert('n');
            // default code block
        }
    }

    addPayment(type) {
        let amount = $('#calc_output').text();
        this.props.addPayment(type, amount);
    }


    rmvInp() {

        let str = $('#calc_output').html();
        str = str.substring(0, str.length - 2);
        $('#calc_output').html(str + ' ');
        if (str == "" || str == " ") {
            $('#calc_output').html('0');
        } else { }

        this.setState({ paidAmount: str })
        this.props.addPayment(str);
    }

    setRefundAmount(amount) {
        this.setState({
            payingAmount: amount,
        });
    }
 

    componentWillMount() {
         let checkList = this.props.checkList;
         let oreder_id=0;
         this.setState({
            payingAmount: checkList.totalPrice
         })
        //  let chec = getOrderPayments(oreder_id ,checkList.totalPrice) 
        //  console.log("oliver_order_payments34",chec) 
       
        // this.setState({
        //       payingAmount: chec.remainning_amount
        // })
        if(localStorage.getItem("oliver_order_payments") && checkList && checkList.totalPrice){
        
            let chec = getOrderPayments(oreder_id ,checkList.totalPrice) 
            if(chec!=null){
            this.setState({
                payingAmount: chec.remainning_amount,
              
            })
        }
        }
    }

    render() {
        const { paidAmount, payingAmount } = this.state;
        return (
            <div className="col-lg-6 col-sm-6 col-xs-12 pt-4 plr-8">
                <div className="items preson_info">
                    <div className="item-heading text-center font24">Payment Type</div>
                    <div className="panel panel-default panel-product-list bt-0 p-0 relDiv" id="buttonGroupPanel" style={{ borderColor: '#979797' }}>
                        <div className="pl-3 pr-3 clearfix overflowscroll" id="buttonGroup">
                            <form action="#">
                                <div className="w-50-block button_with_checkbox mt-1">
                                    <input type="radio" id="test1" value="cash" name="radio-group" onClick={this.addPayment.bind(this, "cash")} />
                                    <label htmlFor="test1" className="label_select_button">Cash</label>
                                </div>
                                <div className="w-50-block button_with_checkbox mt-1">
                                    <input type="radio" id="test2" value="Debit/Credit" name="radio-group" onClick={this.addPayment.bind(this, "debit/credit")} />
                                    <label htmlFor="test2" className="label_select_button">Debit / Credit Card</label>
                                </div>
                                <div className="w-50-block button_with_checkbox mt-1">
                                    <input type="radio" id="gift_card" value="Gift"  name="radio-group" onClick={this.addPayment.bind(this, "gift")}/>
                                    <label htmlFor="gift_card" className="label_select_button">Gift Card</label>
                                </div>
                                <div className="w-50-block button_with_checkbox mt-1">
                                    <input type="radio" id="others" value="others"  name="radio-group" onClick={this.addPayment.bind(this, "store-credit")}/>
                                    <label htmlFor="others" className="label_select_button">others</label>
                                </div>
                                <div className="w-100-block text-center mt-3 mb-2 notes">
                                    <div className="line-copy2">
                                        &nbsp; &nbsp;This will park the sale for up to 24 hours. &nbsp; &nbsp;
                                  </div>
                                </div>
                                <div className="w-100-block button_with_checkbox mt-1" >
                                    <input type="radio" id="test13" name="radio-group" role="menuitem" tabIndex="-1" data-toggle="modal" data-target="#addnotehere" />
                                    <label htmlFor="test13" className="label_select_button">Park Sale</label>
                                </div>

                            </form>
                        </div>
                        <div className="panel panelCalculator">
                            <div className="panel-body p-0">
                                <table className="table table table-bordered calculatorBlockTable">
                                    <tbody>
                                        <tr>
                                            <td colSpan="2" className="text-right br-1 p-0">
                                                <p className="total_checkout_val" id="calc_output">  <NumberFormat value= {payingAmount} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} /></p>
                                                {/* <input type="text" value={this.state.payingAmount} className="form-control tc_val noshaodow" id="calc_output" /> */}
                                            </td>
                                            <td className="text-center pointer p-0" onClick={() => this.rmvInp()}>
                                                {/* <button className="button icon icon-backarrow"></button> */}
                                                <img width="36" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAxNi4wLjAsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjwhRE9DVFlQRSBzdmcgUFVCTElDICItLy9XM0MvL0RURCBTVkcgMS4wLy9FTiIgImh0dHA6Ly93d3cudzMub3JnL1RSLzIwMDEvUkVDLVNWRy0yMDAxMDkwNC9EVEQvc3ZnMTAuZHRkIj4NCjxzdmcgdmVyc2lvbj0iMS4wIiBpZD0iTGF5ZXJfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiDQoJIHZpZXdCb3g9IjAgMCA2MCA2MCIgZW5hYmxlLWJhY2tncm91bmQ9Im5ldyAwIDAgNjAgNjAiIHhtbDpzcGFjZT0icHJlc2VydmUiPg0KPGc+DQoJPGc+DQoJCTxwYXRoIGZpbGwtcnVsZT0iZXZlbm9kZCIgY2xpcC1ydWxlPSJldmVub2RkIiBmaWxsPSJub25lIiBkPSJNMC4zOTUsMS42NThjMjAuMTMyLDAsNDAuMjY0LDAsNjAuMzk1LDANCgkJCWMwLDE5LjA3OSwwLDM4LjE1OSwwLDU3LjIzN2MtMjAuMTMxLDAtNDAuMjYzLDAtNjAuMzk1LDBDMC4zOTUsMzkuODE2LDAuMzk1LDIwLjczNywwLjM5NSwxLjY1OHogTTEyLjI2NSwzMC4wMzUNCgkJCWMwLjc3OS0wLjk1MiwxLjEzNS0xLjQ2OCwxLjU2OS0xLjkwNWMyLjU0NS0yLjU2Miw1LjExNi01LjA5OCw3LjY2My03LjY1N2MyLjIyNC0yLjIzMyw0LjQ4NC00LjQzNSw2LjU5Mi02Ljc3Mw0KCQkJYzAuMzg3LTAuNDMsMC4xMTgtMS40NSwwLjE1Mi0yLjE5OGMtMC43MDgsMC4wNzMtMS40MjgsMC4wODgtMi4xMTUsMC4yNDZjLTAuMjY5LDAuMDYyLTAuNDYsMC40NDctMC42OTMsMC42NzkNCgkJCWMtNS45NTksNS45My0xMS45MTIsMTEuODY3LTE3Ljg4NiwxNy43ODNjLTEuMjIyLDEuMjA5LTEuMDA5LDIuMjM1LDAuMTI4LDMuMzYxYzMuOTIxLDMuODg1LDcuODA3LDcuODA2LDExLjcwNywxMS43MTMNCgkJCWMyLjE4MiwyLjE4Nyw0LjMwNiw0LjQzNiw2LjU5LDYuNTFjMC40OTUsMC40NDgsMS41NjMsMC4yNjYsMi4zNjcsMC4zNzRjLTAuMDk3LTAuNzUtMC4xNDYtMS41MTEtMC4zMTYtMi4yNDMNCgkJCWMtMC4wNjUtMC4yODEtMC40MDEtMC41MDktMC42MzMtMC43NGMtMy4wNjctMy4wNjMtNi4xNDYtNi4xMTMtOS4yMDYtOS4xODNjLTIuMTUzLTIuMTYtNC4yODEtNC4zNDUtNi44MDktNi45MTINCgkJCWMxLjM5Mi0wLjA5OCwyLjIxMi0wLjIwNSwzLjAzMi0wLjIwNWMxMS43LTAuMDEyLDIzLjM5OS0wLjAwNywzNS4wOTktMC4wMTFjMC43MjItMC4wMDEsMS41MTUsMC4xMywyLjE0NC0wLjEyNQ0KCQkJYzAuNTU0LTAuMjI0LDAuOTI1LTAuOSwxLjM3Ny0xLjM3N2MtMC40ODYtMC40MzgtMC45MDktMS4wMjMtMS40NzktMS4yNzFjLTAuNDk5LTAuMjE4LTEuMTY0LTAuMDY0LTEuNzU1LTAuMDY0DQoJCQljLTExLjU2OC0wLjAwMS0yMy4xMzYtMC4wMDEtMzQuNzA0LTAuMDAxQzE0LjMzNywzMC4wMzUsMTMuNTg0LDMwLjAzNSwxMi4yNjUsMzAuMDM1eiIvPg0KCQk8cGF0aCBmaWxsLXJ1bGU9ImV2ZW5vZGQiIGNsaXAtcnVsZT0iZXZlbm9kZCIgZD0iTTEyLjI2NSwzMC4wMzVjMS4zMTksMCwyLjA3MiwwLDIuODI1LDBjMTEuNTY4LDAsMjMuMTM2LDAsMzQuNzA0LDAuMDAxDQoJCQljMC41OTEsMCwxLjI1Ni0wLjE1NCwxLjc1NSwwLjA2NGMwLjU2OSwwLjI0OSwwLjk5MiwwLjgzMywxLjQ3OSwxLjI3MWMtMC40NTIsMC40NzctMC44MjMsMS4xNTMtMS4zNzcsMS4zNzcNCgkJCWMtMC42MjksMC4yNTUtMS40MjIsMC4xMjQtMi4xNDQsMC4xMjVjLTExLjY5OSwwLjAwNC0yMy4zOTktMC4wMDEtMzUuMDk5LDAuMDExYy0wLjgyLDAtMS42NCwwLjEwNy0zLjAzMiwwLjIwNQ0KCQkJYzIuNTI3LDIuNTY3LDQuNjU2LDQuNzUyLDYuODA5LDYuOTEyYzMuMDYsMy4wNjksNi4xMzgsNi4xMiw5LjIwNiw5LjE4M2MwLjIzMiwwLjIzMSwwLjU2NywwLjQ1OSwwLjYzMywwLjc0DQoJCQljMC4xNzEsMC43MzIsMC4yMTksMS40OTMsMC4zMTYsMi4yNDNjLTAuODA0LTAuMTA4LTEuODczLDAuMDc0LTIuMzY3LTAuMzc0Yy0yLjI4NC0yLjA3NC00LjQwOS00LjMyMy02LjU5LTYuNTENCgkJCWMtMy44OTktMy45MDctNy43ODUtNy44MjgtMTEuNzA3LTExLjcxM2MtMS4xMzctMS4xMjYtMS4zNS0yLjE1MS0wLjEyOC0zLjM2MWM1Ljk3NC01LjkxNiwxMS45MjctMTEuODUzLDE3Ljg4Ni0xNy43ODMNCgkJCWMwLjIzMy0wLjIzMiwwLjQyNC0wLjYxNywwLjY5My0wLjY3OWMwLjY4OC0wLjE1OCwxLjQwOC0wLjE3MywyLjExNS0wLjI0NmMtMC4wMzQsMC43NDgsMC4yMzQsMS43NjgtMC4xNTIsMi4xOTgNCgkJCWMtMi4xMDgsMi4zMzgtNC4zNjgsNC41NC02LjU5Miw2Ljc3M2MtMi41NDcsMi41NTktNS4xMTgsNS4wOTUtNy42NjMsNy42NTdDMTMuNCwyOC41NjcsMTMuMDQ0LDI5LjA4MywxMi4yNjUsMzAuMDM1eiIvPg0KCTwvZz4NCjwvZz4NCjwvc3ZnPg0K"></img>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td className="td-calc-padding br-1 p-0">
                                                <button type="button" onClick={() => this.calcInp(1)} className="btn btn-default calculate">1</button>
                                            </td>
                                            <td className="td-calc-padding br-1 p-0">
                                                <button type="button" onClick={() => this.calcInp(2)} className="btn btn-default calculate">2</button>
                                            </td>
                                            <td className="td-calc-padding p-0">
                                                <button type="button" onClick={() => this.calcInp(3)} className="btn btn-default calculate">3</button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td className="td-calc-padding br-1 p-0">
                                                <button type="button" onClick={() => this.calcInp(4)} className="btn btn-default calculate">4</button>
                                            </td>
                                            <td className="td-calc-padding br-1 p-0">
                                                <button type="button" onClick={() => this.calcInp(5)} className="btn btn-default calculate">5</button>
                                            </td>
                                            <td className="td-calc-padding p-0">
                                                <button type="button" onClick={() => this.calcInp(6)} className="btn btn-default calculate">6</button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td className="td-calc-padding br-1 p-0">
                                                <button type="button" onClick={() => this.calcInp(7)} className="btn btn-default calculate">7</button>
                                            </td>
                                            <td className="td-calc-padding br-1 p-0">
                                                <button type="button" onClick={() => this.calcInp(8)} className="btn btn-default calculate">8</button>
                                            </td>
                                            <td className="td-calc-padding p-0">
                                                <button type="button" onClick={() => this.calcInp(9)} className="btn btn-default calculate">9</button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td className="td-calc-padding br-1" onClick={() => this.minplus()}>
                                                <button className="icon icon-plus button"></button>
                                                <button className="icon icon-minus button"></button>
                                            </td>
                                            <td className="td-calc-padding br-1 p-0">
                                                <button type="button" onClick={() => this.calcInp('.')} className="btn btn-default calculate">,</button>
                                            </td>
                                            <td className="td-calc-padding p-0">
                                                <button type="button" onClick={() => this.calcInp(0)} className="btn btn-default calculate">0</button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
}

function mapStateToProps(state) {
    const { checkoutlist } = state;
    return {
        checkoutlist
    };
}
const connectedCheckoutViewThird = connect(mapStateToProps)(CheckoutViewThird);
export { connectedCheckoutViewThird as CheckoutViewThird };