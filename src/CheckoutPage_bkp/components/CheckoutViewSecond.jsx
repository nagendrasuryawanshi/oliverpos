import React from 'react';
import { connect } from 'react-redux';



class CheckoutViewSecond extends React.Component {
    constructor(props) {
        super(props);
          this.state={
            checkList:[]
          }
          this.addPayment = this.addPayment.bind(this);
    }

    componentWillMount(){
        let checkList = localStorage.getItem('CHECKLIST');
        this.setState({checkList:JSON.parse(checkList)})
       
       }

    addPayment(type,store_credit) {
           let amount = $('#calc_output').text();
            console.log("AMOUNT",amount);
            console.log("STORE CFREDIT AMOUNT",store_credit)
            if(store_credit >= amount){
                this.props.addPayment(type , store_credit);
                this.props.getPayment(type, amount);
            }else if (store_credit < amount){
                alert("insufficiant balance please add another way add monay")
            }else{
                this.props.getPayment(type, amount);
            }
          
    }

    render() {
        const { checkout_list } = this.props;
        const { checkList } = this.state;
        let checkoutList = checkList && checkList.customerDetail && checkList.customerDetail.Content
       //console.log("second page",checkList)
        return (
            <div className="col-lg-6 col-sm-6 col-xs-12 pt-4 plr-8">
                        <div className="items preson_info">
                            <div className="item-heading text-center font24">Customer Information</div>
                            <div className="panel panel-default panel-product-list" style={{ borderColor: '#979797' }}>
                                <div className="singleName">
                                    <h4 className="mt-0 mb-2">
                                       {checkoutList?checkoutList.FirstName:''}{" "}{checkoutList?checkoutList.LastName:''}
                                       {checkoutList?<a className="edit-info" data-toggle="modal" href="#edit-info">Edit <span className="hide_small">Information</span></a>:null}
                                    </h4>
                                </div>
                                <div className="overflowscroll" id="UserInfo_checkout">
                                    <div className="personal_info">
                                        <strong className="lead_personal">Personal Info:</strong>
                                        <div className="mt-3 mb-3">
                                            <p className="clearfix">
                                                <span className="text-muted">Email:</span>
                                                <span className="text-danger"> {checkoutList?checkoutList.Email:''}</span>

                                            </p>
                                            <p className="clearfix">
                                                <span className="text-muted">Address:</span>
                                                <span className="text-danger"> {checkoutList?checkoutList.StreetAddress:''}</span>
                                            </p>
                                            <p className="clearfix">
                                                <span className="text-muted">Phone:</span>
                                                <span className="text-danger">{checkoutList? '+' +checkoutList.Phone:''}</span>
                                            </p>
                                            <p className="clearfix">
                                                <span className="text-muted">Notes:</span>
                                                <span className="text-danger">{checkoutList?checkoutList.Notes:''}</span>
                                            </p>
                                        </div>
                                    </div>
                                    <div className="personal_info">
                                        <strong className="lead_personal">Account Info:</strong>
                                        <div className="mt-3 mb-3">
                                            <p className="w-50-block text-center clearfix">
                                                <span className="text-muted w-100">Account Balance</span>
                                                <span className="text-danger w-100">{checkoutList?checkoutList.AccountBalance:''}</span>
                                            </p>
                                            <p className="w-50-block text-center clearfix">
                                                <span className="text-muted w-100">Store Credit</span>
                                                <span className="text-danger w-100">{checkoutList?checkoutList.StoreCredit:''}</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div className="panel panel-footer p-0 b-0 m-0 clearfix bg-white" id="userinfo_footer">
                                    <div className="personal_info w-100-block">
                                        <div className={ checkoutList?'mt-1':'btn-check-disabled'}>
                                            <div className="w-80-block button_with_checkbox">
                                                <input type="radio" id="test3" name="radio-group" onClick={this.addPayment.bind(this, "lay_away")}/>
                                                <label htmlFor="test3" className="label_select_button">Lay away</label>
                                            </div>
                                        </div>
                                        <div className={ checkoutList?"mt-3":"btn-check-disabled"}>
                                            <div className="w-80-block button_with_checkbox">
                                                <input type="radio" id="test4" name="radio-group" onClick={this.addPayment.bind(this, "store_credit",checkoutList?checkoutList.StoreCredit:'')}/>
                                                <label htmlFor="test4" className="label_select_button">Store Credit</label>
                                            </div>
                                        </div>
                                    </div>
                                   
                              </div>
                            </div>
                        </div>
                    </div>
        )
    }
}

function mapStateToProps(state) {
    const { checkoutlist ,checkout_list} = state;
    return {
        checkoutlist,
        checkout_list:checkout_list.items
    };
}
const connectedCheckoutViewSecond = connect(mapStateToProps)(CheckoutViewSecond);
export { connectedCheckoutViewSecond as CheckoutViewSecond };