import { checkoutConstants  } from '../constants/checkout.constants';

export function checkoutlist(state = {}, action) {
    switch (action.type) {
        case checkoutConstants.GETALL_REQUEST:
            return {
                loading: true
            };
        case checkoutConstants.GETALL_SUCCESS:
            return {
                items: action.checkoutlist
            };
        case checkoutConstants.GETALL_FAILURE:
            return {
                error: action.error
            };

        default:
            return state
    }
}

export function checkout_list(state = {}, action) {
   // console.log("action.type" ,action)
    switch (action.type) {
        // case checkoutConstants.PRODUCT_ADD_REQUEST:
        //     return {
        //         loading: true
        //     };
          case checkoutConstants.GET_CHECKOUT_LIST_SUCCESS:    
            return {
                items:  action.checkout_list
                   };
       
            default:
            return state
    }
}


export function shop_order(state = {}, action) {
      switch (action.type) {
         // case checkoutConstants.PRODUCT_ADD_REQUEST:
         //     return {
         //         loading: true
         //     };
           case checkoutConstants.INSERT_SUCCESS:    
           localStorage.setItem("ORDER_ID" ,action.shop_order && action.shop_order.Content);
             return {
                 items:  action.shop_order
                    };
        
             default:
             return state
     }
 }

