import React from 'react';
import { connect } from 'react-redux';
import { CommonHeaderTwo, NavbarPage ,CustomerNote, CartListView , CustomerAddFee } from '../_components';
import { List } from './components/List'



class ListView extends React.Component {



    render() {
        return (
            <div>
                <div className="wrapper">
                    <div className="overlay"></div>
                      <NavbarPage {...this.props} />
                    <div id="content">
                        <CommonHeaderTwo {...this.props} />
                        <div className="inner_content bg-light-white clearfix">
                            <div className="content_wrapper">
                                <List />
                                <CartListView />
                            </div>
                        </div>
                    </div>
                </div>
                <div  tabIndex="-1" id="popup_oliver_add_fee" tabIndex="-1" className="modal modal-wide modal-wide1 fade">
                     <CustomerAddFee/>
                 </div>
                 <div tabIndex="-1" className="modal fade in mt-5 modal-wide" id="addnotehere" role="dialog">
                        <CustomerNote/>
                  </div>
            </div>
        );
    }

}

function mapStateToProps(state) {
    const { registering } = state.registration;
    return {
        registering
    };
}

const connectedListView = connect(mapStateToProps)(ListView);
export { connectedListView as ListView };