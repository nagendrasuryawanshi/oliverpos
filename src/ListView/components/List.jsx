import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { AllProduct } from '../../_components/AllProduct';
import { Attributes } from '../../_components/Attributes';
import { Categories } from '../../_components/Categories';


class List extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            active: false,
            ListItem2: [],
            item: 0
        }
    }

    ActiveList(item) {
        // console.log("list",list)
        this.setState({
            active: true,
            item: item
            // , ListItem2:list.product_list
        })

    }

    componentWillMount()
    {
       setHeightDesktop();
    }
    render() {
        const { active, ListItem2, item } = this.state;
        return (
            <div>
                {active != true ?
                    <div className="col-lg-9 col-sm-8 col-xs-8 pr-0">
                        <div className="items pt-3 ">
                            <div className="item-heading text-center">Library</div>
                            <div className="panel panel-default panel-product-list overflowscroll p-0" id="allProductHeight">
                                <div className="searchDiv" style={{ display: 'none' }}>
                                    <input type="search" className="form-control nameSearch" placeholder="Search Product / Scan" />
                                </div>
                                <table className="table ShopProductTable">
                                    <colgroup>
                                        <col style={{ width: '*' }} />
                                        <col style={{ width: 40 }} />
                                    </colgroup>
                                    <tbody>
                                        <tr key='1' onClick={() => this.ActiveList(1)}>
                                            <td>All Items</td>
                                            <td><a><img src="assets/img/next.png" /></a></td>
                                        </tr>
                                        <tr key='2' onClick={() => this.ActiveList(2)}>
                                            <td>Attributes</td>
                                            <td><a><img src="assets/img/next.png" /></a></td>
                                        </tr>
                                        <tr key='3' onClick={() => this.ActiveList(3)}>
                                            <td>Categories</td>
                                            <td><a><img src="assets/img/next.png" /></a></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    :
                    (
                        item == 1 ?
                        <div className="col-lg-9 col-sm-8 col-xs-8 pr-0">
                                <AllProduct onRef={ref => (this.tileProductFilter = ref)} parantPage="list"/>
                            </div>
                            : item == 2 ?
                                <Attributes />
                                : item == 3 ?
                                    <Categories />
                                    : null

                    )
                }
            </div>
        )
    }

}


function mapStateToProps(state) {
    const { registering } = state.registration;
    return {
        registering
    };
}

const connectedList = connect(mapStateToProps)(List);
export { connectedList as List };


