import { pinLoginConstants } from "../constants/pinlogin.constants";
import { pinLoginService } from "../services/PinLogin.service";
 import { alertActions } from "../../_actions/alert.actions";
 import { history } from "../../_helpers";

export const pinLoginActions = {
  pinLogin  
};

// <!-- Pin Login  -->
function pinLogin(pin) {
  return dispatch => {
    dispatch(request());

    pinLoginService
      .pinLogin(pin)
       .then(        
        user => { 
            if(user=="Success")
            {
              console.log("Login Success");
              dispatch(success(user));
              //history.push('/shopview')
              window.location='/shopview';
          }
          else{
          
            dispatch(failure(user.toString()));
            dispatch(alertActions.error(user.toString()));
            
          }
          },
          error => {            
              dispatch(failure(error.toString()));
              dispatch(alertActions.error(error.toString()));            
          }
       //  res =>{      
      //   dispatch(success(res)),
      //   error => dispatch(failure(error.toString()))
      //   }
      );
  };

  function request() {
    return { type: pinLoginConstants.PIN_LOGIN_REQUEST };
  }
  function success(logindetail) 
  {
    return { type: pinLoginConstants.PIN_LOGIN_SUCCESS, logindetail };
  }
  function failure(error) {
    return { type: pinLoginConstants.PIN_LOGIN_FAILURE, error };
  }
}

// <!-- End  -->

// // <!-- Get All Device List  -->
// function getAllDeviceList() {
//   return dispatch => {
//     dispatch(request());

//     deviceService
//       .getAllDeviceList()
//       .then(
//         device_list => dispatch(success(device_list)),

//         error => dispatch(failure(error.toString()))
//       );
//   };

  // function request() {
  //   return { type: userConstants.GETALL_DEVICE_LIST_REQUEST };
  // }
  // function success(device_list) {
  //   return { type: userConstants.GETALL_DEVICE_LIST_SUCCESS, device_list };
  // }
  // function failure(error) {
  //   return { type: userConstants.GETALL_DEVICE_LIST_FAILURE, error };
  // }
// }

// <!-- End  -->

