import Config from '../../Config'
import { authHeader } from '../../_helpers';

export const pinLoginService = {
    pinLogin,
    logout
   
};
const API_URL=Config.key.OP_API_URL

function pinLogin(pin) {
    const requestOptions = {
        method: 'GET',
        headers: { 'Content-Type': 'application/json' }
    };
   // return fetch(`${config.apiUrl}/users/authenticate`, requestOptions)
   var UDID=localStorage.getItem('UDID')
   //Location
   console.log("user",UDID)

   return fetch(`${API_URL}/ShopAccess/VerifyPin?udid=`+UDID+'&pin='+pin, requestOptions)
        .then(handleResponse)
        .then(response => {
            console.log("PinLoginResponse",response);
            // login successful if there's a jwt token in the response
            if (response.Message=="Success") {               
                localStorage.setItem('user', JSON.stringify(response.Content));                
            }
            return response.Message;
        });
}

function logout() {
    // remove user from local storage to log user out
    localStorage.removeItem('user');
}


function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                logout();
                location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}