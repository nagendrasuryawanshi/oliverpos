import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import {pinLoginActions} from './action/PinLogin.action';
import { userActions } from '../_actions'
import {LoadingOWL } from '../_helpers/loadingOWL';
import { history } from '../_helpers';

class PinPage extends React.Component {
    constructor(props){
        super(props);
        this.state = { 
         totalSize:0,
         txtValue:"" ,
            };
         }
        componentWillMount(){
           // this.props.dispatch(userActions.logout())
            console.log("Local storage UDID",localStorage.getItem('UDID'));
         
            if( ! localStorage.getItem("sitelist") ||  ! localStorage.getItem('UDID')||  ! localStorage.getItem('Location')||  ! localStorage.getItem('register')) 
            {
                history.push('/login');
            }
        }

    addToScreen(e) {
        console.log('The link was clicked.',e);
        if (e == "c" ) {
            if(this.state.totalSize>0)
            {
                this.state.txtValue.substring(0, this.state.txtValue.length-1);
                this.resetScreen();
            }
            return;
        }
        if(this.state.totalSize<4)
           {
               this.state.txtValue+=e;
                this.state.totalSize+=1;
                this.fillPass();
           } 
      }
       resetScreen() {
        this.setState({txtValue:this.state.txtValue.substring(0, this.state.txtValue.length-1)})
        this.state.totalSize-=1;
        this.fillPass();

        //window.location='/loginpin'
      }
    fillPass()
   { 
       let i="";
       for(i=1;i<=4;i++)
       {
          
          if(this.state.totalSize>=i)
          {
              if(this.state.totalSize >= 4)
              { 
                const { txtValue,totalSize } = this.state;
                const { dispatch , alert  } = this.props;
                console.log("Pin",txtValue);
              
              if (txtValue) {
                    dispatch(pinLoginActions.pinLogin(txtValue));
                }
             
               // window.location='/shopview';

              }
               $("#txt"+i).removeClass("bg_trasn");
          }
           else
           {

               $("#txt"+i).addClass("bg_trasn");
           }
       }
   }
   

    render() {
        const { alert } = this.props;
           return (
            <div className="bgimg-1">
            {/* <LoadingOWL /> */}
                <div className="content_main_wapper">
                <a href="javascript:void(0)" className="aonboardingbackicon">
                  <button className="button onboardingbackicon">&nbsp;</button>&nbsp; Go Back
                 </a>
                    {/* <div className="dialog-content"> */}
                        <div className="onboarding-loginBox">
                            <div className="login-form">
                                <div className="onboarding-pg-heading">
                                    <h1>Please Enter Your PIN</h1>
                                    <p>
                                        Enter the 4 digit PIN you’ve set up with Oliver POS.
                              </p>
                                </div>
                                <div>
                                    <div id="calculator" style={{ width: '100%' }}>
                                        <form>
                                            <table>
                                              <tbody>
  
                                                <tr>
                                                    <td>
                                                        <input type="button" value="1" id="keys"  onClick={()=>this.addToScreen('1')} className="b_top0 b_lft0 input-to-number" />
                                                    </td>
                                                    <td>
                                                        <input type="button" value="2" id="keys" onClick={()=>this.addToScreen('2')} className="b_top0 b_lft0 input-to-number" />
                                                    </td>
                                                    <td>
                                                        <input type="button" value="3" id="keys" onClick={()=>this.addToScreen('3')} className="b_top0 b_lft0 b_rgt0 input-to-number" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="button" value="4" id="keys" onClick={()=>this.addToScreen('4')} className="b_top0 b_lft0 input-to-number" />
                                                    </td>
                                                    <td>
                                                        <input type="button" value="5" id="keys" onClick={()=>this.addToScreen('5')} className="b_top0 b_lft0 input-to-number" />
                                                    </td>
                                                    <td>
                                                        <input type="button" value="6" id="keys" onClick={()=>this.addToScreen('6')} className="b_top0 b_lft0 b_rgt0 input-to-number" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="button" value="7" id="keys" onClick={()=>this.addToScreen('7')} className="b_top0 b_lft0 input-to-number" />
                                                    </td>
                                                    <td>
                                                        <input type="button" value="8" id="keys" onClick={()=>this.addToScreen('8')} className="b_top0 b_lft0 input-to-number" />
                                                    </td>
                                                    <td>
                                                        <input type="button" value="9" id="keys" onClick={()=>this.addToScreen('9')} className="b_top0 b_lft0 b_rgt0 input-to-number" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <input type="button" value="" id="keys" onClick={()=>this.addToScreen('')} className="b_top0 b_bottom0 b_lft0 input-to-number"  readOnly={false} />
                                                    </td>
                                                    <td>
                                                        <input type="button" value="0" id="keys" onClick={()=>this.addToScreen('0')} className="b_top0 b_lft0 bb-0 input-to-number" />
                                                    </td>
                                                    <td>
                                                        <input type="button" value="" id="keys" onClick={()=>this.addToScreen('c')} className="b_top0 b_bottom0 b_lft0 b_rgt0 cal-back input-to-number" />
                                                    </td>
                                                </tr>
                                               </tbody> 
                                            </table>
                                        </form>
                                        <div className="clearfix"></div>
                                        <div className="HightLIghtPassword">
                                            <div className="">
                                                <input id="txt1" type="button" className="bg_trasn"   readOnly={true} />
                                                <input id="txt2" type="button" className="bg_trasn"   readOnly={true}/>
                                                <input id="txt3" type="button" className="bg_trasn" readOnly={true} />
                                                <input id="txt4" type="button" className="bg_trasn"   readOnly={true}/>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            {alert?<div className="onboarding-pg-heading">Invalid Pin, Please try with other.</div>:""}
                        </div>
                        
                        <div className="powered-by-oliver">
                            <a href="javascript:void(0)">Powered by Oliver POS</a>
                        </div>
                    {/* </div> */}
                </div>
            </div>
            
        )

    }


}

function mapStateToProps(state) {
    console.log("PinPagestate",state);
    const { alert } = state;
    
    return {
        alert:alert.message
    };
}

const connectedPinPage = connect(mapStateToProps)(PinPage);
export { connectedPinPage as PinPage };