import { activityConstants } from '../constants/activity.constants'
import { activityService } from '../services/activity.services';



export const activityActions = {
    getAll,
    getDetail,
   filteredListOrder ,
   Sendmail
};

function getAll(uid,pagesize,pagenumber) {
    return dispatch => {
        dispatch(request());

        activityService.getAll(uid,pagesize,pagenumber)
            .then(
                activities => dispatch(success(activities)),
                error => dispatch(failure(error.toString()))
            );
    };


    function request() { return { type: activityConstants.GETALL_REQUEST } }
    function success(activities) { return { type: activityConstants.GETALL_SUCCESS, activities } }
    function failure(error) { return { type: activityConstants.GETALL_FAILURE, error } }

   
    


}
function getDetail(ordid,UID) {
    console.log("getDetail",ordid,UID)
    return dispatch => {
        dispatch(request());

        activityService.getDetail(ordid,UID)
            .then(
                single_Order_list => dispatch(success(single_Order_list)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: activityConstants.GET_DETAIL_REQUEST } }
    function success(single_Order_list) { return { type: activityConstants.GET_DETAIL_SUCCESS, single_Order_list } }
    function failure(error) { return { type: activityConstants.GET_DETAIL_FAILURE, error } }

   

}

function filteredListOrder(filteredListOrder){
    console.log("this dispatch section" ,filteredListOrder)
    return dispatch => {
         dispatch(success(filteredListOrder))
         };

       function success(filteredListOrder) { return { type: activityConstants.GET_FILTER_ORDER_LIST_SUCCESS, filteredListOrder } }
        
}

function Sendmail(mail){
   // console.log("activimail",mail);
    return dispatch => {
        dispatch(request());
        activityService.mailSend(mail)
            .then(
                mail_success => dispatch(success(mail_success)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: activityConstants.SEND_EMAIL_REQUEST } }
    function success(mail_success) { return { type: activityConstants.SEND_EMAIL_SUCCESS, mail_success } }
    function failure(error) { return { type: activityConstants.SEND_EMAIL_FAILURE, error } }

}
