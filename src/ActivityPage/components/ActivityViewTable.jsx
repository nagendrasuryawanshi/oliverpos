import React from 'react';    
export const ActivityViewTable = (props) => {
    console.log("OrderPayment",props); 
        return (
            props != null ? (
                <tbody>
                    <tr key={props.key}>
                        <td>Sub-total</td>
                        <td className="text-right">{props.Subtotal !='NaN' ? (props.Subtotal) : 0}</td>
                    </tr>
                    <tr>
                        <td>Discount</td>
                        <td className="text-right">{props.Discount}</td>
                    </tr>
                    <tr>
                        <td>Total Tax</td>
                        <td className="text-right">{
                            (props.tax_refunded > 0) ? <div>{(props.TotalTax-props.tax_refunded).toFixed(2)} <del style={{marginLeft : 5}}>{props.TotalTax.toFixed(2)}</del> </div> : props.TotalTax.toFixed(2)
                        }</td>
                    </tr>

                    <tr className="totalAmount">
                        <td className=""><b>Total</b></td>
                        <td className="text-right"><b>{
                            (props.refunded_amount > 0) ? <div>{(props.TotalAmount-props.refunded_amount).toFixed(2)} <del style={{marginLeft : 5}}>{props.TotalAmount.toFixed(2)}</del> </div> : props.TotalAmount.toFixed(2)
                        }</b></td>
                    </tr>
                    
                    <tr style={ (props.refunded_amount > 0) ? {display : ""} : {display : "none"} }>
                        <td>Refunded tax</td>
                        <td className="text-right">{(props.tax_refunded).toFixed(2)}</td>
                    </tr>
                    <tr className="totalAmount" style={ (props.refunded_amount > 0) ? {display : ""} : {display : "none"} }>
                        <td className=""><b>Refunded Amount</b></td>
                        <td className="text-right"><b>{(props.refunded_amount).toFixed(2)}</b></td>
                    </tr>
                     

                    {props.OrderPayment && props.OrderPayment.map((item, index) => {
                        return (
                            <tr key={`payment${index}`}>
                                <td>{`${item.type} (${item.payment_date})`}</td>
                                <td className="text-right">{item.amount}</td>
                            </tr>
                        )
                    })}

                    <tr className="totalAmount" style={ (props.refunded_amount > 0) ? {display : ""} : {display : "none"} }>
                        <td className=""><b>Refund Payment's</b></td>
                        <td className="text-right"><b></b></td>
                    </tr>

                    {
                        (props.refunded_amount > 0) ? (
                            (typeof props.refundPayments !== "undefined" && props.refundPayments.length > 0) ? (
                                props.refundPayments && props.refundPayments.map((item, index) => {
                                    return (
                                        <tr key={`refund${index}`}>
                                            <td>{`${item.type} (${item.payment_date})`}</td>
                                            <td className="text-right">{item.amount}</td>
                                        </tr>
                                    )
                                })
                            ) : ""
                        ) : ""
                    }
                </tbody>
            ) : null

               
        )
    
}
