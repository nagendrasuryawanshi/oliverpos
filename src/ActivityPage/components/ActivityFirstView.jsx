import React from 'react';


export const ActivityFirstView = (props) => {
//console.log("props121",props);
    return (
        <tbody onClick={props.onClick}>
                <tr>
                    <th colSpan="2" className="tablecaption text-center">
                        {props.CreatedOn }
                    </th>
                </tr>
                <tr className={props.className} style={{cursor : "pointer"}}>
                    <td>
                    {                         
                        (props.refunded_amount > 0) ? <div>{ parseFloat(props.total-props.refunded_amount).toFixed(2) } <del>{parseFloat(props.total).toFixed(2)}</del> </div> : props.total
                    }
                        <p className="comman_subtitle">{props.status.replace("_", " ")}</p>
                    </td>
                <td className="text-right">
                        {props.time}
                </td>
            </tr>
           
        </tbody>
       
    );
}
