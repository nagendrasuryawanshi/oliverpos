import React from 'react';
import { connect } from 'react-redux';
import { activityActions } from '../actions/activity.action';
import { NavbarPage, CommonHeader ,LoadingModal} from '../../_components';
import { ActivityFirstView } from './ActivityFirstView';
import { ActivitySecondView } from './ActivitySecondView';
import { history } from '../../_helpers';
import  moment  from 'moment';

class Activity extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            active: null,
            CreatedDate: null,
            pagenumber: 2,
            getPdfdateTime:null,
            msgPOP:'',
            activeFilter:false,
            activityBuffer : new Array(),
            search:''
            
        }
        this.filterCustomerOrder = this.filterCustomerOrder.bind(this);

    }

    componentDidMount() {
        this.reload();
    }


    formatDate(item){
       // console.log("formatDate",item );
        var mydate = new Date(item);
        var month = ["January", "February", "March", "April", "May", "June",
            "July", "August", "September", "October", "November", "December"][mydate.getMonth()];
        var CreatedOn = mydate.getDate() + '. ' + month + ' ' + mydate.getFullYear();
          return CreatedOn;
      }

    reload(pagno) {
        const UID = localStorage.getItem('UDID');
        //console.log("%cUID BY CUSTOMERS", "color:darkpink", UID)
        this.setState({ UDID: UID })
        console.log("this.state.pagenumber1",this.state.pagenumber)
        var pagesize = 100
        this.props.dispatch(activityActions.getAll(UID, pagesize, pagno));
    }

    load() {

        if (this.state.pagenumber) {
            this.setState({
                pagenumber: this.state.pagenumber + 1,
            });
            console.log("this.state.pagenumber", this.state.pagenumber)
            if (this.state.pagenumber != 1) {
                this.reload(this.state.pagenumber);
            }
        }
    }
   

       activeClass(item,index){
       
           const { UDID } = this.state;
            this.setState({active:index})          
            this.setState({CreatedDate:this.formatDate(item.date)})  
        
              var mydate = new Date(item.date);
              var month = ["January", "February", "March", "April", "May", "June",
                  "July", "August", "September", "October", "November", "December"][mydate.getMonth()];
                var getPdfdate=(mydate.getMonth() + 1) + '/' + mydate.getDate() + '/' +  mydate.getFullYear() +  ' ' + item.time ;
              
                this.setState({getPdfdateTime:getPdfdate})
            //     console.log("this.state.UDID",this.state.UDID)
               this.props.dispatch(activityActions.getDetail(item.order_id,this.state.UDID));  
 
            }

            filterCustomerOrder(e) {
                let filtered = [];
                const { value } = e.target;
                this.setState({search:value})

                console.log("search", value);
                const orderlist = this.props.activities && this.props.activities.activities;
                    //   console.log("orderlist",orderlist);
                 orderlist.map((item) => {
                    if(item.order_status.toUpperCase().toString().indexOf(value)!=-1 ||item.order_status.toLowerCase().toString().indexOf(value)!=-1 
                    || item.total.toString().indexOf(value)!=-1 || item.time.toString().indexOf(value)!=-1
                       )
                    
                    filtered.push(item);
                    this.setState({ activeFilter: true })
                   
                })
                //console.log("filteredList filnal", filtered);
                if (filtered.length == 0) {
                 //   console.log("o length")
                    this.setState({ activeFilter: false })
                 }
                 this.props.dispatch(activityActions.filteredListOrder(filtered))
            }   
          
            clearInput(){
                this.setState({
                    search:'',
                    activeFilter: false
                })
            
            }      
     windowLocation1(type , id) {
        if(type== 'statuscompleted' && id){
           
            history.push('/refund');

       
        }
        // if(type== 'statuspending' && id){
        //      let ListItem = [];
            
        //     console.log("GO TO CHECKOUT PAGE",this.props.single_Order_list)
        //     let single_Order_list = this.props.single_Order_list.Content;
        //    // console.log("moment",moment().format('DD ,MMMM YYYY, h:mm a'))

        //     single_Order_list.line_items.map(item=>{
        //         ListItem.push({Title:item.name,Price:item.subtotal,qunatity:item.quantity})
        //     })
        //     console.log("GO TO CHECKOUT PAGE 1",ListItem)
        //     let orderCustomerInfo = single_Order_list.orderCustomerInfo;
        //      let addcust = {
        //         Content :{
        //                 AccountBalance: 0,
        //                 City: "",
        //                 Email: orderCustomerInfo?orderCustomerInfo.customer_email:'',
        //                 FirstName: orderCustomerInfo?orderCustomerInfo.customer_name:'',
        //                 Id: orderCustomerInfo?orderCustomerInfo.customer_id:'',
        //                 LastName: "",
        //                 Notes: "",
        //                 Phone: "",
        //                 Pin: 0,
        //                 Pincode: "",
        //                 StoreCredit: orderCustomerInfo?orderCustomerInfo.store_credit:'',
        //                 StreetAddress: "",
        //                 UID: 0,
        //     }
        
        // }
        //    console.log("GO TO CHECKOUT PAGE 2",addcust)
        //      var  CheckoutList = {
        //             ListItem: ListItem,
        //             customerDetail: addcust,
        //             totalPrice: single_Order_list.total_amount,
        //             discountCalculated: single_Order_list.discount,
        //             tax: single_Order_list.total_tax,
        //             subTotal:single_Order_list.total_amount,
        //             TaxId:3,
        //             status:single_Order_list.order_status,
        //             order_id:single_Order_list.order_id,
        //             order_date:moment(single_Order_list.OrderDateTime).format('DD ,MMMM YYYY, h:mm a')
        //         }
        //         console.log("GO TO CHECKOUT PAGE 3",CheckoutList)
        //         localStorage.setItem("CHECKLIST", JSON.stringify(CheckoutList))
        //         history.push('/checkout');
        //     }

        if(type== 'statuspending' && id){            
           localStorage.removeItem("oliver_order_payments"); //remove existing payments           
           let single_Order_list = this.props.single_Order_list.Content;
           console.log("single_Order_list123", single_Order_list)
            //added by deepsagar
            let setOrderPaymentsToLocalStorage = new Array();          
            if ( typeof single_Order_list.order_payments !== 'undefined' ) {
                single_Order_list.order_payments.map( pay => {
                    setOrderPaymentsToLocalStorage.push({
                        "Id"             : pay.Id,
                        "payment_type"   : pay.type,
                        "payment_amount" : pay.amount,
                        "order_id"       : single_Order_list.order_id,
                    });
                })
            }
          
          localStorage.setItem("oliver_order_payments", JSON.stringify(setOrderPaymentsToLocalStorage))   
          //added by deepsagar
           console.log("single_Order_list", single_Order_list)
           let ListItem = new Array();
           single_Order_list.line_items.map(item=>{
                ListItem.push({
                    line_item_id : item.line_item_id,
                    quantity : item.quantity,
                    Title : item.name,
                    Price : item.subtotal,
                    subtotalPrice : item.subtotal,
                    subtotaltax : item.subtotal_tax,
                    totalPrice : item.total,
                    totaltax : item.total_tax,
                    product_id : item.product_id,
                    variation_id : item.variation_id,
                    after_discount : item.total
                })
           })
           
           //console.log("place_order old shakun", ListItem);    
           let orderCustomerInfo = single_Order_list.orderCustomerInfo;
           console.log("single_Order_list.orderCustomerInfo",single_Order_list.orderCustomerInfo)
            let addcust = {
               Content :{
                       AccountBalance: 0,
                       City: "",
                       Email: orderCustomerInfo?orderCustomerInfo.customer_email:'',
                       FirstName: orderCustomerInfo?orderCustomerInfo.customer_name:'',
                       Id: orderCustomerInfo?orderCustomerInfo.customer_id:'',
                       LastName: "",
                       Notes: "",
                       Phone: "",
                       Pin: 0,
                       Pincode: "",
                       StoreCredit: orderCustomerInfo?orderCustomerInfo.store_credit:'',
                       StreetAddress: "",
                       UID: 0,
           }
       }
       
       
          console.log("GO TO CHECKOUT PAGE 2",addcust)
            var  CheckoutList = {
                   ListItem: ListItem,
                   customerDetail: orderCustomerInfo?addcust:'',
                   totalPrice: single_Order_list.total_amount,
                   discountCalculated: single_Order_list.discount,
                   tax: single_Order_list.total_tax,
                   subTotal:single_Order_list.total_amount,
                   TaxId:3,
                   status:single_Order_list.order_status,
                   order_id:single_Order_list.order_id,
                   order_date:moment(single_Order_list.OrderDateTime).format('DD ,MMMM YYYY, h:mm a')
               }
               console.log("GO TO CHECKOUT PAGE 3",CheckoutList)
               localStorage.setItem("CHECKLIST", JSON.stringify(CheckoutList))
               history.push('/checkout');
           }
    
           
    }
 
     openModal(type,content){
         const { mail_success } = this.props;
         let  id=content.order_id?content.order_id:"";
         //console.log("mail_success123",mail_success);
        // let customer_mail=content.orderCustomerInfo.customer_email;
     if(type=='print'|| type=='email' && !id){
          this.setState({msgPOP:'Please Select an order first.'})
          $('#PrintMessage').modal('show');
       }
     if(type=='email' && id){
          //console.log("customer_mail",content)
          if(!content.orderCustomerInfo && content.orderCustomerInfo == null ){
            this.setState({msgPOP:'E-mail is  Not Defined '})
            $('#PrintMessage').modal('show');
          }
          if(content.orderCustomerInfo !=null ){
          //  console.log("email",content.orderCustomerInfo.customer_email);

            var email=content.orderCustomerInfo.customer_email
            this.props.dispatch(activityActions.Sendmail(email));  
           // if(mail_success == 'sent'){
             this.setState({msgPOP:'E-mail Receipt Sent Successfully'})
           // $('#PrintMessage').modal('show');
            //}
          } 
       } 
      if(type=='refund' && id){
        // console.log("email",id)
       this.setState({msgPOP:'Selected order has been Refunded.'})
       $('#PrintMessage').modal('show');
       }
     if(type=='voidsale' && id){
        // console.log("email",id)
       this.setState({msgPOP:'Selected order has been Cancelled.'})
       $('#PrintMessage').modal('show');
     } 
       
      if(type=='print' && id){
      }  
     
    }

    render() {
        const { activities, single_Order_list,filteredListOrder,mail_success } = this.props;
        const {
            active,msgPOP,activeFilter,activityBuffer,search
          } = this.state;
        
       // console.log("before loop filteredListOrder", this.props.filteredListOrder);                             
        activities && activities.activities && activities.activities.map(item => {
            activityBuffer.push(item);
        })
        if(mail_success == 'sent'){
            $('#PrintMessage').modal('show');
        }

      //  console.log("deepactivity", this.state.activityBuffer);


      ///  console.log("after loop filteredListOrder", filteredListOrder);
        
        return (
            <div>
                <div className="wrapper">
                    <div className="overlay"></div>
                    <NavbarPage {...this.props} />
                    <div id="content">
                      {!activityBuffer || activityBuffer.length == 0  ?<LoadingModal/>:''}
                        <CommonHeader {...this.props} />
                        <div className="inner_content bg-light-white clearfix">
                            <div className="content_wrapper">
                                <div className="col-xs-5 col-sm-3 p-0">
                                    <div className="items">
                                        <div className="panel panel-default panel-product-list p-0 bor-customer">
                                            <div className="searchDiv relDiv">
                                            <input type="text" onChange={this.filterCustomerOrder} value={search} className="form-control nameSearch search_customer_input" placeholder="Search by Name, Email or Phone" id="searchUser" data-column-index="0" />

                                                {/* <input type="search" className="form-control nameSearch search_customer_input" placeholder="Search by Name, Email or Phone" /> */}
                                                <svg onClick={()=>this.clearInput()} className="search_customer_input2" width="23" version="1.1" xmlns="http://www.w3.org/2000/svg" height="64" viewBox="0 0 64 64" xmlns="http://www.w3.org/1999/xlink" enableBackground="new 0 0 64 64" >
                                                    <g>
                                                        <path fill="#C5BFBF" d="M28.941,31.786L0.613,60.114c-0.787,0.787-0.787,2.062,0,2.849c0.393,0.394,0.909,0.59,1.424,0.59   c0.516,0,1.031-0.196,1.424-0.59l28.541-28.541l28.541,28.541c0.394,0.394,0.909,0.59,1.424,0.59c0.515,0,1.031-0.196,1.424-0.59   c0.787-0.787,0.787-2.062,0-2.849L35.064,31.786L63.41,3.438c0.787-0.787,0.787-2.062,0-2.849c-0.787-0.786-2.062-0.786-2.848,0   L32.003,29.15L3.441,0.59c-0.787-0.786-2.061-0.786-2.848,0c-0.787,0.787-0.787,2.062,0,2.849L28.941,31.786z"></path>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div className="overflowscroll" id="header-search-button">
                                                {/* <div className="overflowscroll window-header-search"> */}
                                                <table className="table CurrentActivityTable">
                                                    <colgroup>
                                                        <col width="*" />
                                                        <col width="40" />
                                                    </colgroup>
                                                      {activeFilter == true &&
                                                        filteredListOrder && filteredListOrder.map((item, index) => {
                                                           
                                
                                                            return (
                                                                <ActivityFirstView
                                                                    key={index}
                                                                    onClick={() => this.activeClass(item, index)}
                                                                    CreatedOn={this.formatDate(item.date)}
                                                                    status={item.order_status}
                                                                    total={item.total}
                                                                    refunded_amount={item.refunded_amount}
                                                                    time={item.time.slice(1, 5)}
                                                                    className={active == index ? 'customer_active' : null}
                                                                />
                                                            )
                                                        })} 
                                                  {activeFilter == false &&
   
                                                   activityBuffer && activityBuffer.map((item, index) => {
                                                      

                                                        return (
                                                            <ActivityFirstView
                                                                key={index}
                                                                onClick={() => this.activeClass(item, index)}
                                                                CreatedOn={this.formatDate(item.date)}                                                                status={item.order_status}
                                                                total={item.total}
                                                                refunded_amount={item.refunded_amount}
                                                                time={item.time.slice(1, 5)}
                                                                className={active == index ? 'customer_active' : null}
                                                            />
                                                        )
                                                    })}
                                                </table>
                                            </div>
                                       
                                            <div className="createnewcustomer">
                                                <button type="button" className="btn btn-block btn-primary total_checkout bg-blue" onClick={() => this.load()}>Load More</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <ActivitySecondView
                                    status={true}
                                    onClick1={() => { this.windowLocation1('statuscompleted',single_Order_list ? single_Order_list.Content.order_id : '') }}
                                    onClick2={() => { this.windowLocation1('statuspending',single_Order_list ? single_Order_list.Content.order_id : '') }}
                                    printPOP={()=>this.openModal('print',single_Order_list ? single_Order_list.Content : '')}
                                    emailPOP={()=>this.openModal('email',single_Order_list ? single_Order_list.Content : '')}
                                    RefundPOP={()=>this.openModal('refund',single_Order_list ? single_Order_list.Content : '')}
                                    VoidPOP={()=>this.openModal('voidsale',single_Order_list ? single_Order_list.Content : '')}
                                    Reciept={single_Order_list ? single_Order_list.Content.order_id : ''}
                                    User={single_Order_list ? single_Order_list.Content.orderCustomerInfo && single_Order_list.Content.orderCustomerInfo.customer_name : ''}
                                    Orderstatus={single_Order_list ? single_Order_list.Content.order_status : ''}
                                    Totalamount={single_Order_list ? single_Order_list.Content.total_amount : 0}
                                    total_tax={single_Order_list ? single_Order_list.Content.total_tax : 0}
                                    refunded_amount={single_Order_list ? single_Order_list.Content.refunded_amount : 0}
                                    tax_refunded={single_Order_list ? single_Order_list.Content.tax_refunded : 0}
                                    Balance={0}
                                    Details={single_Order_list ? single_Order_list.Content : ''}
                                    CreatedDate={this.state.CreatedDate}
                                    getPdfdateTime={this.state.getPdfdateTime}
                                />
                            </div>
                        </div>
                    </div>
                </div>
              
                <div id="PrintMessage" className="modal modal-wide modal-wide1 fade">
                    <div className="modal-dialog" id="dialog-midle-align">
                        <div className="modal-content">
                        <div className="modal-header">
                            <button type="button" className="close" data-dismiss="modal" aria-hidden="true">
                            <img src="http://netmagnetic.co.in/wp2/wp-content/plugins/pos/resources/img/delete-icon.png"/>
                            </button>
                            <h4 className="error_model_title modal-title" id="epos_error_model_title">Message</h4>
                        </div>
                        <div className="modal-body p-0">
                            <h3 id="epos_error_model_message" className="popup_payment_error_msg">{msgPOP}</h3>
                        </div>
                        <div className="modal-footer p-0">
                            <button type="button" className="btn btn-primary btn-block h66" data-dismiss="modal" aria-hidden="true">OK</button>
                        </div>
                        </div>
                    </div>
                    </div>



            </div>


        );
    }
}

function mapStateToProps(state) {
    const { activities, single_Order_list,filteredListOrder ,mail_success} = state;
    return {
        activities,
        single_Order_list: single_Order_list.items,
        filteredListOrder:filteredListOrder.items,
        mail_success:mail_success.items
    };
}

const connectedActivity = connect(mapStateToProps)(Activity);
export { connectedActivity as Activity };