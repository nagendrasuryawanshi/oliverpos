import React from 'react';
import {ActivityViewTable} from './ActivityViewTable'
import { default as NumberFormat } from 'react-number-format'




// function PrintElem(elem,getPdfdateTime)
// {
//    console.log("getPdfdateTime",getPdfdateTime);
//     Popup(elem);
// }

function PrintElem(data,getPdfdateTime) 
{
    console.log("elemprint",data, getPdfdateTime);
    var mywindow = window.open('', 'my div', 'height=400,width=600');
    mywindow.document.write(`<html><head><meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Dashboard - Oliver</title>
    
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
          <style>
     @media screen {
          @font-face {
            font-family: 'Lato';
            font-style: normal;
            font-weight: 400;
            src: local('Lato Regular'), local('Lato-Regular'), url(https://fonts.gstatic.com/s/lato/v11/qIIYRU-oROkIk8vfvxw6QvesZW2xOQ-xsNqO47m55DA.woff) format('woff');
          }

    body {
            font-family: "Lato", "Lucida Grande", "Lucida Sans Unicode", Tahoma, Sans-Serif;
          }
        }
   
    </style></title>`);
    /*optional stylesheet*/ //mywindow.document.write('<link rel="stylesheet" href="main.css" type="text/css" />');
    mywindow.document.write(`</head><body>`);
    mywindow.document.write(
     ` 
       <div class="printer80" style="width: 100%; display: inline-block; color: #000 ; line-height: normal;">
           <div class="panel panel-default" style="box-shadow: 0 1px 1px rgba(0,0,0,.05); border-radius: 4px;border: 1px solid transparent;background-color: #fff;margin-bottom: 16px;border-color: #ddd;padding: 0px;">
             <div class="panel-heading" style="color: #333;border-color: #ddd;padding: 5px;border-bottom: 1px solid transparent;border-top-left-radius: 3px;
border-top-right-radius: 3px;">
               <!-- <p style="margin: 0px; display: inline-block; text-align: center; margin: auto; line-height: 58px;">
                 <img src="img/oliver.png" style="width: 70px; float: left; ">
                 <strong><i></i></strong>
               </p> -->
                <!--  <div style="display: block; text-align: center;">*****************</div> -->
                 <address 
                   style="margin: 0px;padding: 2px 5px 0px;letter-spacing: 0.6px;text-align: center;line-height: normal;font-size: 15px;text-align: center; color: #000;">
                     <!-- <a href="#" style="color: #000; margin-bottom: 2px; line-height: normal;">wptest.creativemaple.com</a><br> -->
                     ${data.LocationName}
                 </address>
                 <div class="clearfix"></div>
            </div>
            <div style="width: 100%; margin: auto; text-align: center; display: block; overflow: hidden; color: #000;">---------------------------------------------------</div>
             <div class="panel-body">
                 <div style="display:inline-block;width: 100%;">
                   <div style="padding-left: 10px;">
                     <h2 style="text-align: left; margin-top: 10px; margin-bottom: 0px;font-size: 17px;margin-bottom: 0px;  ">Reciept / Tax Invoice </h2>
                      <div style="display: inline-block; float: left; text-align: left;">
                       <p style="margin: 2px 0px 0px;color: #000;letter-spacing: 1.2px; font-size: 14px; width:100%">
                            Invoice: #${data.order_id}
                         <br>
                       </p>
                     </div>
                     <div  style="display: inline-block; float: right; text-align: left; width:100%">
                       <p style="margin: 2px 0px 0px;color: #000;letter-spacing: 1.2px; font-size: 14px; width:100%"> Time: ${getPdfdateTime} </p>
                     </div>
                     <div style="display: inline-block; float: left; text-align: left;">
                       <p style="margin: 2px 0px 0px;color: #000;letter-spacing: 1.2px; font-size: 14px width:100%;">
                         Register: ${data.RegisterName}
                       </p>
                     </div>
                  </div>
                 </div>   
                 <table class="table table-printer-price" style="width: 100%; padding-top: 5px; padding-bottom: 5px;border-collapse: separate;
                   border-spacing: 0px; margin-top: 10px;">
                   <tbody>
                   ${data && data.line_items.map((item, index)=>
                  `  <tr key=${index}>
                       <td style=" text-align: left; border-bottom: 1px dotted #000 ;color:#000; padding: 9px;">${item.quantity}</td>
                       <td style="text-align: left;  border-bottom: 1px dotted #000 ;color:#000; padding: 9px;">${item.name}</td>
                       <td style="text-align: right;  border-bottom: 1px dotted #000 ;color:#000; padding: 9px;">${item.subtotal}</td>
                    </tr>`
                    )}
                   </tbody>
               </table>
               <div class="p-0">
                   <div class="widget" style="background-color: #fff;padding: 2px;border-radius: 0px;margin: 5px 0px;padding: 0px 10px; ">
                       <div class="summary-block" style="">
                           <div class="summary-content" style="padding: 2px 0px;text-align: left; border-bottom: 1px dotted #000;">
                               <div class="summary-head" style="display: inline-block;text-align: left;"><h5 class="summary-title" style="font-size: 16px;color: #1c1e22;margin: 0px; padding:5px 0px; ">$${data.total_amount-data.total_tax}</h5></div>
                               <div class="summary-price" style="display: inline-block; float: right; ">
                                   <p class="summary-text" style="color: #000;font-size: 16px;font-weight: 400;letter-spacing: -1px;margin: 0px;">$29</p>
                                   <!-- <span class="summary-small-text pull-right"></span> -->
                               </div>
                           </div>
                       </div>
                       <div class="summary-block" style="">
                           <div class="summary-content" style="padding: 2px 0px;text-align: left;     border-bottom: 1px dotted #000;">
                              <div class="summary-head" style="display: inline-block;text-align: left;"> 
                               <h5 class="summary-title" style="font-size: 16px;color: #1c1e22;margin: 0px; padding:5px 0px;">Tax</h5>
                               </div>
                               <div class="summary-price" style="display: inline-block; float: right; ">
                                   <p class="summary-text" style="color: #000;font-size: 16px;font-weight: 400;letter-spacing: -1px;margin: 0px;">$${data.total_tax}</p>
                                   <!-- <span class="summary-small-text pull-right"></span> -->
                               </div>
                           </div>
                       </div>
                       <div class="summary-block">
                           <div class="summary-content" style="padding: 2px 0px;text-align: left;     border-bottom: 1px dotted #000;">
                              <div class="summary-head" style="display: inline-block; text-align: left;"> 
                               <h5 class="summary-title" style="font-size: 16px; color: #1c1e22; margin: 0px; padding:5px 0px;">Total</h5>
                               </div>
                               <div class="summary-price" style="display: inline-block; float: right; ">
                                   <p class="summary-text" style="color: #000; font-size: 16px; font-weight: 400; letter-spacing: -1px; margin: 0px; ">$${data.total_amount}</p>
                                   <!-- <span class="summary-small-text pull-right"></span> -->
                               </div>
                           </div>
                       </div>
                   </div>
                 
               </div>
                </div>
                <div style="width: 100%; margin: auto; text-align: center; display: block; overflow: hidden;">---------------------------------------------------</div>
                <div class="panel-footer" style="padding: 5px;letter-spacing: 0.6px;text-align: center;line-height: normal;font-size: 15px;text-align: center;color: #000;text-transform: capitalize;">
                 <p style="margin: 0px;">
                   Thank You Shoping With Us <br>
                </p>
                <div style="width: 100%; margin: auto; text-align: center; display: block; overflow: hidden;">-----------------------------------------------</div>
                 <p style="margin: 0px;">
                   Thank You<br>  
                   <a href="#" style="padding: 5px;letter-spacing: 0.6px;text-align: center;line-height: normal;font-size: 15px;text-align: center;color: #000;text-transform: capitalize; display: inline-block; margin: 2px 0px;">http:www.myshop.com</a><br>
                      Printer With Oliver POS
                 </p><br><br>
                </div>
           </div>
       </div>`
       
   
);
    mywindow.document.write('</body style="text-align: center;></html>');

    mywindow.print();
    mywindow.close();

    return true;
}

 export const ActivitySecondView = (props) => {

    var subtotal = parseFloat( parseFloat( (props.Details.total_amount-props.Details.refunded_amount) ) - parseFloat( props.Details.total_tax-props.Details.tax_refunded) ).toFixed(2);    

//     console.log("PROPS",props);
    
//  var subtotal=props.Details.total_amount-props.Details.total_tax;
//  var sub=subtotal.toString();
   ///console.log("subtotal",subtottotal_amountal);
 
    console.log("subtotal13",props.getPdfdateTime);
        return (
            <div className="col-xs-7 col-sm-9 mt-4">
                <div className="panel panel-custmer">
                    <div className="panel-default">
                        <div className="panel-heading text-center customer_name font18 visible2">
                            {props.CreatedDate}
                        </div>
                        <div className="panel-body customer_history p-0">
                            <div className="col-sm-12">
                                <div className="row">
                                    <div className="col-lg-9 col-sm-8 plr-8">
                                        <form className="customer-detail">

                                            <div className="form-group">
                                                <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 pl-0 mb-4 plr-8">
                                                    <label htmlFor="">Reciept Nr.::</label>
                                                    <div className="col-sm-12 p-0">
                                                        <p> {props.Reciept}</p>
                                                    </div>
                                                </div>
                                                <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 pl-0 mb-4 plr-8">
                                                    <label htmlFor="">Customer Details:</label>
                                                    <div className="col-sm-12 p-0">
                                                        <p className="text-blue">---</p>
                                                    </div>
                                                </div>
                                                <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 pl-0 mb-4 plr-8">
                                                    <label htmlFor="">User:</label>
                                                    <div className="col-sm-12 p-0">
                                                        <p> {props.User}</p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="form-group">
                                                <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 pl-0 mb-4 plr-8">
                                                    <label htmlFor="">Order Status:</label>
                                                    <div className="col-sm-12 p-0">
                                                        <p> {props.Orderstatus}</p>
                                                    </div>
                                                </div>
                                                <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 pl-0 mb-4 plr-8">
                                                    <label htmlFor="">Total Amount:</label>
                                                    <div className="col-sm-12 p-0">
                                                        <p> {(props.Totalamount - props.refunded_amount).toFixed(2)}</p>
                                                    </div>
                                                </div>
                                                <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 pl-0 mb-4 plr-8">
                                                    <label htmlFor="">Balance:</label>
                                                    <div className="col-sm-12 p-0">
                                                        <p> {props.Balance}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                    <div className="col-lg-3 col-sm-4 bl-1 plr-8">
                                        <form action="#" className="w-250-px pl-3">
                                           {props.Orderstatus=='completed'?
                                            <div className="w-100-block button_with_checkbox pb-0 danger" onClick={props.onClick1}>
                                                <input type="radio" id="test1" name="radio-group" />
                                                <label htmlFor="test1" className="label_select_button">Refunded Sale</label>
                                            </div>
                                            :
                                            props.Orderstatus == "pending" || props.Orderstatus == "cancelled" || props.Orderstatus == "lay_away" || props.Orderstatus == "park_sale" || props.Orderstatus == "init sale" || props.Orderstatus == "processing" ?
                                               <div className="w-100-block button_with_checkbox " onClick={props.onClick2}>
                                               <input type="radio" id="test1" name="radio-group" />
                                               <label htmlFor="test1" className="label_select_button">Open Sale</label>
                                           </div>
                                            :
                                              props.Orderstatus == "refunded" ?                                      
                                            <div className="w-100-block button_with_checkbox disabled" >
                                            <input type="radio" id="test1" name="radio-group"  onClick={props.RefundPOP}/>
                                            <label htmlFor="test1" className="label_select_button" >Refunded Sale</label>
                                            </div> 
                                            :
                                              props.Orderstatus == "void_sale" ?
                                               <div className="w-100-block button_with_checkbox disabled"   onClick={props.VoidPOP}>
                                               <input type="radio" id="test1" name="radio-group" />
                                               <label htmlFor="test1" className="label_select_button" >Cancelled</label>
                                           </div>
                                            :null
                                            } 
                                         
                                            <div className="w-100-block text-center mt-1 mb-1 notes">
                                                <div className="line-copy"></div>
                                            </div>
                                            <div className="w-100-block button_with_checkbox" >
                                                <input type="radio" id="test2" name="radio-group" onClick={props.emailPOP} />
                                                <label htmlFor="test2" className="label_select_button">Email Reciept</label>
                                            </div>
                                                 <div className="w-100-block button_with_checkbox">                                    
                                                <input type="radio" id="test3" name="radio-group"  onClick={props.Details !="" ?()=>PrintElem(props.Details,props.getPdfdateTime):props.printPOP}/>
                                                <label htmlFor="test3" className="label_select_button">Print Reciept</label>
                                            </div>
                                            
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div className="panel-footer bt-0 p-0">
                            <div className="table_head">
                                <table className="table CurrentActivityTable_shortingHead">
                                    <thead>
                                        <tr>
                                            <th colSpan="4">Item</th>
                                        </tr>
                                    </thead>
                                </table>
                            </div>
                            <div className="overflowscroll window-header-cname_chistory">
                                <table className="table CurrentActivityTable_shortingBody">
                                    <colgroup>
                                        <col width="50" />
                                        <col width="*" />
                                        <col width="120" />
                                    </colgroup>
                                    <tbody>
                                        {props.Details?
                                         props.Details.line_items.map((item, index) => {
                                            return (
                                                <tr key={index}>
                                                    <td>
                                                        {
                                                            (item.quantity_refunded < 0) ? <div>{item.quantity+item.quantity_refunded}
                                                            <del style={{marginLeft:5}}>{item.quantity}</del></div> : item.quantity
                                                        }
                                                    </td>
                                                    <td>{item.name}</td>
                                                    <td className="text-right">
                                                        { 
                                                            (item.amount_refunded > 0) ? <div>{parseFloat(item.total-item.amount_refunded).toFixed(2)} <del style={{marginLeft:5}}>{parseFloat(item.total).toFixed(2)}</del></div> : parseFloat(item.total).toFixed(2) 
                                                        }
                                                        {/* <NumberFormat value={item.total} displayType={'text'} thousandSeparator={true} decimalScale={2} fixedDecimalScale={true} /> */}
                                                    </td>
                                                </tr>                                            
                                            )
                                        })
                                        :<tr></tr>
                                    }
                                    </tbody>
                                </table>
                                <div className="col-lg-offset-7 col-lg-5 col-sm-offset-6 col-sm-6 col-xs-offset-4 col-xs-8 p-0">
                                    <table className="table tableTotalCalculated">
                                      { props.Details ? 
                                        <ActivityViewTable                                             
                                            Subtotal = { subtotal ? subtotal : 0}
                                            Discount = { props.Details.discount }
                                            TotalTax = { props.Details.total_tax }
                                            tax_refunded = { props.Details.tax_refunded }
                                            TotalAmount = { props.Details.total_amount }
                                            refunded_amount = { props.Details.refunded_amount }                                            
                                            OrderPayment = { props.Details.order_payments }
                                            refundPayments = { props.Details.order_Refund_payments }
                                        />
                                     : "" }
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        )
    
    }
