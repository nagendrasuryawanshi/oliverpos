import Config from '../Config'

export const allProductService = {
    getAll,
    getAttributes,
    refresh
};

const API_URL=Config.key.OP_API_URL

function refresh() {
    // remove user from local storage to log user out
  
}
function getAll() {
    const requestOptions = {
        method: 'GET',
     };

    var UDID=localStorage.getItem("UDID")
    console.log("UDID",UDID);
    return fetch(`${API_URL}/ShopData/GetAllProducts?Udid=${UDID}`, requestOptions)
    .then(handleResponse)
    .then(productlst=>{
        console.log(productlst.Content);
        return productlst.Content;
    });
}
function getAttributes() {
    const requestOptions = {
        method: 'GET',
     };

    var UDID=localStorage.getItem("UDID")
    console.log("UDID",UDID);
    return fetch(`${API_URL}/ShopSetting/GetAttributes?Udid=${UDID}`, requestOptions)
    .then(handleResponse)
    .then(attributelist=>{
        console.log(attributelist.Content);
        return attributelist.Content;
    });
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
               // logout();
                location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}