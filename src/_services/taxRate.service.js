import Config from '../Config'

export const taxRateService = {
    getAll,  
    refresh
};

const API_URL=Config.key.OP_API_URL

function refresh() {
    // remove user from local storage to log user out
  
}
function getAll() {
    var UserLocation=localStorage.getItem('UserLocations')?localStorage.getItem('UserLocations'):[] ;
    var UDID=localStorage.getItem("UDID");
      var loc_country="";
      var loc_country_state="";
      var loc_city="";
      var loc_postcode="";
      var loc_address_1="";

      //[{"Id":1,"Name":"Oliver POS Shop","Prefix":"L","Order":1,"Zip":"E2H 0A9","City":"Sent John's","CountryName":"CA","StateName":"NL","Address1":"Sent John's Shop","Address2":null,"DispayName":"L1"}]

    if(UserLocation.length>0){
         loc_country=UserLocation[0].CountryName;
         loc_country_state=UserLocation[0].StateName;
         loc_city=UserLocation[0].City;
         loc_postcode=UserLocation[0].Zip;
         loc_address_1=UserLocation[0].Address1;
    }
     console.log("UserLocation123",UserLocation); 

    console.log("UDID",UDID);

    const requestOptions = {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
          },
        
           body: JSON.stringify({ UDID, loc_country ,loc_country_state,loc_city,loc_postcode,loc_address_1})
      //  body: "udid=4040246102&loc_country=CA&loc_country_state=NL&loc_city=Sent John's&loc_postcode=&loc_address_1=Sent John's Shop"        
     };

  
    return fetch(`${API_URL}/ShopSetting/GetRate`, requestOptions) //?Udid=${UDID}
    .then(handleResponse)
    .then(taxratelst=>{
        console.log("taxratelst123",taxratelst.Content);
        return taxratelst.Content;
    });
}


function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
               // logout();
                location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}