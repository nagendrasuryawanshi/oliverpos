import Config from '../Config'
import { authHeader ,history} from '../_helpers';


export const userService = {
    login,
    logout,
    register,
    getAll,
    getById,
    update,
    delete: _delete
};
const API_URL=Config.key.OP_API_URL

function login(email, password) {
    console.log("login Jason", JSON.stringify({ email, password }))
    const requestOptions = {
        method: 'POST',           
      //  mode: 'no-cors',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
    
        body: JSON.stringify({ email, password })
    };
    //return fetch(`${config.apiUrl}/users/authenticate`, requestOptions)
    return fetch(`${API_URL}/Subscription/DeviceLogin`, requestOptions)
        .then(handleResponse)
        .then(user => {
            console.log("Login Response",user.Content[0]);
           // login successful if there's a jwt token in the response
            if (user.Content[0].UDID) {
                // store user details and jwt token in local storage to keep user logged in between page refreshes
                //localStorage.setItem('user', JSON.stringify(user));
                const requestOptions1 = {
                    method: 'GET',    
                  headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                  } };
                return fetch(`${API_URL}/ShopAccess/GetLocations?udid=`+user.Content[0].UDID,requestOptions1)
                .then(handleResponse)
                .then(location => {
                    console.log("location Response",location);
                    localStorage.setItem('user', JSON.stringify(user));
                    localStorage.setItem('UserLocations', JSON.stringify(location.Content));

                    return user;

                }).catch(error=>{
                      Message:"Error- No location found"  
                })
                
            }
            
        });
}


function logout() {
    // remove user from local storage to log user out
     localStorage.removeItem('user');
     //localStorage.removeItem('UserLocations');
     //localStorage.removeItem('UDID');
    history.push('/login')
}

function getAll() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${config.apiUrl}/users`, requestOptions).then(handleResponse);
}

function getById(id) {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    };

    return fetch(`${config.apiUrl}/users/${id}`, requestOptions).then(handleResponse);
}

function register(user) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(user)
    };

    return fetch(`${config.apiUrl}/users/register`, requestOptions).then(handleResponse);
}

function update(user) {
    const requestOptions = {
        method: 'PUT',
        headers: { ...authHeader(), 'Content-Type': 'application/json' },
        body: JSON.stringify(user)
    };

    return fetch(`${config.apiUrl}/users/${user.id}`, requestOptions).then(handleResponse);;
}

// prefixed function name with underscore because delete is a reserved word in javascript
function _delete(id) {
    const requestOptions = {
        method: 'DELETE',
        headers: authHeader()
    };

    return fetch(`${config.apiUrl}/users/${id}`, requestOptions).then(handleResponse);
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                logout();
                location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}