import Config from '../Config'

export const discountService = {
    getAll,  
    refresh
};

const API_URL=Config.key.OP_API_URL

function refresh() {
    // remove user from local storage to log user out
  
}
function getAll() {
    const requestOptions = {
        method: 'GET',
     };

    var UDID=localStorage.getItem("UDID")
    console.log("UDID",UDID);
    return fetch(`${API_URL}/ShopSetting/GetDiscount?Udid=${UDID}`, requestOptions)
    .then(handleResponse)
    .then(discountlst=>{
        console.log(discountlst.Content);
        return discountlst.Content;
    });
}


function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
               // logout();
                location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}