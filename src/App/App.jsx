import React from 'react';
import { Router, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import { history } from '../_helpers';
import { alertActions } from '../_actions';
import { PrivateRoute } from '../_components';
import { HomePage } from '../HomePage';
import { LoginPage } from '../LoginPage';
import { RegisterPage } from '../RegisterPage';
import { Activity } from '../ActivityPage';
import { CustomerView } from '../CustomerPage';
import { CheckoutView } from '../CheckoutPage';
import { RefundView } from '../RefundPage';
import { CashReportView } from '../CashReportPage';
import { SettingView } from '../SettingPage';
import { SiteLinkView } from '../SiteLinkPage';
import { LoginRegisterView } from '../LoginRegisterPage';
import { PinPage } from '../LoginPin/PinPage';
import { ShopView } from '../ShopView';
import { ListView } from '../ListView';
import { LoginLocation } from '../LoginLocation';
import { CompleteRefund } from '../CheckoutPage/components/CompleteRefund';
import { RefundComplete } from '../RefundPage/components/refund_complete'; //by deepsagar
import { InternalErr } from '../Error/InternalErr'
 

class App extends React.Component {
    constructor(props) {
        super(props);

        const { dispatch ,alert} = this.props;
        history.listen((location, action) => {
            // clear alert on location change
            dispatch(alertActions.clear());
        });
        
    }

    render() {
        const { alert } = this.props;
        console.log("alert",alert)
        
        return (
            // <div className={"bgimg-1"}>
                <Router history={history}>
                    <div>
                        <PrivateRoute exact path="/" component={ShopView} />                       
                        <PrivateRoute path="/shopview" component={ShopView} />
                        <PrivateRoute path="/customerview" component={CustomerView} />
                        <PrivateRoute path="/checkout" component={CheckoutView} />
                        <PrivateRoute path="/setting" component={SettingView} />
                        <PrivateRoute path="/cash_report" component={CashReportView} />
                        <PrivateRoute path="/activity" component={Activity} />
                        <PrivateRoute path="/refund" component={RefundView} />                     
                        <PrivateRoute path="/listview" component={ListView} />                      
                        <PrivateRoute path="/choose_registration" component={LoginRegisterView} />
                        <PrivateRoute path="/complete_refund" component={CompleteRefund} /> 
                        <PrivateRoute path="/refund_complete" component={RefundComplete} /> 

                        <Route path="/login" component={LoginPage} />
                        <Route path="/register" component={RegisterPage} />
                        <Route path="/login_location" component={LoginLocation} /> 
                        <Route path="/site_link" component={SiteLinkView} />
                        <Route exact  path="/loginpin" component={PinPage} /> 
                        <Route path='/Error' component={InternalErr} />
                    </div>
                </Router>

            // </div>
        );
    }
}

function mapStateToProps(state) {
    const { alert } = state;
    return {
        alert
    };
}

const connectedApp = connect(mapStateToProps)(App);
export { connectedApp as App }; 