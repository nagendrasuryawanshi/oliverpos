import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { favouriteListActions } from '../action/favouritelist.actions';
import { alert } from '../../_reducers/alert.reducer';
import {FavouriteItemView } from '../components/FavouriteItemView'
import { cartProductActions}  from '../../_actions/cartProduct.action'


class FavouriteList extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            active: false,
            item:null,
            num:'',
            UDID:localStorage.getItem('UDID'),
            activeClass:null
        }

        this.ActiveList = this.ActiveList.bind(this);
        this.tileProductListFilter = this.tileProductListFilter.bind(this);
    }

    componentWillMount() {
        const { dispatch } = this.props;
        const UID = localStorage.getItem('UDID');
        dispatch(favouriteListActions.getAll(UID, 1));
    }

    tileProductListFilter( data, type ){ 
        this.props.tileFilterData(data, type)
    }

    ActiveList(item, index, type) {  
        console.log("item12",item);
        this.tileProductListFilter( item, type );
        if(item.attribute_slug){
            this.setState({
                active: true,
                item: item,
                num:index
            })
            this.props.dispatch(favouriteListActions.getSubAttributes(this.state.UDID,item.attribute_slug));

        }
      if(item.category_id){
        this.setState({
            active: true,
            item: item,
            num:index
            // , ListItem2:list.product_list
        })
        this.props.dispatch(favouriteListActions.getChildCategories(this.state.UDID,item.category_id));

      }
      if(item.parent_attribute){
        this.setState({
            active: true,
            item: item,
            num:index
            // , ListItem2:list.product_list
        })      

      } 
      if(item.category_slug){
        this.setState({
            active: true,
            item: item,
            num:index
            // , ListItem2:list.product_list
        })

      } 
      if(item.Product_Id){
      
        if(item.Type == "variable"){
            // console.log("variable",item);
            var data ={
             item:item,
             DefaultQunatity:1  
            }     
            this.props.productData( data );
            $('#variationProductPopup').modal('show');
            localStorage.setItem('Favproduct',JSON.stringify(data))      
             //this.props.dispatch(favouriteListActions.variationProdList(data));
            }
          //  const { cartproductlist } = this.props; 
            
          var  cartlist= localStorage.getItem("CARD_PRODUCT_LIST") ? JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST") ): []

          console.log("FavouritAddToCart",cartlist)
          console.log("FavouritAddToCart",item.Stock ,item.Type)
          //  var cartlist=cartproductlist?cartproductlist:[];//this.state.cartproductlist;    
            if(item.Type == "simple"){  
                

                if(parseFloat(item.Stock)<=0 )
                {
                    console.log("Outof sctock");
                    $('#outOfStockModal').modal('show');
                }
                else
                {
                 console.log("Simple1");
                 localStorage.setItem('Favproduct',null)             
                 cartlist.push(item)              
                console.log("pranav",cartlist);
                this.props.dispatch(cartProductActions.addtoCartProduct(cartlist));
                }
             }  

      }  
      
    }

        RemoveFavProduct(data){
            console.log("data",data);
            if(data.category_slug){
                const UID = localStorage.getItem('UDID');
                const  favid=data.id
                this.props.dispatch(favouriteListActions.favProductRemove(UID, favid));

            }
            if(data.attribute_slug){

               const UID = localStorage.getItem('UDID');
               const  favid=data.id
               this.props.dispatch(favouriteListActions.favProductRemove(UID, favid));
  
             }
             if(data.Product_Id){  
                const UID = localStorage.getItem('UDID');
                const  favid=data.Id
                this.props.dispatch(favouriteListActions.favProductRemove(UID, favid));
             }
             if(data.parent_attribute){
                const UID = localStorage.getItem('UDID');
                const  favid=data.id
                this.props.dispatch(favouriteListActions.favProductRemove(UID, favid));
             }

          }
 
 
    render() {
        const { favourites , favouritesChildCategoryList,favouritesSubAttributeList} = this.props
        const { active, item,num ,childCategoryList} = this.state;
        const favouritesItems = favourites.items;
        const favouritesItemsAttribute = favouritesItems && favouritesItems.FavAttribute;
        const favouritesItemsCategory = favouritesItems && favouritesItems.FavCategory;
        const favouritesItemsProduct = favouritesItems && favouritesItems.FavProduct;
        const favouritesItemsSubCategory = favouritesItems && favouritesItems.FavSubCategory;
        const favouritesItemsSubAttribute = favouritesItems && favouritesItems.FavSubAttribute;
        console.log("this.props.removeSubAttributes",favourites);
        return (

            <div className="col-lg-8 col-md-7 col-sm-7 plr-8 pl-0">
                { active != true ? 
                <div className="category list-unstyled overflowscroll window-header">
                 

                    <div className="pt-3 clearfix">
                       
                        { 
                            // l oop for display favorite attributes
                            favouritesItems && favouritesItemsAttribute.map((item, index) => {
                                return (
                                    <div key={index} className="col-lg-3 col-md-4 col-sm-4 col-xs-6 plr-8 relativeDiv pr-0 pos-product-list-filter" data-attribute-id={item.attribute_id} data-id={item.id}  >
                                        <div className="category_list">
                                            <a href="javascript:void(0)" onClick={() => this.ActiveList(item, 1, "attribute")}>
                                                {item.attribute_slug}
                                            </a>
                                        </div>
                                        <a className="delete" href="javascript:void(0)">
                                            <span aria-hidden="true"  onClick={()=>this.RemoveFavProduct(item)}>x</span>
                                            <span className="sr-only">Close</span>
                                        </a>
                                    </div>
                                )
                            })
                        
                            // loop for display favorite attributes
                        }


                        {
                            // loop for display favorite sub attributes
                            favouritesItems && favouritesItemsSubAttribute.map((item, index) => {
                                console.log("item1",item);
                                return (
                                    <div key={index} className="col-lg-3 col-md-4 col-sm-4 col-xs-6 plr-8 relativeDiv pr-0 pos-product-list-filter" data-attribute-id={item.attribute_id} data-id={item.id} data-parent-attribute={item.parent_attribute} >
                                        <div className="category_list" onClick={() => this.ActiveList(item, 3, "sub-attribute")}>
                                            <a href="javascript:void(0)">
                                                {item.attribute_slug} / {item.parent_attribute.replace("pa_", "")}
                                            </a>
                                        </div>
                                        <a className="delete" href="javascript:void(0)">
                                            <span aria-hidden="true"  onClick={()=>this.RemoveFavProduct(item)}>x</span>
                                            <span className="sr-only">Close</span>
                                        </a>
                                    </div>
                                )
                            })
                            // loop for display favorite sub attributes
                        }

                        {
                            // loop for display favorite category
                            favouritesItems && favouritesItemsCategory.map((item, index) => {
                                return (
                                    <div key={index} className="col-lg-3 col-md-4 col-sm-4 col-xs-6 plr-8 relativeDiv pr-0 pos-product-list-filter" data-category-id={item.category_id} data-id={item.id} data-category-slug={item.category_slug}  >
                                        <div className="category_list" onClick={() =>this.ActiveList(item, 2, "category")}>
                                            <a href="javascript:void(0)">
                                                {item.name}
                                            </a>
                                        </div>
                                        <a className="delete" href="javascript:void(0)">
                                            <span aria-hidden="true"  onClick={()=>this.RemoveFavProduct(item)}>x</span>
                                            <span className="sr-only">Close</span>
                                        </a>
                                    </div>
                                )
                            })
                            // loop for display favorite category
                        }

                        {
                            // loop for display favorite category
                            favouritesItems && favouritesItemsSubCategory.map((item, index) => {

                                return (
                                    <div key={index} className="col-lg-3 col-md-4 col-sm-4 col-xs-6 plr-8 relativeDiv pr-0 pos-product-list-filter" data-category-id={item.category_id} data-id={item.id} data-category-slug={item.category_slug}  >
                                        <div className="category_list" onClick={() =>this.ActiveList(item, 4, "sub-category")}>
                                            <a href="javascript:void(0)">
                                                {item.name}
                                            </a>
                                        </div>
                                        <a className="delete" href="javascript:void(0)">
                                            <span aria-hidden="true"  onClick={()=>this.RemoveFavProduct(item)}>x</span>
                                            <span className="sr-only">Close</span>
                                        </a>
                                    </div>
                                )
                            })
                            // loop for display favorite category
                        }

                        {
                            // loop for display favorite product
                            favouritesItems && favouritesItemsProduct.map((item, index) => {
                                return (
                                    <div key={index} className="col-lg-3 col-md-4 col-sm-4 col-xs-6 plr-8 relativeDiv pr-0 pos-product-list-filter" data-product-id={item.Product_Id} data-id={item.Id} data-stock={item.stock} data-price={item.Price} >
                                        <div className="category_list labelAdd" onClick={() =>this.ActiveList(item, 5, "product")}>
                                            <a href="javascript:void(0)">
                                                <img src={item.Image} alt="new" />
                                            </a>
                                            <label className="labelTag">{item.Title}</label>
                                        </div>
                                        <a className="delete" href="javascript:void(0)">
                                            <span aria-hidden="true"  onClick={()=>this.RemoveFavProduct(item)}>x</span>
                                            <span className="sr-only">Close</span>
                                        </a>
                                    </div>
                                )
                            })
                            // loop for display favorite product
                        }

                        <div className="col-lg-3 col-md-4 col-sm-4 col-xs-6 plr-8 pr-0">
                            <div className="category_list labelRemove" href="#tallModal" data-toggle="modal">
                                <a>
                                    <img src="assets/img/add_gry.png" />
                                </a>
                                <a className="delete" href="#">
                                    <span aria-hidden="true"  >x</span>
                                    <span className="sr-only">Close</span>
                                </a>
                            </div>
                        </div>

                    </div>
                </div>
                :
                (
                    num==1?
                   < FavouriteItemView
                        SubAttributeList={favouritesSubAttributeList.items?favouritesSubAttributeList.items:null}
                        tileFilter={this.tileProductListFilter}
                      />
                   : num==2?
                   < FavouriteItemView
                        childCategory={favouritesChildCategoryList.items?favouritesChildCategoryList.items:null}
                        tileFilter={this.tileProductListFilter}
                      />:
                      num==3?
                      < FavouriteItemView
                        childSubAttributeList={this.state.item?item:null}
                       />:
                       num==4?
                       < FavouriteItemView
                            subchildCategory={this.state.item?item:null}
                            tileFilter={this.tileProductListFilter}
                        />:
                      null
                )
            } 
            </div>
        )
    }
}


function mapStateToProps(state) {
    const { favourites,favouritesChildCategoryList,favouritesSubAttributeList,favProductDelete } = state;
    return {
        favourites,
        favouritesChildCategoryList,
        favouritesSubAttributeList,
        favProductDelete
    };
}

const connectedFavouriteList = connect(mapStateToProps)(FavouriteList);
export { connectedFavouriteList as FavouriteList };