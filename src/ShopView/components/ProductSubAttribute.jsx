import React from 'react';
import { connect } from 'react-redux';

class ProductSubAttribute extends  React.Component {
          
        constructor(props) { 
            super(props);    
        }

        render() {
             const ProductSubAttribute = this.props;
            //  console.log("ProductSubAttribute", ProductSubAttribute);
             return (              
                ProductSubAttribute.options ? (
                    ProductSubAttribute.options.map((option, index) => {
                        return(
                            <div className="col-sm-3" key={index}>
                                 <div className="button_with_checkbox">
                                     <input type="radio" id={`option${option}`} name={`variation-option-${ProductSubAttribute.parentAttribute}`} value={option} onClick={this.props.click.bind(this, option, ProductSubAttribute.parentAttribute)}/>
                                     <label htmlFor={`option${option}`} className="label_select_button">{option}</label>
                                 </div>
                             </div>
                        )
                    })
                ) : ""
             ) 
        }
    
    }
    
    
function mapStateToProps(state) {
    const { registering } = state.registration;
    return {
        registering
    };
}

const connectedShopView = connect(mapStateToProps)( ProductSubAttribute );
export { connectedShopView as ProductSubAttribute };
//export default ProductAttribute;