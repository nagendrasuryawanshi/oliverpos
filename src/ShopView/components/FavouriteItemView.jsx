import React from 'react';
import { connect } from 'react-redux';
import { history } from '../../_helpers'
import { favouriteListActions } from '../action/favouritelist.actions';

class FavouriteItemView extends React.Component {
    constructor(props) {
        super(props);
       this.state={
       }
       this.tileFilterData = this.tileFilterData.bind(this);
    }

    redirect(){
        history.go();
    } 

    tileFilterData(data, type, index){
        this.props.tileFilter(data, type);

        switch ( type ) {
            case "inner-sub-attribute":
              $('.inner-sub-attribute a').removeAttr("style");
              $(`#${index}_sub_attribute a`).css("color", "#A9D47D");
              break;
            case "inner-sub-category":
              $('.inner-sub-category a:first').removeAttr("style");
              $(`#${data}_sub_attribute a:first`).css("color", "#A9D47D");
              break;
          }
    }

    RemoveFavProduct(data){
       console.log("data",data);
       const UID = localStorage.getItem('UDID');
       const  favid=data.id
       this.props.dispatch(favouriteListActions.favProductRemove(UID, favid));


      // this.redirect()
    }

    render() {      
     console.log("this.props12",this.props);
      return (
         <div className="col-lg-8 col-md-7 col-sm-7 plr-8 pl-0">
            <div className="pt-3 clearfix">
                <div className="col-lg-3 col-md-4 col-sm-4 col-xs-6 plr-8 relativeDiv pr-0" onClick={()=>this.redirect()}>

                    <div className="category_list bg_green">
                        <a href="javascript:void(0)">
                            Back
                   </a>
                    </div>
                </div>
            
            {
                this.props.childCategory && this.props.childCategory.map((item, index) => {
                    return (                 
                    <div key={index} id={`${index}_sub_category`} className="col-lg-3 col-md-4 col-sm-4 col-xs-6 plr-8 relativeDiv pr-0 inner-sub-category" data-attribute-id={item.attribute_id} data-id={item.id} onClick={ () => this.tileFilterData(item, "inner-sub-category", index) } >
                            <div className="category_list">
                                <a href="javascript:void(0)">
                                    {item.Value}
                                </a>
                            </div>
                            {/* <a className="delete" href="javascript:void(0)">
                                <span aria-hidden="true">x</span>
                                <span className="sr-only">Close</span>
                            </a> */}
                        </div>
                    )
                })
                // loop for display favorite product
            }
             {
                this.props.SubAttributeList && this.props.SubAttributeList.map((item, index) => {
                    return (
                        <div key={index} id={`${index}_sub_attribute`} className="col-lg-3 col-md-4 col-sm-4 col-xs-6 plr-8 relativeDiv pr-0 inner-sub-attribute" data-attribute-id={item.attribute_id} data-id={item.id} onClick={() => this.tileFilterData(item.Code, "inner-sub-attribute", index)}>
                            <div className="category_list">
                                <a href="javascript:void(0)">
                                    {item.Value}
                                </a>
                            </div>
                            {/* <a className="delete" href="javascript:void(0)">
                                <span aria-hidden="true">x</span>
                                <span className="sr-only">Close</span>
                            </a> */}
                        </div>
                    )
                })
                // loop for display favorite product
            }
             {    this.props.childSubAttributeList && this.props.childSubAttributeList ?
                               
                                    <div  className="col-lg-3 col-md-4 col-sm-4 col-xs-6 plr-8 relativeDiv pr-0" >
                                    <div className="category_list">
                                        <a href="javascript:void(0)">
                                        {this.props.childSubAttributeList.attribute_slug} / {this.props.childSubAttributeList.parent_attribute.replace("pa_", "")} 
                                        </a>
                                    </div>
                                     <a className="delete" href="javascript:void(0)" >
                                        <span aria-hidden="true"  onClick={()=>this.RemoveFavProduct(this.props.childSubAttributeList)} >x</span>
                                        <span className="sr-only">Close</span>
                                    </a> 
                                </div>:null
                                
                           
                            // loop for display favorite sub attributes
                        }
                     {    this.props.subchildCategory && this.props.subchildCategory ?
                                <div  className="col-lg-3 col-md-4 col-sm-4 col-xs-6 plr-8 relativeDiv pr-0 inner-sub-category" id={`${this.props.subchildCategory.category_slug}_sub_category`}>
                                    <div className="category_list" onClick={ () => this.tileFilterData(this.props.subchildCategory.category_slug, "inner-sub-category", this.props.subchildCategory.name.category_slug) } >
                                        <a href="javascript:void(0)" >
                                        {this.props.subchildCategory.name}
                                        </a>
                                    </div>
                                     <a className="delete" href="javascript:void(0)">
                                        <span aria-hidden="true" onClick={()=>this.RemoveFavProduct(this.props.subchildCategory)}>x</span>
                                        <span className="sr-only">Close</span>
                                    </a> 
                                </div>:null
                                
                           
                            // loop for display favorite sub attributes
                        }    
            </div>
        </div>
        )
    }

}

function mapStateToProps(state) {
   // console.log("refundfirstState",state)
    const { refundlist } = state;
    return {
        refundlist,
    
    };
}
const connectedFavouriteItemView = connect(mapStateToProps)(FavouriteItemView);
export { connectedFavouriteItemView as FavouriteItemView };

