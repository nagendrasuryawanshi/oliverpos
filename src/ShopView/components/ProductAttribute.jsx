import React from 'react';
import { connect } from 'react-redux';
import { ProductSubAttribute } from './ProductSubAttribute';

class ProductAttribute extends  React.Component {
          
        constructor(props) { 
            super(props);
          // alert()
            this.state = {
                getAttributes : null
            }    

            this.handleOptionClick = this.handleOptionClick.bind(this);          
        }

        handleOptionClick(option, attribute){     
              
            this.props.optionClick(option, attribute);
        }

        handleClick(){
            alert();
        }

        render() {
            const ProductAttribute = this.props;             
             return (
              
                ProductAttribute.attribute ?
                 ( ProductAttribute.attribute.map((attribute, index) => 
                 {
                 return (
                        <div  className="col-md-12 col-sm-12 text-center" key={index}>
                            <div className="line-2"></div>
                            <label className="color mb-2">{attribute.Name}</label>
                            <div className="row">
                                    {/* for getting attribute options */}
                                    <ProductSubAttribute options={attribute.Option} parentAttribute={attribute.Slug} click={this.handleOptionClick}/>

                                <div className="clearfix"></div>
                            </div>
                        </div>
                 )
                    }
                 
                    )
                 ) : ""
             )
        }
    
    }
    
    
function mapStateToProps(state) {
    const { registering } = state.registration;
    return {
        registering
    };
}

const connectedShopView = connect(mapStateToProps)(ProductAttribute);
export { connectedShopView as ProductAttribute };
//export default ProductAttribute;