import React from 'react';
import { connect } from 'react-redux';
import { NavbarPage, CommonHeaderTwo, CustomerAddFee, CustomerNote } from '../_components';
import { FavouriteList } from './components/FavouriteList';
import { CartListView } from '../_components/CartListView'
import { AllProduct } from '../_components/AllProduct';
import { ProductAttribute } from './components/ProductAttribute';
import { cartProductActions } from '../_actions/cartProduct.action'
import { history } from '../_helpers';
import { taxRateAction } from '../_actions/taxRate.action'
import { DiscountPopup } from '../_components/DiscountPopup'
import { TileModel } from '../_components/TileModel'
import { ProductOutOfStockPopupModel } from '../_components/ProductOutOfStockPopupModel'
import { discount_popup } from '../_components/CalculatorFunction';


class ShopView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            getVariationProductData: null,
            hasVariationProductData: false,
            variationoptionArray: {},
            discountAmount: 0,
            discountType: "",
            variationCombination: [],
            tileFilterProductData: null,
            tileFilterProducttype: null,
            // variationProductList:localStorage.getItem('Favproduct')
        }
        this.handleProductData = this.handleProductData.bind(this);
        this.optionClick = this.optionClick.bind(this);
        this.incrementDefaultQuantity = this.incrementDefaultQuantity.bind(this);
        this.decrementDefaultQuantity = this.decrementDefaultQuantity.bind(this);
        this.setDefaultQuantity = this.setDefaultQuantity.bind(this);
        this.handleDiscount = this.handleDiscount.bind(this);


        this.addVariationProductToCart = this.addVariationProductToCart.bind(this);
        this.handletileFilterData = this.handletileFilterData.bind(this);


        // const { dispatch } = this.props;
        // dispatch(discountActions.getAll());          
        const { dispatch } = this.props;
        dispatch(taxRateAction.getAll());


        // dispatch(categoriesActions.getAll());
    }



    componentWillMount() {
        
        // localStorage.removeItem("CARD_PRODUCT_LIST")
        if (!localStorage.getItem('user')) {
            history.push('/loginpin');
        }
        var udid = localStorage.getItem("UDID")
        console.log("local cartproductlist", localStorage.getItem("cartproductlist-" + udid) ?
            localStorage.getItem("cartproductlist-" + udid) : null);
        // var cartlist= JSON.parse(localStorage.getItem("cartproductlist-"+udid));
        // console.log("cartlist",cartlist);
        // const { dispatch } = this.props;   
        // dispatch(cartProductActions.addtoCartProduct(localStorage.getItem("cartproductlist-"+localStorage.getItem("UDID"))));  


        setTimeout(function () {
            //Put All Your Code Here, Which You Want To Execute After Some Delay Time.
            setHeightDesktop();

        }, 1000);

    }

    handleProductData(productData) {

        var filterdata = [];
        var allProductList = [];
        allProductList.push(this.props.productlist)
        if (productData.item) {
            allProductList[0].productlist.map(item => {
                //  console.log("productData.item.Product_Id",productData.item.Product_Id,item.WPID)
                if (productData.item.Product_Id == item.WPID) {
                    filterdata.push(item)
                }
            })

        }

        var product = ''
        if (!productData.item) {
            product = productData;
        }

        this.setState({
            getVariationProductData: product ? product : filterdata[0],
            hasVariationProductData: true,
            loadProductAttributeComponent: true,
            variationOptionclick: 0,
            variationTitle: productData.Title ? productData.Title : productData.item ? productData.item.Title : '',
            variationId: 0,
            variationPrice: productData.Price ? productData.Price : productData.item ? productData.item.Price : '',
            variationStockQunatity: productData.StockQuantity ? productData.StockQuantity : productData.item ? productData.item.Stock : '',
            variationImage: productData.ProductImage ? productData.ProductImage : productData.item ? productData.item.Image : '',
            variationDefaultQunatity: 1 ? 1 : productData ? productData.DefaultQunatity : ''

        });

    }

    optionClick(option, attribute) {

        let attributeLenght = this.getAttributeLenght();
        this.state.variationoptionArray[attribute] = option;
        let variationOptionclick = (Object.keys(this.state.variationoptionArray)).length;
        if (attributeLenght == variationOptionclick) {
            this.searchvariationProduct(this.state.variationoptionArray);

        }
    }

    searchvariationProduct(combinations) {
        console.log("combinations");
        let combination = new Array();

        for (var key in combinations) {
            if (combinations.hasOwnProperty(key)) {
                combination.push(combinations[key]);
                this.setState({ variationCombination: combination })
            }
        }

        let variations = this.state.getVariationProductData.Variations;
        let rCombination = combination.join("~");
        let lCombination = (combination.reverse()).join("~");

        var found = variations.find(function (element) {
            if (element.combination == rCombination.toLowerCase() || element.combination == lCombination.toLowerCase()) {
                return element;
            }
        });

        if (typeof found !== 'undefined') {
            this.setState({
                variationTitle: found.Title,
                variationId: found.WPID,
                variationParentId: found.ParentId,
                variationPrice: found.Price,
                variationStockQunatity: found.StockQuantity,
                variationImage: (found.ProductImage == null) ? this.state.variationImage : found.ProductImage,
                variationIsTaxable: found.Taxable,
            });
            $("#add_variation_product_btn").css({ "cursor": "pointer", "pointer-events": "auto" });
        }

    }

    getAttributeLenght() {
        return this.state.getVariationProductData.ProductAttributes.length;
    }

    incrementDefaultQuantity() {
        // alert();

        if (this.state.variationDefaultQunatity) {
            let qty = parseInt(this.state.variationDefaultQunatity);
            qty++;
            this.setDefaultQuantity(qty);
        }

    }



    decrementDefaultQuantity() {
        if (this.state.variationDefaultQunatity && this.state.variationDefaultQunatity > 1) {
            let qty = parseInt(this.state.variationDefaultQunatity);
            qty--;
            this.setDefaultQuantity(qty);
        }
    }

    addVariationProductToCart() {
        var data = {
            line_item_id: 0,
            quantity: this.state.variationDefaultQunatity,
            Title: this.state.variationTitle,
            Price: parseInt(this.state.variationDefaultQunatity) * parseFloat(this.state.variationPrice),
            product_id: this.state.variationParentId,
            variation_id: this.state.variationId,
            isTaxable: this.state.variationIsTaxable,
        }

        let cartItemListHolder = [];
        let cartItemList = localStorage.getItem("CARD_PRODUCT_LIST") ? JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST")) : []

        if (cartItemList == null) {
            cartItemListHolder.push(data);
            this.props.dispatch(cartProductActions.addtoCartProduct(cartItemListHolder));
        } else {
            cartItemList.push(data)
            this.props.dispatch(cartProductActions.addtoCartProduct(cartItemList));
        }

        $(".close").trigger("click");
    }

    handletileFilterData(data, type) {
        this.setState({
            tileFilterProductData: data,
            tileFilterProducttype: type,
        });

        this.tileProductFilter.filterProductByTile(type, data);
    }


    quantity_plus() {
        var current_value = parseInt($('#item_count').val());
        current_value = current_value + 1;
        $('#item_count').val(current_value);
    }

    quantity_minus() {
        var current_value = parseInt($('#item_count').val());
        if (current_value > 1) {
            current_value = current_value - 1;
            $('#item_count').val(current_value);
        }
    }

    setDefaultQuantity(qty) {
        this.setState({
            variationDefaultQunatity: qty,
        });
    }

    handleDiscount(amt, type) {
        console.log("callbackAmount1 ", parseFloat(amt));
        console.log("callbackAmountType1 ", type);
        if (amt) {

            this.setState({ discountAmount: parseFloat(amt), discountType: type });
            //  this.state.discountAmount= amt;
            //  this.state.discountType=type; 
        }

    }


    perCur(curEle, displayId, currency_sign) {
        if ($('#' + displayId).attr('currency') == 'num') {
            $('#' + displayId).attr('currency', 'per');
            $('#' + displayId).text('%');
            $(curEle).html(currency_sign);
        } else {
            $('#' + displayId).attr('currency', 'num');
            $('#' + displayId).html(currency_sign);
            $(curEle).text('%');
        }
    }
    rmvInp1() {

    }
    // incdec(value){

    // }
    minplus() {
        console.log(jQuery('#spnCalcType').text());
        if (jQuery('#spnCalcType').text() == "%")
            jQuery('#spnCalcType').text("$");
        else
            jQuery('#spnCalcType').text("%");

    }

    checkout() {
        window.location.href = 'checkout'
    }

    render() {
        const { getVariationProductData } = this.state;
        const { hasVariationProductData } = this.state;
        const { discountlist, productlist, favouritesChildCategoryList } = this.props;

        // this.setState({
        //     allProductList : productlist,
        // });
        // console.log("getVariationProductData",this.state.getVariationProductData);
        return (

            <div className="hide-overflow">
                <div className="wrapper">
                    <div className="overlay"></div>
                    <NavbarPage {...this.props} />
                    <div id="content">
                        <CommonHeaderTwo {...this.props} searchProductFilter={this.handletileFilterData} />

                        <div className="inner_content bg-light-white clearfix">
                            <div className="content_wrapper">
                                <div className="col-lg-9 col-sm-8 col-xs-8 p-0">
                                    <FavouriteList productData={this.handleProductData} tileFilterData={this.handletileFilterData} />
                                    {/* <div className="col-lg-4 col-md-5 col-sm-5 p-0 plr-8">  */}
                                    {/* <ProductList/> */}
                                    {/* </div> */}
                                    <div className="col-lg-4 col-md-5 col-sm-5 p-0 plr-8">
                                        <AllProduct productData={this.handleProductData} onRef={ref => (this.tileProductFilter = ref)} />
                                    </div>
                                </div>
                                <CartListView onDiscountAmountChange={this.handleDiscount} onChange={this.addFee} discountAmount={this.state.discountAmount} discountType={this.state.discountType} />

                            </div>
                        </div>
                    </div>
                </div>


                {/* <!-------product list popup------------> */}

                <div id="variationProductPopup" tabIndex="-1" className="modal modal-wide fade full_height_modal">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">
                                    <img src="assets/img/delete-icon.png" />
                                </button>
                                {/* <h4 className="modal-title">{hasVariationProductData ? getVariationProductData.Title : ''}</h4> */}
                                <h4 className="modal-title">{hasVariationProductData ? this.state.variationTitle : ''}</h4>
                            </div>
                            <div className="modal-body overflowscroll ButtonRadius" id="scroll_mdl_body">
                                <div className="row">
                                    <div className="col-md-4 text-center">
                                        <img src={hasVariationProductData ? this.state.variationImage : ''} id="prdImg" style={{ width: '100%', height: 200, borderRadius: 8, marginLeft: 20 }} />
                                    </div>
                                    <div className="col-md-8">
                                        <div className="col-md-2">
                                            <div className="modal_wide_field">
                                                <input type="text" readOnly="true" value={hasVariationProductData ? this.state.variationDefaultQunatity : ''} className="form-control h_eight70" />
                                            </div>
                                        </div>
                                        <div className="col-md-4">
                                            <div className="btn-group btn-group-justified" role="group" aria-label="...">
                                                <div className="btn-group" role="group">
                                                    <a onClick={this.incrementDefaultQuantity} className="btn btn-default bd-r h_eight70 btn-plus">
                                                        <button className="icon icon-plus button"></button>
                                                    </a>
                                                </div>
                                                <div className="btn-group" role="group">
                                                    <a onClick={this.decrementDefaultQuantity} className="btn btn-default h_eight70 btn-plus bl-0">
                                                        <button className="icon icon-minus button"></button>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-6">
                                            <div className="input-group">
                                                <div className="input-group-btn w-50 btn-left">
                                                    <button type="button" className="btn btn-default br-0 h_eight70">In Stock</button>
                                                </div>
                                                <div className="modal_wide_field">
                                                    <input type="text" className="form-control h_eight70 borderRightBottomRadius" readOnly={true} value={hasVariationProductData ? this.state.variationStockQunatity : 1} />
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-md-12 mt-4">
                                            <div className="input-group">
                                                <div className="input-group-btn w-50 btn-left">
                                                    <button type="button" className="btn btn-default bd-r h_eight70">Price</button>
                                                </div>
                                                <div className="modal_wide_field">
                                                    <input type="text" readOnly={true} className="form-control bl-0 h_eight70 borderRightBottomRadius" value={hasVariationProductData ? this.state.variationPrice : 0} />
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <ProductAttribute attribute={hasVariationProductData ? getVariationProductData.ProductAttributes : null} optionClick={this.optionClick} />


                                </div>
                            </div>

                            <div className="modal-footer p-0">
                                <button type="button" className="btn btn-block btn-primary checkout-items buttonProductVariation" id="add_variation_product_btn" style={{ cursor: "no-drop", pointerEvents: "none" }} data-variation-id={hasVariationProductData ? this.state.variationId : 0} onClick={this.addVariationProductToCart.bind(this)}>Add Product</button>
                            </div>
                        </div>
                    </div>
                </div>
                < TileModel />
                <ProductOutOfStockPopupModel />
                {/* <div id="outOfStockModal" tabIndex="-1" className="modal modal-wide fade ">
        <div className="modal-dialog">
             <div className="modal-content">
             <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
            <img src="http://netmagnetic.co.in/wp2/wp-content/plugins/pos/resources/img/delete-icon.png" />
            </button>
            <h4 class="error_model_title modal-title" id="epos_error_model_title">Message</h4>
        </div>
        <div class="modal-body p-0">
            <h3 id="epos_error_model_message" class="popup_payment_error_msg">Beanie is out of stock.</h3>
        </div>
        <div class="modal-footer p-0">
            <button type="button" class="btn btn-primary btn-block h66" data-dismiss="modal" aria-hidden="true">OK</button>
        </div>
             </div>
         </div>
     </div> */}

                {/* <div id="tallModal" tabIndex="-1" className="modal modal-wide fade full_height_one">
        <div className="modal-dialog">
            <div className="modal-content">
                <div className="modal-header">
                    <button type="button" className="close" data-dismiss="modal" aria-hidden="true">
                        <img src="assets/img/delete-icon.png"/>
                    </button>
                    <h4 className="modal-title">Add To Tile</h4>
                </div>
                <div className="modal-body pl-0 pr-0 pt-0" id="scroll_mdl_body">
                    <div className="all_product">
                        <div className="clearfix">
                            <div className="searchDiv relDiv">
                                <input className="form-control nameSearch search_customer_input backbutton bb-0 noshadow pl-5" placeholder="Search Product" type="search"/>
                                <svg className="search_customer_input2" style={{right:24}} width="23" version="1.1" xmlns="http://www.w3.org/2000/svg" height="64" viewBox="0 0 64 64" xmlns="http://www.w3.org/1999/xlink" enableBackground="new 0 0 64 64">
                                    <g>
                                        <path fill="#C5BFBF" d="M28.941,31.786L0.613,60.114c-0.787,0.787-0.787,2.062,0,2.849c0.393,0.394,0.909,0.59,1.424,0.59   c0.516,0,1.031-0.196,1.424-0.59l28.541-28.541l28.541,28.541c0.394,0.394,0.909,0.59,1.424,0.59c0.515,0,1.031-0.196,1.424-0.59   c0.787-0.787,0.787-2.062,0-2.849L35.064,31.786L63.41,3.438c0.787-0.787,0.787-2.062,0-2.849c-0.787-0.786-2.062-0.786-2.848,0   L32.003,29.15L3.441,0.59c-0.787-0.786-2.061-0.786-2.848,0c-0.787,0.787-0.787,2.062,0,2.849L28.941,31.786z"></path>
                                    </g>
                                </svg>
                            </div>
                        </div>
                        <div className="overflowscroll" id="scroll_mdl_body">
                            <table className="table table-hover ShopProductTable">
                            <tbody>
                                <tr data-toggle="modal" href="#allProduct">
                                    <th className="pl-3">
                                        All Product
                                    </th>
                                    <td align="right">
                                        <img src="assets/img/next.png" className="pushnext" id="all_product"/>
                                    </td>
                                </tr>
                                <tr data-toggle="modal" href="#allAttribute">
                                    <th className="pl-3">
                                        Attributes
                                    </th>
                                    <td align="right">
                                        <img src="assets/img/next.png" className="pushnext" id="Attributes"/>
                                    </td>
                                </tr>
                                <tr data-toggle="modal" href="#allCategories">
                                    <th className="pl-3">
                                        Categories
                                    </th>
                                    <td align="right">
                                        <img src="assets/img/next.png" className="pushnext" id="Categories"/>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div className="modal-footer p-0">
                    <button type="button" className="btn btn-primary btn-block h66">SAVE & UPDATE</button>
                </div>
            </div>
        </div>
    </div> */}
                {/* modal close */}
                {/* <div id="allProduct" tabIndex="-1" className="modal modal-wide fade full_height_modal">
        <div className="modal-dialog">
            <div className="modal-content">
                <div className="modal-header">
                    <button type="button" className="close" data-dismiss="modal" aria-hidden="true">
                        <img src="assets/img/delete-icon.png"/>
                    </button>
                    <h4 className="modal-title">Add To Tile</h4>
                </div>
                <div className="modal-body pl-0 pr-0 pt-0">
                    <div className="all_product">
                        <div className="clearfix">
                            <div className="searchDiv relDiv">
                                <input className="form-control nameSearch bb-0 backbutton" value="Back" type="search" readOnly={true} data-toggle="modal" href="#tallModal"/>
                            </div>
                        </div>
                        <div className="overflowscroll" id="scroll_mdl_body">
                            <table className="table table ShopProductTable">
                            <tbody>
                                <tr>
                                    <td>
                                        Sample product
                                    </td>
                                    <td className="productPrice">
                                        255
                                    </td>
                                    <td align="right" className="clearfix">
                                        <div className="custom_radio custom_radio_set">
                                            <input name="setFavorite" id="addtotile1" value="4" type="radio"/>
                                            <label htmlFor="addtotile1">&nbsp;</label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Premium Quality
                                    </td>
                                    <td className="pt-2 pb-2 text-right">
                                        255
                                    </td>
                                    <td align="right" className="clearfix">
                                        <div className="custom_radio custom_radio_set">
                                            <input name="setFavorite" id="addtotile2" value="4" type="radio"/>
                                            <label htmlFor="addtotile2">&nbsp;</label>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Woo Logo
                                    </td>
                                    <td className="pt-2 pb-2 text-right">
                                        255
                                    </td>
                                    <td align="right" className="clearfix">
                                        <div className="custom_radio custom_radio_set float-right">
                                            <input name="setFavorite" id="addtotile3" value="4" type="radio"/>
                                            <label htmlFor="addtotile3">&nbsp;</label>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div className="modal-footer p-0">
                    <button type="button" className="btn btn-primary btn-block h66">SAVE & UPDATE</button>
                </div>
            </div>
        </div>
    </div> */}
                {/* modal close */}
                {/* <div id="allAttribute" tabIndex="-1" className="modal modal-wide fade full_height_modal">
        <div className="modal-dialog">
            <div className="modal-content">
                <div className="modal-header">
                    <button type="button" className="close" data-dismiss="modal" aria-hidden="true">
                        <img src="assets/img/delete-icon.png"/>
                    </button>
                    <h4 className="modal-title">All Attribute</h4>
                </div>
                <div className="modal-body pl-0 pr-0 pt-0">
                    <div className="all_product">
                        <div className="clearfix">
                            <div className="searchDiv relDiv">
                                <input className="form-control nameSearch bb-0 backbutton" value="Back" type="search" readOnly={true} data-toggle="modal" href="#tallModal"/>
                            </div>
                        </div>
                        <div className="overflowscroll" id="scroll_mdl_body">
                            <table className="table table-hover ShopProductTable">
                            <tbody>
                                <tr>
                                    <td className="selectProductCategory pr-2">
                                        <div className="custom_radio custom_radio_set">
                                            <input name="setFavorite" id="allAttribute1" value="4" type="radio"/>
                                            <label htmlFor="allAttribute1">&nbsp;</label>
                                        </div>
                                    </td>
                                    <td className="pl-3">
                                        cell
                                    </td>
                                    <td align="right">
                                        <img src="assets/img/next.png" className="pushnext" id="all_product"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td className="selectProductCategory pr-2">
                                        <div className="custom_radio custom_radio_set">
                                            <input name="setFavorite" id="allAttribute2" value="4" type="radio"/>
                                            <label htmlFor="allAttribute2">&nbsp;</label>
                                        </div>
                                    </td>
                                    <td className="pl-3">
                                        cell
                                    </td>
                                    <td align="right">
                                        <img src="assets/img/next.png" className="pushnext" id="all_product"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td className="selectProductCategory pr-2">
                                        <div className="custom_radio custom_radio_set">
                                            <input name="setFavorite" id="allAttribute3" value="4" type="radio"/>
                                            <label htmlFor="allAttribute3">&nbsp;</label>
                                        </div>
                                    </td>
                                    <td className="pl-3">
                                        cell
                                    </td>
                                    <td align="right">
                                        <img src="assets/img/next.png" className="pushnext" id="all_product"/>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div className="modal-footer p-0">
                    <button type="button" className="btn btn-primary btn-block h66">SAVE & UPDATE</button>
                </div>
            </div>
        </div>
    </div> */}
                {/* modal close */}
                {/* <div id="allCategories" tabIndex="-1" className="modal modal-wide fade full_height_modal">
        <div className="modal-dialog">
            <div className="modal-content">
                <div className="modal-header">
                    <button type="button" className="close" data-dismiss="modal" aria-hidden="true">
                        <img src="assets/img/delete-icon.png"/>
                    </button>
                    <h4 className="modal-title">All Categories</h4>
                </div>
             <div className="modal-body pl-0 pr-0 pt-0" id="scroll_mdl_body">
                    <div className="all_product">
                        <div className="clearfix">
                            <div className="searchDiv relDiv">
                                <input className="form-control nameSearch bb-0 backbutton" value="Back" type="search" readOnly={true} data-toggle="modal" href="#tallModal"/>
                            </div>
                        </div>
                        <div className="overflowscroll" id="scroll_mdl_body">
                        <table className="table table-hover ShopProductTable">
                            <tbody>
                                <tr>
                                    <td className="selectProductCategory pr-2">
                                        <div className="custom_radio custom_radio_set">
                                            <input name="setFavorite" id="allCategories1" value="4" type="radio"/>
                                            <label htmlFor="allCategories1">&nbsp;</label>
                                        </div>
                                    </td>
                                    <td className="pl-3">
                                        cell
                                    </td>
                                    <td align="right">
                                        <img src="assets/img/next.png" className="pushnext" id="all_product"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td className="selectProductCategory pr-2">
                                        <div className="custom_radio custom_radio_set">
                                            <input name="setFavorite" id="allCategories2" value="4" type="radio"/>
                                            <label htmlFor="allCategories2">&nbsp;</label>
                                        </div>
                                    </td>
                                    <td className="pl-3">
                                        cell
                                    </td>
                                    <td align="right">
                                        <img src="assets/img/next.png" className="pushnext" id="all_product"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td className="selectProductCategory pr-2">
                                        <div className="custom_radio custom_radio_set">
                                            <input name="setFavorite" id="allCategories3" value="4" type="radio"/>
                                            <label htmlFor="allCategories3">&nbsp;</label>
                                        </div>
                                    </td>
                                    <td className="pl-3">
                                        cell
                                    </td>
                                    <td align="right">
                                        <img src="assets/img/next.png" className="pushnext" id="all_product"/>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div className="modal-footer p-0"> 
                    <button type="button" className="btn btn-primary btn-block h66">SAVE & UPDATE</button>
                </div>
            </div>
        </div>
    </div> */}
                {/* modal close */}
                <div id="addProduct" tabIndex="-1" className="modal modal-wide fade full_height_modal modalProductAdd">
                    <div className="modal-dialog">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">
                                    <img src="assets/img/delete-icon.png" />
                                </button>
                                <h4 className="modal-title">Head Phone 6</h4>
                            </div>
                            <div className="modal-body overflowscroll ButtonRadius" id="scroll_mdl_body">
                                <div className="row">
                                    <div className="col-md-4 text-center">
                                        <div className="showViewImage">
                                            <img src="assets/img/sheos.png" />
                                        </div>
                                    </div>
                                    <div className="col-md-8">
                                        <div className="row">
                                            <div className="col-md-2 pl-0">
                                                <div className="modal_wide_field">
                                                    <input type="text" value="1" className="form-control h_eight70" />
                                                </div>
                                            </div>
                                            <div className="col-md-4 p-0">
                                                <div className="btn-group btn-group-justified" role="group" aria-label="...">
                                                    <div className="btn-group" role="group">
                                                        <a href="javascript:void(0);" onClick={() => this.quantity_plus()} className="btn btn-default bd-r h_eight70">
                                                            {/* <a onClick={this.incdec('inc')} className="btn btn-default bd-r h_eight70 btn-plus"> */}
                                                            <button className="icon icon-plus button"></button>
                                                        </a>
                                                    </div>
                                                    <div className="btn-group" role="group">
                                                        <a href="javascript:void(0);" onClick={() => this.quantity_minus()} className="btn btn-default h_eight70">
                                                            {/* <a onClick={this.incdec('dec')} className="btn btn-default h_eight70 btn-plus bl-0"> */}
                                                            <button className="icon icon-minus button"></button>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div className="col-md-6">
                                                <div className="input-group">
                                                    <div className="input-group-btn w-50 btn-left">
                                                        <button type="button" className="btn btn-default br-0 h_eight70">In Stock</button>
                                                    </div>
                                                    <div className="modal_wide_field">
                                                        <input type="text" className="form-control h_eight70 borderRightBottomRadius" readOnly={true} value="12" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-md-12 pl-0 mt-3">
                                                <div className="input-group">
                                                    <div className="input-group-btn w-50 btn-left">
                                                        <button type="button" className="btn btn-default bd-r h_eight70">Price</button>
                                                    </div>
                                                    <div className="modal_wide_field">
                                                        <input type="text" readOnly={true} className="form-control bl-0 h_eight70 borderRightBottomRadius" value="1236,00 $" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="clearfix"></div>
                                </div>

                                <div className="line-2"></div>
                                <div className="row">

                                    <div className="col-md-12 text-center">
                                        <label className="color mb-2">Colour</label>
                                        <div className="row">
                                            <div className="col-sm-3">
                                                <div className="button_with_checkbox">
                                                    <input type="radio" id="a" name="radio-group" />
                                                    <label htmlFor="a" className="label_select_button">Blue</label>
                                                </div>
                                            </div>
                                            <div className="col-sm-3">
                                                <div className="button_with_checkbox">
                                                    <input type="radio" id="b" name="radio-group" />
                                                    <label htmlFor="b" className="label_select_button">Black</label>
                                                </div>
                                            </div>
                                            <div className="col-sm-3">
                                                <div className="button_with_checkbox">
                                                    <input type="radio" id="c" name="radio-group" />
                                                    <label htmlFor="c" className="label_select_button">Green</label>
                                                </div>
                                            </div>
                                            <div className="col-sm-3">
                                                <div className="button_with_checkbox">
                                                    <input type="radio" id="d" name="radio-group" />
                                                    <label htmlFor="d" className="label_select_button">Red</label>
                                                </div>
                                            </div>
                                            <div className="clearfix"></div>
                                        </div>
                                    </div>
                                    <div className="clearfix"></div>
                                </div>

                                <div className="line-2"></div>
                                <div className="row">
                                    <div className="col-md-12 text-center">
                                        <label className="color mb-2">Size</label>
                                        <div className="row">
                                            <div className="col-sm-3">
                                                <div className="button_with_checkbox">
                                                    <input type="radio" id="test4" name="radio-group" />
                                                    <label htmlFor="test4" className="label_select_button">12</label>
                                                </div>
                                            </div>
                                            <div className="col-sm-3">
                                                <div className="button_with_checkbox">
                                                    <input type="radio" id="test5" name="radio-group" />
                                                    <label htmlFor="test5" className="label_select_button">24</label>
                                                </div>
                                            </div>
                                            <div className="col-sm-3">
                                                <div className="button_with_checkbox">
                                                    <input type="radio" id="test6" name="radio-group" />
                                                    <label htmlFor="test6" className="label_select_button">36</label>
                                                </div>
                                            </div>
                                            <div className="col-sm-3">
                                                <div className="button_with_checkbox">
                                                    <input type="radio" id="test7" name="radio-group" />
                                                    <label htmlFor="test7" className="label_select_button">48</label>
                                                </div>
                                            </div>
                                            <div className="col-sm-3">
                                                <div className="button_with_checkbox">
                                                    <input type="radio" id="test8" name="radio-group" />
                                                    <label htmlFor="test8" className="label_select_button">60</label>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="clearfix"></div>
                                    </div>
                                </div>
                            </div>

                            <div className="modal-footer p-0">
                                <button type="button" className="btn btn-primary btn-block h66">Add Product</button>
                            </div>
                        </div>
                    </div>
                </div>
                {/* modal close */}
                <DiscountPopup onDiscountAmountChange={this.handleDiscount} taxratelist={this.props.taxratelist} />

                <div id="popup_CustomFee" tabIndex="-1" className="modal modal-wide modal-wide1 fade">
                    <div className="modal-dialog" id="dialog-midle-align">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">
                                    <img src="assets/img/delete-icon.png" />
                                </button>
                                <h4 className="modal-title">Add Discount (Mittens)</h4>
                            </div>
                            <div className="modal-body p-0">
                                <form className="clearfix">
                                    {/* <div className="col-sm-5">
                            <div className="fixedinCalHeight overflowscroll">
                            <div>
                           {
                              
                                this.props.discountlist ?
                                
                                 this.props.discountlist.map((item,index)=>{
                                   // let isSimpleProduct = (item.Type != "simple") ? true : false ;  
                                     return(                                                    
                                        <div key={index} className="button_with_checkbox pt-4">
                                        <input type="radio" id="oliver_discount" name="radio-group"/>
                                        <label htmlFor="oliver_discount" className="label_select_button">{(item.Name.length>15 ? item.Name.substring(0,15)+'...':item.Name )+( item.Type=="Percentage"? item.Amount+"%": item.Type=="Number"?item.Amount:"") }</label>
                                         </div>
                                    )
                                  })
                                  :<div></div>             
                            
                            }</div>
                              

                            </div>
                        </div> */}
                                    <div className="col-sm-7 p-0">
                                        <div className="panel-product-list" id="panelCalculatorpopUp">
                                            <div className="panel panelCalculator">
                                                <div className="panel-body p-0">
                                                    <table className="table table-bordered shopViewPopUpCalculator">
                                                        <tbody>
                                                            <tr>
                                                                <td colSpan="2" className="text-right br-1 bl-1 bt-0">
                                                                    <div className="input-group discount-input-group">
                                                                        <input type="text" id="txtdisAmount" className="form-control text-right" placeholder="1234" value={this.state.discountAmount} text={this.state.discountAmount} aria-describedby="basic-addon1" />
                                                                        <span className="input-group-addon" id="spnCalcType" name="spnCalcType">$</span>
                                                                    </div>
                                                                </td>
                                                                <td className="text-center pointer bt-0" onClick={() => this.calcInp('c')}>

                                                                    <button type="button" className="btn btn-default calculate">
                                                                        <img width="36" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0iaXNvLTg4NTktMSI/Pgo8IS0tIEdlbmVyYXRvcjogQWRvYmUgSWxsdXN0cmF0b3IgMTkuMS4wLCBTVkcgRXhwb3J0IFBsdWctSW4gLiBTVkcgVmVyc2lvbjogNi4wMCBCdWlsZCAwKSAgLS0+CjxzdmcgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgdmVyc2lvbj0iMS4xIiBpZD0iQ2FwYV8xIiB4PSIwcHgiIHk9IjBweCIgdmlld0JveD0iMCAwIDMxLjA1OSAzMS4wNTkiIHN0eWxlPSJlbmFibGUtYmFja2dyb3VuZDpuZXcgMCAwIDMxLjA1OSAzMS4wNTk7IiB4bWw6c3BhY2U9InByZXNlcnZlIiB3aWR0aD0iNTEycHgiIGhlaWdodD0iNTEycHgiPgo8Zz4KCTxnPgoJCTxwYXRoIGQ9Ik0zMC4xNzEsMTYuNDE2SDAuODg4QzAuMzk4LDE2LjQxNiwwLDE2LjAyLDAsMTUuNTI5YzAtMC40OSwwLjM5OC0wLjg4OCwwLjg4OC0wLjg4OGgyOS4yODMgICAgYzAuNDksMCwwLjg4OCwwLjM5OCwwLjg4OCwwLjg4OEMzMS4wNTksMTYuMDIsMzAuNjYxLDE2LjQxNiwzMC4xNzEsMTYuNDE2eiIgZmlsbD0iIzRiNGI0YiIvPgoJPC9nPgoJPGc+CgkJPHBhdGggZD0iTTE2LjAxNywzMS4wNTljLTAuMjIyLDAtMC40NDUtMC4wODMtMC42MTctMC4yNUwwLjI3MSwxNi4xNjZDMC4wOTgsMTUuOTk5LDAsMTUuNzcsMCwxNS41MjkgICAgYzAtMC4yNCwwLjA5OC0wLjQ3MSwwLjI3MS0wLjYzOEwxNS40LDAuMjVjMC4zNTItMC4zNDEsMC45MTQtMC4zMzIsMS4yNTUsMC4wMmMwLjM0LDAuMzUzLDAuMzMxLDAuOTE1LTAuMDIxLDEuMjU1TDIuMTYzLDE1LjUyOSAgICBsMTQuNDcxLDE0LjAwNGMwLjM1MiwwLjM0MSwwLjM2MSwwLjkwMiwwLjAyMSwxLjI1NUMxNi40OCwzMC45NjgsMTYuMjQ5LDMxLjA1OSwxNi4wMTcsMzEuMDU5eiIgZmlsbD0iIzRiNGI0YiIvPgoJPC9nPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+CjxnPgo8L2c+Cjwvc3ZnPgo="></img>
                                                                    </button>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td className="td-calc-padding br-1 bl-1">
                                                                    <button type="button" onClick={() => this.calcInp(1)} className="btn btn-default calculate">1</button>
                                                                </td>
                                                                <td className="td-calc-padding br-1">
                                                                    <button type="button" onClick={() => this.calcInp(2)} className="btn btn-default calculate">2</button>
                                                                </td>
                                                                <td className="td-calc-padding">
                                                                    <button type="button" onClick={() => this.calcInp(3)} className="btn btn-default calculate">3</button>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td className="td-calc-padding br-1 bl-1">
                                                                    <button type="button" onClick={() => this.calcInp(4)} className="btn btn-default calculate">4</button>
                                                                </td>
                                                                <td className="td-calc-padding br-1">
                                                                    <button type="button" onClick={() => this.calcInp(5)} className="btn btn-default calculate">5</button>
                                                                </td>
                                                                <td className="td-calc-padding">
                                                                    <button type="button" onClick={() => this.calcInp(6)} className="btn btn-default calculate">6</button>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td className="td-calc-padding br-1 bl-1">
                                                                    <button type="button" onClick={() => this.calcInp(7)} className="btn btn-default calculate">7</button>
                                                                </td>
                                                                <td className="td-calc-padding br-1">
                                                                    <button type="button" onClick={() => this.calcInp(8)} className="btn btn-default calculate">8</button>
                                                                </td>
                                                                <td className="td-calc-padding">
                                                                    <button type="button" onClick={() => this.calcInp(9)} className="btn btn-default calculate">9</button>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td className="td-calc-padding br-1 bl-1" >
                                                                    <button type="button" onClick={() => this.minplus()} className="btn btn-default calculate"> %</button>

                                                                </td>
                                                                <td className="td-calc-padding br-1">
                                                                    <button type="button" onClick={() => this.calcInp('.')} className="btn btn-default calculate">,</button>
                                                                </td>
                                                                <td className="td-calc-padding">
                                                                    <button type="button" onClick={() => this.calcInp(0)} className="btn btn-default calculate">0</button>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div className="modal-footer p-0">
                                <button type="button" className="btn btn-primary btn-block h66">ADD DISCOUNT</button>
                            </div>
                        </div>
                    </div>
                </div>
                {/* modal close */}
                <div tabIndex="-1" className="modal fade in mt-5 modal-wide" id="addnotehere" role="dialog">
                    <CustomerNote />
                </div>
                {/* modal close */}
                <div tabIndex="-1" className="modal fade modal-wide in sidebar-minus window-height" id="registeropenclose1" role="dialog">
                    <div className="modal-dialog modal-lg">
                        <div className="modal-content">
                            <div className="modal-header" >
                                <button type="button" className="close" data-dismiss="modal">×</button>
                                <h4 className="modal-title">Modal Header</h4>
                            </div>
                            <div className="modal-body p-0">
                                <div className="panel panel-custmer b-0">
                                    <div className="panel-default">
                                        <div className="panel-heading text-center customer_name font18 visible2">
                                            Close Register
                            </div>
                                        <div className="panel-body customer_history p-0">
                                            <div className="col-sm-12">
                                                <div className="row">
                                                    <div className="col-lg-9 col-sm-8 plr-8">
                                                        <form className="customer-detail">
                                                            <div className="form-group">
                                                                <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 pl-0 mb-4 plr-8">
                                                                    <label htmlFor="">Register:</label>
                                                                    <div className="col-sm-12 p-0">
                                                                        <p>Register 1</p>
                                                                    </div>
                                                                </div>
                                                                <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 pl-0 mb-4 plr-8">
                                                                    <label htmlFor="">Date &amp; Time:</label>
                                                                    <div className="col-sm-12 p-0">
                                                                        <p className="">15. January 12:57PM</p>
                                                                    </div>
                                                                </div>
                                                                <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 pl-0 mb-4 plr-8">
                                                                    <label htmlFor="">User:</label>
                                                                    <div className="col-sm-12 p-0">
                                                                        <p>Mike Kellerman</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <div className="col-lg-3 col-sm-4 bl-1 plr-8">
                                                        <form action="#" className="w-100 pl-3">
                                                            <div className="w-100-block button_with_checkbox">
                                                                <input type="radio" id="p-report" name="radio-group" />
                                                                <label htmlFor="p-report" className="label_select_button">Print Report</label>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="panel-footer bt-0 p-0">
                                            <div className="table_head">
                                                <table className="table table-border table-hover borderless c_change mb-0 tbl-bb-1 tbl-layout-fixe">
                                                    {/* <colgroup>
                                            <col width="*">
                                                <col width="350">
                                                    <col width="350">
                                                        <col width="350">
                                        </colgroup> */}
                                                    <tbody>
                                                        <tr>
                                                            <th className="bt-0 pb-0 bb-1">Item</th>
                                                            <th className="bt-0 pb-0 text-right bb-1">Expected</th>
                                                            <th className="bt-0 pb-0 text-right bb-1">Actual</th>
                                                            <th className="bt-0 pb-0 text-right bb-1">Difference</th>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div className="">
                                                <table className="table table-borderless tbl-bb-1 font tbl-layout-fixe pt-3 pb-3">
                                                    {/* <colgroup>
                                            <col width="*">
                                                <col width="350">
                                                    <col width="350">
                                                        <col width="350">
                                        </colgroup> */}
                                                    <tbody>
                                                        <tr>
                                                            <td>Cash In Tiller</td>
                                                            <td align="right">950.00 </td>
                                                            <td align="right" className="text-blue">1050.00</td>
                                                            <td align="right">1050.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Debit/Credit Card</td>
                                                            <td align="right">950.00 </td>
                                                            <td align="right" className="text-blue">1050.00</td>
                                                            <td align="right">1050.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Gift Card</td>
                                                            <td align="right">950.00 </td>
                                                            <td align="right" className="text-blue">1050.00</td>
                                                            <td align="right">1050.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Others</td>
                                                            <td align="right">950.00 </td>
                                                            <td align="right" className="text-blue">1050.00</td>
                                                            <td align="right">1050.00</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div className="col-lg-offset-7 col-lg-5 col-sm-offset-6 col-sm-6 col-xs-offset-4 col-xs-8 p-0">
                                                    <table className="table table-borderless tbl-bb-1 mb-0 font pt-1 pb-1">
                                                        <tbody>
                                                            <tr>
                                                                <td className="pl-0">Total Difference</td>
                                                                <td className="text-right">10.00</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div className="col-sm-12">
                                                    <div className="register-closenote">
                                                        <textarea cols="12" rows="4" className="form-control" placeholder="Enter notes here"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer p-0">
                                <button type="button" className="btn btn-primary btn-block h66">SAVE &amp; UPDATE</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="checkout1" className="modal modal-wide modal-wide1 fade">
                    <div className="modal-dialog" id="dialog-midle-align">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">
                                    <img src="http://netmagnetic.co.in/wp2/wp-content/plugins/pos/resources/img/delete-icon.png" />
                                </button>
                                <h4 className="error_model_title modal-title" id="epos_error_model_title">Message</h4>
                            </div>
                            <div className="modal-body p-0">
                                <h3 id="epos_error_model_message" className="popup_payment_error_msg">Please add at least one product in cart !</h3>
                            </div>
                            <div className="modal-footer p-0">
                                <button type="button" className="btn btn-primary btn-block h66" data-dismiss="modal" aria-hidden="true">OK</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="no_discount" className="modal modal-wide modal-wide1 fade">
                    <div className="modal-dialog" id="dialog-midle-align">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">
                                    <img src="http://netmagnetic.co.in/wp2/wp-content/plugins/pos/resources/img/delete-icon.png" />
                                </button>
                                <h4 className="error_model_title modal-title" id="epos_error_model_title">Message</h4>
                            </div>
                            <div className="modal-body p-0">
                                <h3 id="epos_error_model_message" className="popup_payment_error_msg">Not use 100% discount !</h3>
                            </div>
                            <div className="modal-footer p-0">
                                <button type="button" className="btn btn-primary btn-block h66" data-dismiss="modal" aria-hidden="true">OK</button>
                            </div>
                        </div>
                    </div>
                </div>

                <div tabIndex="-1" id="popup_oliver_add_fee" tabIndex="-1" className="modal modal-wide modal-wide1 fade">
                    <CustomerAddFee />
                </div>
            </div>

        );
    }
}

function mapStateToProps(state) {
    //console.log("shopview state",variationProductList);
    const { discountlist, productlist, favouritesChildCategoryList, taxratelist,alert } = state;
    return {
        discountlist,
        productlist,
        taxratelist: taxratelist.taxratelist,
        alert
    };
}

const connectedShopView = connect(mapStateToProps)(ShopView);
export { connectedShopView as ShopView };

