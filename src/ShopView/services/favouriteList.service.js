import Config from '../../Config'
import { authHeader } from '../../_helpers'

export const favouriteListService = {  
    getAll,
    getChildCategory,
    getSubAttributesList ,
    addToFavourite ,
    favProductRemove
};

const API_URL=Config.key.OP_API_URL
function getAll(UDID, Register_id) {    
    const requestOptions = {
        method: 'GET',           
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
    };

    return fetch(`${API_URL}/ShopData/GetDisplayFavList?udid=`+UDID+'&Registerid='+Register_id, requestOptions)
        .then(handleResponse)
        .then(favouriteListRes => {
            var favouriteList = favouriteListRes.Content;        
            return favouriteList;
    });
}
function getChildCategory(UDID, catId) {

    const requestOptions = {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
    };
    
    return fetch(`${API_URL}/ShopSetting/GetChildCategories?udid=${UDID}&CateId=${catId}`, requestOptions)
        .then(handleResponse)
        .then(favouriteChildCatlist => {
           // console.log("favouriteChildCatlist", favouriteChildCatlist);
            var favouriteChildCatlist = favouriteChildCatlist.Content;
            return favouriteChildCatlist;
        });
}

 function getSubAttributesList(UDID,slug){

    const requestOptions = {
        method: 'GET',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
        }
    };
    
    return fetch(`${API_URL}/ShopSetting/GetSubAttributes?udid=${UDID}&slug=${slug}`, requestOptions)
        .then(handleResponse)
        .then(subAttributelist => {
           // console.log("favouriteChildCatlist", favouriteChildCatlist);
            var subAttributelist = subAttributelist.Content;
            return subAttributelist;
        });


 }

 function addToFavourite(ItemType,ItemId,ItemSlug)
  {
    //UserID,RegisterId,udid,
    console.log("addtofavourite Jason", JSON.stringify({ ItemId,ItemType,ItemSlug }))
    console.log( "user", localStorage.getItem('user'));
    console.log("UDID",localStorage.getItem("UDID"));
    console.log("register",localStorage.getItem('register'));

var udid= localStorage.getItem("UDID");
var User=JSON.parse( localStorage.getItem('user'));
var UserID=User.user_id;
var RegisterId=localStorage.getItem('register');
console.log("Request",udid,UserID,RegisterId)
    const requestOptions = {
        method: 'POST',           
      //  mode: 'no-cors',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
     
        body: JSON.stringify({ UserID,RegisterId,udid,ItemId,ItemType,ItemSlug })
    };  
 
    return fetch(`${API_URL}/ShopData/SaveFavorite`, requestOptions)
        .then(handleResponse)
        .then(response => {
            console.log("addtofavourite Response",response);
           // addtofavourite successful if there's a jwt token in the response
            // if (user.Content[0].UDID) {
            //     // store user details and jwt token in local storage to keep user logged in between page refreshes
            //     //localStorage.setItem('user', JSON.stringify(user));
            //     const requestOptions1 = {
            //         method: 'GET',    
            //       headers: {
            //         'Accept': 'application/json',
            //         'Content-Type': 'application/json',
            //       } };
            //     return fetch(`http://app.oliverpos.com/api/ShopAccess/GetLocations?udid=`+user.Content[0].UDID,requestOptions1)
            //     .then(handleResponse)
            //     .then(location => {
            //         console.log("location Response",location);
            //         localStorage.setItem('user', JSON.stringify(user));
            //         localStorage.setItem('UserLocations', JSON.stringify(location.Content));

                     return response;

            //     }).catch(error=>{
            //           Message:"Error- No location found"  
            //     })
                
            // }
            
        });
}

        function favProductRemove(UDID,favid){
            const requestOptions = {
                method: 'GET',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                }
            };
        

            return fetch(`${API_URL}/ShopData/DeleteFavorite?Udid=${UDID}&favId=${favid}`, requestOptions)
                .then(handleResponse)
                .then(favProductdelete=> {
                    console.log("favProductdelete", favProductdelete);
                    var favProductdelete= favProductdelete;
                    return favProductdelete;
                });
        

            
        }
function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                logout();
                location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}


