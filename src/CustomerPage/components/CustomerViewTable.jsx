import React from 'react';
import { history } from '../../_helpers';
import { activityActions } from '../../ActivityPage/actions/activity.action';
import  moment  from 'moment';

function ActivityPage (props,id,uid) {
    console.log("ActivityPage" ,props, id,uid );
    props.dispatch(activityActions.getDetail(id,uid));
    history.push('/activity');
    
}
    
export const CustomerViewTable = (props) => {

        return (
                  
            <tr  onClick={()=>ActivityPage(props.Props ,props.ID,props.UDID)}>
                <td className="status_shorting">{moment(props.Date).format('DD, MMMM YYYY')}</td>
                <td className="status_shorting">{props.Status}</td>
                <td className="status_shorting">{props.Location}</td>
                <td className="amount_shorting">{props.Amount}</td>
                <td><img src="assets/img/next.png" className="width14" /></td>
            </tr>
                      
               
        )
    
}
