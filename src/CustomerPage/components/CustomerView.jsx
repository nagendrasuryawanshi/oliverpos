import React from 'react';
import { connect } from 'react-redux';
import { NavbarPage, CommonHeaderTwo, CommonHeaderThree } from '../../_components';
import { CustomerViewFirst } from './CustomerViewFirst';
import { CustomerViewSecond } from './CustomerViewSecond';
import { customerActions } from '../actions/customer.action';
import { CustomerViewEdit } from './CustomerViewEdit';
import { CustomerViewCreate } from './CustomerViewCreate';
import { LoadingModal } from '../../_components';
import { history } from '../../_helpers'

class CustomerView extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            cust_fname: '',
            cust_lname: '',
            submitted: false,
            cust_email: '',
            cust_phone_number: '',
            cust_street_address: '',
            cust_city: '',
            cust_postcode: '',
            cust_note: '',
            user_address: '',
            user_city: '',
            user_contact: '',
            user_email: '',
            user_fname: '',
            user_lname: '',
            user_pcode: '',
            user_note: '',
            active: history.location.state ? 0 : null,
            activeFilter: false,
            UDID: '',
            Cust_ID: '',
            user_store_credit: '',
            pageOfItems: [],
            search: '',
            backUrl: history.location.state,
            emailValid:false
        };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.filterCustomer = this.filterCustomer.bind(this);
        this.onChangePage = this.onChangePage.bind(this);
        console.log("history.goBack", history.location.state);

    }

    onChangePage(pageOfItems) {
        // update state with new page of items
        this.setState({ pageOfItems: pageOfItems });
    }

    handleChange(e) {
        let emailValid = this.state.emailValid;
        const { name, value } = e.target;
        console.log("text area", value,name)
        switch(name) {
            case 'cust_email':
              emailValid = value.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i);
              break;
              default:
              break;
          }
          console.log("emailValid",emailValid)
           if(emailValid && emailValid.length==4){
              //alert()
             this.setState({emailValid:''})
          }else if(emailValid == null ){
            this.setState({emailValid:'email is Invalid'})
          }

         this.setState({ [name]: value});
    }

  
   

    handleSubmit() {
        //  e.preventDefault();

        this.setState({ submitted: true });
        const { cust_city, cust_email, cust_fname, cust_lname,
            cust_note, cust_phone_number, cust_postcode, cust_street_address,
            UDID,
            user_address, user_city, user_contact, user_email, user_fname,
            user_lname, user_pcode, user_note, Cust_ID, user_store_credit,emailValid
        } = this.state;
        // console.log("data", cust_city)
        const { dispatch } = this.props;
        if (cust_city && cust_email && cust_fname && cust_lname && cust_note &&
            cust_phone_number && cust_postcode && cust_street_address && emailValid == '') {
            const save = {
                Id: "",
                FirstName: cust_fname,
                LAstName: cust_lname,
                Contact: cust_phone_number,
                startAmount: 0,
                Email: cust_email,
                Udid: UDID,
                Notes: cust_note,
                StreetAddress: cust_street_address,
                Pincode: cust_postcode,
                City: cust_city
            }
            //console.log("data", save)
            dispatch(customerActions.save(save));
            $(".close").click();
            // window.location = '/customerview'

        }
        if (user_address && user_city && user_contact && user_email &&
            user_fname && user_lname && user_pcode) {
            //console.log("Cust_ID", Cust_ID)
            // console.log("Edit", user_city)
            const update = {
                Id: Cust_ID,
                FirstName: user_fname,
                LAstName: user_lname,
                Contact: user_contact,
                startAmount: 0,
                Email: user_email,
                Udid: UDID,
                Notes: user_note,
                StreetAddress: user_address,
                Pincode: user_pcode,
                City: user_city
            }
            //console.log("update", update)
            dispatch(customerActions.save(update));
            $(".close").click();
            // window.location = '/customerview'

        }

    }

    SetValue(eleT, val) {
        eleT.html(eleT.html().replace(' ', '') + val + ' ');
    }
    calcInp(input) {
        var elemJ = jQuery('#calc_output');

        if (elemJ.html() == "0 ") {
            alert();
            elemJ.html('');
        }
        switch (input) {
            case 1:
                this.SetValue(elemJ, '1');
                break;
            case 2:
                this.SetValue(elemJ, '2');
                break;
            case 3:
                this.SetValue(elemJ, '3');
                break;
            case 4:
                this.SetValue(elemJ, '4');
                break;
            case 5:
                this.SetValue(elemJ, '5');
                break;
            case 6:
                this.SetValue(elemJ, '6');
                break;
            case 7:
                this.SetValue(elemJ, '7');
                break;
            case 8:
                this.SetValue(elemJ, '8');
                break;
            case 9:
                this.SetValue(elemJ, '9');
                break;
            case 0:
                this.SetValue(elemJ, '0');
                break;
            case '.':
                if (elemJ.html() == "") {
                    this.SetValue(elemJ, '0.0');
                } else {
                    if (elemJ.html().includes(".")) {

                    } else {
                        this.SetValue(elemJ, '.');
                    }
                }
                break;
            //alert('n');
            // default code block
        }
    }

    rmvInp() {
        var str = jQuery('#calc_output').html();
        str = str.substring(0, str.length - 2);
        jQuery('#calc_output').html(str + ' ');
        if (str == "" || str == " ") {
            jQuery('#calc_output').html('0 ');
        } else { }
    }

    minplus() {

    }


    reload() {
        const UID = localStorage.getItem('UDID');
        //console.log("%cUID BY CUSTOMERS", "color:darkpink", UID)
        this.setState({ UDID: UID })
        const { Cust_ID } = this.state;
        this.props.dispatch(customerActions.getAll(UID));
        if (Cust_ID && UID) {
            this.props.dispatch(customerActions.getDetail(Cust_ID, UID));
        }
        
    }

    componentDidMount() {       
        this.reload();
        
        // if(this.state.backUrl){
        //     console.log("selectFirstCustomer");
        //     this.selectFirstCustomer()

        // }
    }

    activeClass(item, index) {
        //console.log("item", item)
        //console.log("index", index)
        const { UDID } = this.state;
        const { dispatch } = this.props;
        this.setState({
            active: index,
            //singleItem:item,
            user_address: item.StreetAddress ? item.StreetAddress : '',
            user_city: item.City ? item.City : '',
            user_contact: item.Contact ? item.Contact : '',
            user_email: item.Email ? item.Email : '',
            user_fname: item.FirstName ? item.FirstName : '',
            user_lname: item.LastName ? item.LastName : '',
            user_pcode: item.Pincode ? item.Pincode : '',
            user_note: item.notes ? item.notes : '',
            Cust_ID: item.Id ? item.Id : '',
            user_store_credit: item.store_credit ? item.store_credit : ''
        })
        console.log("this.state.active", this.state.active);
        dispatch(customerActions.getDetail(item.Id, UDID));
    }

    deleteCustomer() {
        //alert()
        const { UDID, Cust_ID } = this.state;
        const { dispatch } = this.props;
        dispatch(customerActions.Delete(Cust_ID, UDID));
        this.reload();

    }

    goBack(single_cutomer_list) {
        if (single_cutomer_list != null &&  this.state.backUrl  ) {
              console.log("this.state.backUrl hain ");
            history.push(this.state.backUrl);
            localStorage.setItem('AdCusDetail', JSON.stringify(single_cutomer_list))
            localStorage.getItem('AdCusDetail');
            console.log("CHECKLIST", JSON.parse(localStorage.getItem('CHECKLIST')));
            let list = localStorage.getItem('CHECKLIST')?JSON.parse(localStorage.getItem('CHECKLIST')):null;
            console.log("LIST", list);
            console.log("single_cutomer_list", single_cutomer_list);
            if(list!=null){
            const CheckoutList = {
                ListItem: list.ListItem,
                customerDetail: single_cutomer_list,
                totalPrice: list.totalPrice,
                discountCalculated: list.discountCalculated,
                tax: list.taxAmount,
                subTotal: list.subTotal,
                TaxId: list.tax
            }
            console.log("CheckoutList", CheckoutList);
            localStorage.setItem('CHECKLIST', JSON.stringify(CheckoutList))
         }
        }
        if(single_cutomer_list != null &&  !this.state.backUrl){
            console.log("this.state.backUrl nahi hain ")

            history.push('/shopview');
            localStorage.setItem('AdCusDetail', JSON.stringify(single_cutomer_list))
            localStorage.getItem('AdCusDetail');
            console.log("CHECKLIST", JSON.parse(localStorage.getItem('CHECKLIST')));
            let list = localStorage.getItem('CHECKLIST')?JSON.parse(localStorage.getItem('CHECKLIST')):null;
            console.log("LIST", list);
            console.log("single_cutomer_list", single_cutomer_list);
            if(list!=null){
            const CheckoutList = {
                ListItem: list.ListItem,
                customerDetail: single_cutomer_list,
                totalPrice: list.totalPrice,
                discountCalculated: list.discountCalculated,
                tax: list.taxAmount,
                subTotal: list.subTotal,
                TaxId: list.tax
            }
            console.log("CheckoutList", CheckoutList);
            localStorage.setItem('CHECKLIST', JSON.stringify(CheckoutList))
         }

        }

        //  console.log("history.goBack12",  localStorage.getItem('AdCusDetail')  );
        //  window.location.href = '/shopview'
    }

    filterCustomer(e) {
        let filtered = [];
        const { value } = e.target;
        console.log("search", value);
        this.setState({ search: value })
        const customerlist = this.props.customerlist && this.props.customerlist.Content;

        customerlist.map((item) => {
            if (String(item.FirstName).toUpperCase().toString().indexOf(String(value).toUpperCase()) != -1 || String(item.FirstName).toLowerCase().toString().indexOf(String(value).toLowerCase()) != -1
            || String(item.LastName).toUpperCase().toString().indexOf(String(value).toUpperCase()) != -1 || String(item.LastName).toLowerCase().toString().indexOf(String(value).toLowerCase()) != -1
            || String(item.Email).toUpperCase().toString().indexOf(String(value).toUpperCase()) != -1 || String(item.Email).toLowerCase().toString().indexOf(String(value).toLowerCase()) != -1
            || String(item.Contact) == String(value)
        )
        {
                // console.log('filteredList', item)
                filtered.push(item)
                this.setState({ activeFilter: true })
            }
        })
        // console.log("filteredList filnal", filtered);
        if (filtered.length == 0) {
            //  console.log("o length")
            this.setState({ activeFilter: false })
        }
        this.props.dispatch(customerActions.filteredList(filtered))
    }

    clearInput() {
        this.setState({
            search: '',
            activeFilter: false
        })

    }

    // selectFirstCustomer(){
    //     $("#customer-list tr:first").click();
        
    //     }


    render() {
        const { customerlist, single_cutomer_list, filteredList } = this.props;
        // console.log("%cSINGLE CUSTOMER LIST", "color:red")
        const {
            cust_city,
            cust_email,
            cust_fname,
            cust_lname,
            cust_note,
            cust_phone_number,
            cust_postcode,
            cust_street_address,
            submitted,
            user_address,
            user_city,
            user_contact,
            user_email,
            user_fname,
            user_lname,
            user_pcode,
            user_note,
            active,
            UDID,
            Cust_ID,
            activeFilter,
            pageOfItems,
            search,
            emailValid
        } = this.state;
        console.log("emailValid2",emailValid)
        
        return (

            <div>
                <div className="wrapper">
                    <div className="overlay"></div>
                    <NavbarPage {...this.props} />
                    <div id="content">
                        {!customerlist ? <LoadingModal /> : ''}

                        {this.state.backUrl ?
                            <CommonHeaderThree />
                            : <CommonHeaderTwo {...this.props} />
                        }
                        <div className="inner_content bg-light-white clearfix">
                            <div className="content_wrapper">
                                <div className="col-sm-3 col-xs-5 p-0">
                                    <div className="items">
                                        <div className="panel panel-default panel-product-list p-0 bb-0 bor-customer">
                                            <div className="searchDiv relDiv">
                                                <input type="text" onChange={this.filterCustomer} value={search} className="form-control nameSearch search_customer_input" placeholder="Search by Name, Email or Phone" id="searchUser" data-column-index="0" />
                                                <svg onClick={() => this.clearInput()} className="search_customer_input2" width="23" version="1.1" xmlns="http://www.w3.org/2000/svg" height="64" viewBox="0 0 64 64" xmlns="http://www.w3.org/1999/xlink" enableBackground="new 0 0 64 64">
                                                    <g>
                                                        <path fill="#C5BFBF" d="M28.941,31.786L0.613,60.114c-0.787,0.787-0.787,2.062,0,2.849c0.393,0.394,0.909,0.59,1.424,0.59   c0.516,0,1.031-0.196,1.424-0.59l28.541-28.541l28.541,28.541c0.394,0.394,0.909,0.59,1.424,0.59c0.515,0,1.031-0.196,1.424-0.59   c0.787-0.787,0.787-2.062,0-2.849L35.064,31.786L63.41,3.438c0.787-0.787,0.787-2.062,0-2.849c-0.787-0.786-2.062-0.786-2.848,0   L32.003,29.15L3.441,0.59c-0.787-0.786-2.061-0.786-2.848,0c-0.787,0.787-0.787,2.062,0,2.849L28.941,31.786z"></path>
                                                    </g>
                                                </svg>
                                            </div>
                                            <div className="overflowscroll" id="header-search-button">
                                                <table className="table customerViewTable table-hover" id="customer-list">
                                                    <colgroup>
                                                        <col width="*" />
                                                        <col width="35" />
                                                    </colgroup>
                                                    <tbody>
                                                        {activeFilter == true &&

                                                            filteredList && filteredList.map((item, index) => {
                                                                return (
                                                                    <CustomerViewFirst
                                                                        key={index}
                                                                        onClick={() => this.activeClass(item, index)}
                                                                        FirstName={item.FirstName}
                                                                        LastName={item.LastName}
                                                                        PhoneNumber={item.Contact}
                                                                        className={active == index ? 'customer_active' : null}
                                                                    />
                                                                )
                                                            })}
                                                        {activeFilter == false &&
                                                            customerlist && customerlist.Content && customerlist.Content.map((item, index) => {
                                                                return (
                                                                    <CustomerViewFirst
                                                                        key={index}
                                                                        onClick={() => this.activeClass(item, index)}
                                                                        FirstName={item.FirstName}
                                                                        LastName={item.LastName}
                                                                        PhoneNumber={item.Contact}
                                                                        className={active == index ? 'customer_active' : null}
                                                                    />
                                                                )
                                                            })}


                                                    </tbody>
                                                </table>
                                            </div>
                                            <div className="createnewcustomer" >
                                                <button type="button" className="btn btn-block btn-primary total_checkout bg-blue" data-toggle="modal" href="#create-customer" >CREATE NEW CUSTOMER</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <CustomerViewSecond
                                    onChangePage={this.onChangePage}
                                    pageOfItems={pageOfItems}
                                    Props={this.props}
                                    UDID={UDID}
                                    ID={single_cutomer_list ? single_cutomer_list.Content.Id : ''}
                                    onClick={() => { this.goBack(single_cutomer_list) }}
                                    FirstName={single_cutomer_list ? single_cutomer_list.Content.FirstName : ''}
                                    LastName={single_cutomer_list ? single_cutomer_list.Content.LastName : ''}
                                    Email={single_cutomer_list ? single_cutomer_list.Content.Email : ''}
                                    PhoneNumber={single_cutomer_list ? single_cutomer_list.Content.Phone : ''}
                                    Address={single_cutomer_list ? single_cutomer_list.Content.StreetAddress : ''}
                                    Notes={single_cutomer_list ? single_cutomer_list.Content.Notes : ''}
                                    StoreCredit={single_cutomer_list ? single_cutomer_list.Content.StoreCredit : ''}
                                    AccountBalance={single_cutomer_list ? single_cutomer_list.Content.AccountBalance : ''}
                                    Details={single_cutomer_list ? single_cutomer_list.Content.orders : ''}
                                />
                            </div>

                        </div>
                    </div>
                </div>


                <div className="modal fade modal-wide in sidebar-minus" id="registeropenclose1" role="dialog">
                    <div className="modal-dialog modal-lg">
                        <div className="modal-content">
                            <div className="modal-header" style={{ display: 'none' }} >
                                <button type="button" className="close" data-dismiss="modal">×</button>
                                <h4 className="modal-title">Modal Header</h4>
                            </div>
                            <div className="modal-body p-0">
                                <div className="panel panel-custmer b-0">
                                    <div className="panel-default">
                                        <div className="panel-heading text-center customer_name font18 visible2">
                                            Close Register
                                        </div>
                                        <div className="panel-body customer_history p-0">
                                            <div className="col-sm-12">
                                                <div className="row">
                                                    <div className="col-lg-9 col-sm-8 plr-8">
                                                        <form className="customer-detail">
                                                            <div className="form-group">
                                                                <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 pl-0 mb-4 plr-8">
                                                                    <label htmlFor="">Register:</label>
                                                                    <div className="col-sm-12 p-0">
                                                                        <p>Register 1</p>
                                                                    </div>
                                                                </div>
                                                                <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 pl-0 mb-4 plr-8">
                                                                    <label htmlFor="">Date &amp; Time:</label>
                                                                    <div className="col-sm-12 p-0">
                                                                        <p className="text-blue">15. January 12:57PM</p>
                                                                    </div>
                                                                </div>
                                                                <div className="col-lg-4 col-md-6 col-sm-6 col-xs-6 pl-0 mb-4 plr-8">
                                                                    <label htmlFor="">User:</label>
                                                                    <div className="col-sm-12 p-0">
                                                                        <p>Mike Kellerman</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>
                                                    </div>
                                                    <div className="col-lg-3 col-sm-4 bl-1 plr-8">
                                                        <form action="#" className="w-100 pl-3">
                                                            <div className="w-100-block button_with_checkbox">
                                                                <input type="radio" id="p-report" name="radio-group" />
                                                                <label htmlFor="p-report" className="label_select_button">Print Report</label>
                                                            </div>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="panel-footer bt-0 p-0">
                                            <div className="table_head">
                                                <table className="table table-border table-hover borderless c_change mb-0 tbl-bb-1 tbl-layout-fixe">
                                                    <colgroup>
                                                        <col width="*" />
                                                        <col width="350" />
                                                        <col width="350" />
                                                        <col width="350" />
                                                    </colgroup>
                                                    <tbody>
                                                        <tr>
                                                            <th className="bt-0 pb-0 bb-1">Item</th>
                                                            <th className="bt-0 pb-0 text-right bb-1">Expected</th>
                                                            <th className="bt-0 pb-0 text-right bb-1">Actual</th>
                                                            <th className="bt-0 pb-0 text-right bb-1">Difference</th>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                            <div className="">
                                                <table className="table table-borderless tbl-bb-1 font tbl-layout-fixe pt-3 pb-3">
                                                    <colgroup>
                                                        <col width="*" />
                                                        <col width="350" />
                                                        <col width="350" />
                                                        <col width="350" />
                                                    </colgroup>
                                                    <tbody>
                                                        <tr>
                                                            <td>Cash In Tiller</td>
                                                            <td align="right">950.00 </td>
                                                            <td align="right" className="text-blue">1050.00</td>
                                                            <td align="right">1050.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Debit/Credit Card</td>
                                                            <td align="right">950.00 </td>
                                                            <td align="right" className="text-blue">1050.00</td>
                                                            <td align="right">1050.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Gift Card</td>
                                                            <td align="right">950.00 </td>
                                                            <td align="right" className="text-blue">1050.00</td>
                                                            <td align="right">1050.00</td>
                                                        </tr>
                                                        <tr>
                                                            <td>Others</td>
                                                            <td align="right">950.00 </td>
                                                            <td align="right" className="text-blue">1050.00</td>
                                                            <td align="right">1050.00</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <div className="col-lg-offset-7 col-lg-5 col-sm-offset-6 col-sm-6 col-xs-offset-4 col-xs-8 p-0">
                                                    <table className="table table-borderless tbl-bb-1 mb-0 font pt-1 pb-1">
                                                        <tbody>
                                                            <tr>
                                                                <td className="pl-0">Total Difference</td>
                                                                <td className="text-right">10.00</td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div className="col-sm-12">
                                                    <div className="register-closenote">
                                                        <textarea cols="12" rows="4" className="form-control" placeholder="Enter notes here"></textarea>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div className="modal-footer p-0">
                                <button type="button" className="btn btn-primary btn-block h66">SAVE &amp; UPDATE</button>
                            </div>
                        </div>
                    </div>
                </div>

                {Cust_ID ? (
                    <div id="edit-information" className="modal modal-wide fade full_height_one">

                        <CustomerViewEdit
                            onChange={this.handleChange}
                            onClick={() => this.handleSubmit()}
                            user_address={user_address ? user_address : ''}
                            user_city={user_city ? user_city : ''}
                            user_contact={user_contact ? user_contact : ''}
                            user_fname={user_fname ? user_fname : ''}
                            user_lname={user_lname ? user_lname : ''}
                            user_email={user_email ? user_email : ''}
                            user_note={user_note ? user_note : ''}
                            user_pcode={user_pcode ? user_pcode : ''}
                            submitted={submitted}
                        />

                    </div>
                ) :

                    <div id="edit-information" className="modal modal-wide modal-wide1 fade">
                        <div className="modal-dialog" id="dialog-midle-align">
                            <div className="modal-content">
                                <div className="modal-header">
                                    <button type="button" className="close" data-dismiss="modal" aria-hidden="true">
                                        <img src="assets/img/delete-icon.png" />
                                    </button>
                                    <h4 className="error_model_title modal-title" id="epos_error_model_title">Message</h4>
                                </div>
                                <div className="modal-body p-0">
                                    <h3 id="epos_error_model_message" className="popup_payment_error_msg">Please Choose Customer !</h3>
                                </div>
                                <div className="modal-footer p-0">
                                    <button type="button" className="btn btn-primary btn-block h66" data-dismiss="modal" aria-hidden="true">OK</button>
                                </div>
                            </div>
                        </div>
                    </div>

                }


                <div id="delete-information" className="modal modal-wide modal-wide1 fade">
                    <div className="modal-dialog" id="dialog-midle-align">
                        <div className="modal-content">
                            <div className="modal-header">
                                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">
                                    <img src="assets/img/delete-icon.png" />
                                </button>
                                <h4 className="error_model_title modal-title" id="epos_error_model_title">Alert Message</h4>
                            </div>
                            <div className="modal-body p-0">
                                {Cust_ID ? (
                                    <h3 id="epos_error_model_message" className="popup_payment_error_msg">Are You Delete This Reacord !</h3>
                                )
                                    :
                                    <h3 id="epos_error_model_message" className="popup_payment_error_msg">Please Choose Customer !</h3>
                                }
                            </div>
                            <div className="modal-footer p-0">
                                {Cust_ID ? (
                                    <button type="button" onClick={() => this.deleteCustomer()} className="btn btn-primary btn-block h66" data-dismiss="modal" aria-hidden="true">OK</button>
                                ) :
                                    <button type="button" className="btn btn-primary btn-block h66" data-dismiss="modal" aria-hidden="true">OK</button>
                                }
                            </div>
                        </div>
                    </div>
                </div>

                <div id="create-customer" className="modal modal-wide fade full_height_one">
                    <CustomerViewCreate
                        onChange={this.handleChange}
                        onClick={() => this.handleSubmit()}
                        cust_street_address={cust_street_address ? cust_street_address : ''}
                        cust_city={cust_city ? cust_city : ''}
                        cust_phone_number={cust_phone_number ? cust_phone_number : ''}
                        cust_fname={cust_fname ? cust_fname : ''}
                        cust_lname={cust_lname ? cust_lname : ''}
                        cust_email={cust_email ? cust_email : ''}
                        cust_note={cust_note ? cust_note : ''}
                        cust_postcode={cust_postcode ? cust_postcode : ''}
                        submitted={submitted}
                        emailValid={emailValid}
                    />
                </div>

            </div>
        )
    }
}

function mapStateToProps(state) {
    const { single_cutomer_list, customerlist, filteredList } = state;
    return {
        single_cutomer_list: single_cutomer_list.items,
        customerlist: customerlist.items,
        filteredList: filteredList.items,
    };
}

const connectedCustomerView = connect(mapStateToProps)(CustomerView);
export { connectedCustomerView as CustomerView };