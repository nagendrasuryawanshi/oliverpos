import React from 'react';

export const CustomerViewEdit = (props) => {
return(
    <div className="modal-dialog">
        <div className="modal-content">
            <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">
                    <img src="assets/img/delete-icon.png" />
                </button>
                <h4 className="modal-title">Edit Information</h4>
            </div>
            <div className="modal-body overflowscroll" id="scroll_mdl_body">
                <form className="clearfix form_editinfo">
                    <div className="col-sm-6">
                        <div className={'form-group' + (props.submitted && !props.user_fname ? ' has-error' : '')}>
                            <div className="input-group">
                                <div className="input-group-addon">
                                    First Name
                                               </div>
                                <input className="form-control" value={props.user_fname} id="user_fname" name="user_fname" type="text" onChange={props.onChange} />
                            </div>
                            {props.submitted && !props.user_fname &&
                                <div className="help-block">First Name is required</div>
                            }
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <div className={'form-group' + (props.submitted && !props.user_lname ? ' has-error' : '')}>
                            <div className="input-group">
                                <div className="input-group-addon">
                                    Last Name
                                                </div>
                                <input className="form-control" value={props.user_lname} id="user_lname" name="user_lname" type="text" onChange={props.onChange} />
                            </div>
                            {props.submitted && !props.user_lname &&
                                <div className="help-block">Last Name is required</div>
                            }
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <div className={'form-group' + (props.submitted && !props.user_email ? ' has-error' : '')}>
                            <div className="input-group">
                                <div className="input-group-addon">
                                    Email
                                                 </div>
                                <input className="form-control" value={props.user_email} id="user_email" name="user_email" type="text" disabled />
                            </div>
                            {props.submitted && !props.user_email &&
                                <div className="help-block">Email is required</div>
                            }
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <div className={'form-group' + (props.submitted && !props.user_contact ? ' has-error' : '')}>
                            <div className="input-group">
                                <div className="input-group-addon">
                                    Phone Number
                                                </div>
                                <input maxLength={10} className="form-control" value={props.user_contact} id="user_contact" name="user_contact" type="text" onChange={props.onChange} />
                            </div>
                            {props.submitted && !props.user_contact &&
                                <div className="help-block">Contact Number is required</div>
                            }
                        </div>
                    </div>
                    <div className="col-sm-12">
                        <div className={'form-group' + (props.submitted && !props.user_address ? ' has-error' : '')}>
                            <div className="input-group">
                                <div className="input-group-addon">
                                    Street Address&nbsp;
                                                </div>
                                <input className="form-control" value={props.user_address} id="user_address" name="user_address" type="text" onChange={props.onChange} />
                            </div>
                            {props.submitted && !props.user_address &&
                                <div className="help-block">Address is required</div>
                            }
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <div className={'form-group' + (props.submitted && !props.user_city ? ' has-error' : '')}>
                            <div className="input-group">
                                <div className="input-group-addon">
                                    City&nbsp;&nbsp;
                                                 </div>
                                <input className="form-control" value={props.user_city} id="user_city" name="user_city" type="text" onChange={props.onChange} />
                            </div>
                            {props.submitted && !props.user_city &&
                                <div className="help-block">City Name is required</div>
                            }
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <div className={'form-group' + (props.submitted && !props.user_pcode ? ' has-error' : '')}>
                            <div className="input-group">
                                <div className="input-group-addon">
                                    Post Code
                                                </div>
                                <input className="form-control" value={props.user_pcode} id="user_pcode" name="user_pcode" type="text" onChange={props.onChange} />
                            </div>
                            {props.submitted && !props.user_pcode &&
                                <div className="help-block">Post Code is required</div>
                            }
                        </div>
                    </div>
                    <div className="col-sm-12">
                        {/* <div className={'form-group' + (submitted && !user_note ? ' has-error' : '')}> */}
                        <div className='form-group'>
                            <div className="input-group">
                                <div className="input-group-addon">
                                    Notes
                                                </div>
                                <textarea className="form-control" name="user_note" type="text" value={props.user_note} onChange={props.onChange}>{props.user_note}</textarea>
                            </div>
                            {/* {submitted && !user_note &&
                                                <div className="help-block">Notes is required</div>
                                            } */}
                        </div>
                    </div>
                </form>
            </div>
            <div className="modal-footer p-0">
                <button type="button" onClick={props.onClick} className="btn btn-primary btn-block h66">SAVE & UPDATE</button>
            </div>
        </div>
    </div>
)
}