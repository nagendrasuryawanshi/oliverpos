import React from 'react';


export const CustomerViewCreate = (props) =>{
    console.log("props.cust_email",props)
    return(
        <div className="modal-dialog">
        <div className="modal-content">
            <div className="modal-header">
                <button type="button" className="close" data-dismiss="modal" aria-hidden="true">
                    <img src="assets/img/delete-icon.png" />
                </button>
                <h4 className="modal-title">Add New Customer</h4>
            </div>
            <div className="modal-body overflowscroll" id="scroll_mdl_body">
                <form name="form" className="clearfix form_editinfo">
                    <div className="col-sm-6">
                        <div className={'form-group' + (props.submitted && !props.cust_fname ? ' has-error' : '')}>
                            <div className="input-group">
                                <div className="input-group-addon">
                                    First Name
                               </div>
                                <input className="form-control" name="cust_fname" value={props.cust_fname} placeholder="First Name" type="text" onChange={props.onChange} />
                            </div>
                            {props.submitted && !props.cust_fname &&
                                <div className="help-block">First Name is required</div>
                            }
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <div className={'form-group' + (props.submitted && !props.cust_lname ? ' has-error' : '')}>
                            <div className="input-group">
                                <div className="input-group-addon">
                                    Last Name
                                 </div>
                                <input className="form-control" name="cust_lname" value={props.cust_lname} placeholder="Last Name" type="text" onChange={props.onChange} />
                            </div>
                            {props.submitted && !props.cust_lname &&
                                <div className="help-block">Last Name is required</div>
                            }
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <div className={'form-group' + (props.submitted && !props.cust_email ? ' has-error' : '')}>
                            <div className="input-group">
                                <div className="input-group-addon">
                                    Email
                                 </div>
                                <input className="form-control" name="cust_email" value={props.cust_email} placeholder="Email" type="email" onChange={props.onChange} />
                            </div>
                            {props.submitted && !props.cust_email &&
                                 <div className="help-block">{props.emailValid == false ? 'email is required':' '}</div>
                               }
                           {props.emailValid == 'email is Invalid' &&
                                 <div className="help-block" style={{color:'#a94442'}}>{props.emailValid}</div>
                               }
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <div className={'form-group' + (props.submitted && !props.cust_phone_number ? ' has-error' : '')}>
                            <div className="input-group">
                                <div className="input-group-addon">
                                    Phone Number
                                 </div>
                                <input maxLength={10} className="form-control" name="cust_phone_number" value={props.cust_phone_number} placeholder="Phone Number" type="tel" onChange={props.onChange} />
                            </div>
                            {props.submitted && !props.cust_phone_number &&
                                <div className="help-block">Phone Number is required</div>
                            }
                        </div>
                    </div>
                    <div className="col-sm-12">
                        <div className={'form-group' + (props.submitted && !props.cust_street_address ? ' has-error' : '')}>
                            <div className="input-group">
                                <div className="input-group-addon">
                                    Street Address&nbsp;
                                  </div>
                                <input className="form-control" name="cust_street_address" value={props.cust_street_address} placeholder="Street Address" type="text" onChange={props.onChange} />
                            </div>
                            {props.submitted && !props.cust_street_address &&
                                <div className="help-block">Address is required</div>
                            }
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <div className={'form-group' + (props.submitted && !props.cust_city ? ' has-error' : '')}>
                            <div className="input-group">
                                <div className="input-group-addon">
                                    City&nbsp;&nbsp;
                                   </div>
                                <input className="form-control" name="cust_city" value={props.cust_city} placeholder="City" type="text" onChange={props.onChange} />
                            </div>
                            {props.submitted && !props.cust_city &&
                                <div className="help-block">City Name is required</div>
                            }
                        </div>
                    </div>
                    <div className="col-sm-6">
                        <div className={'form-group' + (props.submitted && !props.cust_postcode ? ' has-error' : '')}>
                            <div className="input-group">
                                <div className="input-group-addon">
                                    Post Code
                                 </div>
                                <input className="form-control" name="cust_postcode" value={props.cust_postcode} placeholder="Post Code" type="text" onChange={props.onChange} />
                            </div>
                            {props.submitted && !props.cust_postcode &&
                                <div className="help-block">Post Code is required</div>
                            }
                        </div>
                    </div>
                    <div className="col-sm-12">
                        <div className={'form-group' + (props.submitted && !props.cust_note ? ' has-error' : '')}>
                            <div className="input-group">
                                <div className="input-group-addon">
                                    Notes
                                 </div>
                                <textarea className="form-control" name="cust_note" type="text" value={props.cust_note} placeholder="Testing" onChange={props.onChange} />
                            </div>
                            {props.submitted && !props.cust_note &&
                                <div className="help-block">Notes is required</div>
                            }
                        </div>
                    </div>
                </form>
            </div>
            <div className="modal-footer p-0">
                <button onClick={props.onClick} type="button" className="btn btn-primary btn-block h66">SAVE & UPDATE</button>
            </div>
        </div>
    </div>
    )
}