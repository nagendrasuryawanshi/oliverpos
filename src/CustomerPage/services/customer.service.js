import Config from '../../Config'

export const customerService = {
    getAll,
    save,
    getDetail,
    Delete,
    removeStoreCredit
};

const API_URL=Config.key.OP_API_URL

function getAll(uid) {
    const requestOptions = {
        method: 'GET',
     };
    return fetch(`${API_URL}/ShopCustomer/GetAll?Udid=${uid}`, requestOptions).then(handleResponse);
}

function getDetail(id,uid) {
    const requestOptions = {
        method: 'GET',
     };
    return fetch(`${API_URL}/ShopCustomer/GetDetail?Udid=${uid}&id=${id}`, requestOptions).then(handleResponse);
}

function save(customer) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify(customer)
    };

    return fetch(`${API_URL}/ShopCustomer/Save`, requestOptions).then(handleResponse);
}

function Delete(id,uid) {
    const requestOptions = {
        method: 'GET',
     };
    return fetch(`${API_URL}/ShopCustomer/Delete?Udid=${uid}&id=${id}`, requestOptions).then(handleResponse);
}

function removeStoreCredit(uid,id,amount){
    const requestOptions = {
        method: 'GET',
     };
    return fetch(`${API_URL}/ShopCustomer/RemoveFromStoreCredit?Udid=${uid}&Id=${id}&point=${amount}`, requestOptions).then(handleResponse);
}


function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text);
        if (!response.ok) {
            if (response.status === 401) {
                // auto logout if 401 response returned from api
                logout();
                location.reload(true);
            }

            const error = (data && data.message) || response.statusText;
            return Promise.reject(error);
        }

        return data;
    });
}