import { customerConstants } from '../constants/customer.constants'
import { customerService } from '../services/customer.service';
import { alertActions } from '../../_actions';
import { history } from '../../_helpers';


export const customerActions = {
    getAll,
    save,
    getDetail,
    Delete,
    filteredList,
    removeStoreCredit
  
};

function getAll(UID) {
    return dispatch => {
        dispatch(request());

        customerService.getAll(UID)
            .then(
                customerlist => dispatch(success(customerlist)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: customerConstants.GETALL_REQUEST } }
    function success(customerlist) { return { type: customerConstants.GETALL_SUCCESS, customerlist } }
    function failure(error) { return { type: customerConstants.GETALL_FAILURE, error } }
}

function getDetail(ID,UID) {
    return dispatch => {
        dispatch(request());

        customerService.getDetail(ID,UID)
            .then(
                single_cutomer_list => dispatch(success(single_cutomer_list)),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: customerConstants.GET_DETAIL_REQUEST } }
    function success(single_cutomer_list) { return { type: customerConstants.GET_DETAIL_SUCCESS, single_cutomer_list } }
    function failure(error) { return { type: customerConstants.GET_DETAIL_FAILURE, error } }
}

function save(customer) {
    return dispatch => {
        dispatch(request(customer));

        customerService.save(customer)
            .then(
                customer => { 
                    
                    dispatch(success(customer));
                    history.push('/customerview');
                    dispatch(alertActions.success('save custmoer detail successfully'));
                },
                error => {
                    dispatch(failure(error.toString()));
                    dispatch(alertActions.error(error.toString()));
                }
            );
    };

    function request() { return { type: customerConstants.INSERT_REQUEST } }
    function success(customer) { return { type: customerConstants.INSERT_SUCCESS, customer } }
    function failure(error) { return { type: customerConstants.INSERT_FAILURE, error } }
}

function Delete(ID,UID) {
    return dispatch => {
        dispatch(request());

        customerService.Delete(ID,UID)
            .then(
                customer => dispatch(success()),
                error => dispatch(failure(error.toString()))
            );
    };

    function request() { return { type: customerConstants.DELETE_REQUEST } }
    function success(customer) { return { type: customerConstants.DELETE_SUCCESS ,customer} }
    function failure(error) { return { type: customerConstants.DELETE_FAILURE, error } }
}

function filteredList(filteredList){
    return dispatch => {
         dispatch(success(filteredList))
         };

       function success(filteredList) { return { type: customerConstants.GET_FILTER_SUCCESS, filteredList } }
  
}

function removeStoreCredit(UDID,Id,amount){
    return dispatch => {
        dispatch(request(UDID,Id,amount));

        customerService.removeStoreCredit(UDID,Id,amount)
            .then(
               remove_customer => { 
                    dispatch(success(remove_customer));
                    //window.localtion = '/checkout';
                  },
                error => {
                    dispatch(failure(error.toString()));
                 }
            );
    };

    function request() { return { type: customerConstants.REMOVE_STORE_CREDIT_REQUEST } }
    function success(remove_customer) { return { type: customerConstants.REMOVE_STORE_CREDIT_SUCCESS, remove_customer } }
    function failure(error) { return { type: customerConstants.REMOVE_STORE_CREDIT_FAILURE, error } }
}


