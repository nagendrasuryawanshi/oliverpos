import React from 'react';
import { connect } from 'react-redux';
import { locationActions } from './action/location.action';
 import { history } from "../_helpers";
class LoginLocation extends React.Component {
    constructor(props) {
        super(props);

    }
    componentWillMount(){
        // if(localStorage.getItem("sitelist") &&  localStorage.getItem('UDID')) //&&  localStorage.getItem('Location'))
        //  history.push('/loginpin');
    }
    handleSubmit(Id) {
        localStorage.setItem('Location',Id)
        
        history.push('/choose_registration');
       // history.push('/loginpin');
        
    }
    windowLocation(location) {
        window.location = location
    }
     componentDidMount(){
        this.props.dispatch(locationActions.getAll());
        $(".chooseregisterLinks").niceScroll({styler: "fb",cursorcolor: "#2BB6E2",cursorwidth: '3',cursorborderradius: '10px',background: '#d5d5d5',spacebarenabled: false,cursorborder: '',scrollspeed: 60,cursorfixedheight: 70});   
     }
    render() {
        var UserLocations = this.props.locations;//JSON.parse(localStorage.getItem('UserLocations'));
console.log("UserLocations",UserLocations);
        return (
            <div className="bgimg-1">
            <div className="content_main_wapper">
                <a href="/site_link" className="aonboardingbackicon">
                    <button className="button onboardingbackicon" onClick={() => this.windowLocation('/site_link')}>&nbsp;</button>&nbsp; Go Back
       </a>
               
                    <div className="onboarding-loginBox">
                        <div className="login-form">
                            <div className="onboarding-pg-heading">
                                <h1>Choose Your Location</h1>
                                <p>
                                You’ve set up these locations. Choose the Location you’re working from right now. </p>
                            </div>
                            <form className="onboarding-login-field" action="#">
                               <ul className="list-group chooseregisterLinks m-b-20" style={{height: 230}}>
                                        
                                {UserLocations?UserLocations.map((item, index) => {
                                    return (
                                        <li key={index} className="list-group-item" onClick={()=>this.handleSubmit(item.Id)} ><a href="#">{item.Name}<img src="assets/img/green-arrow.png"/></a></li>
                                    )
                                }):null} 
                                </ul> 
                                <div className="checkBox-custom m-b-20">
                                    <div className="checkbox">
                                        <label className="customcheckbox">Remember location
                               <input type="checkbox" checked="checked" readOnly={true} />
                                            <span className="checkmark"></span>
                                        </label>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div className="powered-by-oliver">
                        <a href="javascript:void(0)">Powered by Oliver POS</a>
                    </div>
              
            </div>
            </div>
        );
    }
}

function mapStateToProps(state) {  
    const { locations } = state.locations;
    return {
        locations
    };
}

const connectedLoginLocation = connect(mapStateToProps)(LoginLocation);
export { connectedLoginLocation as LoginLocation };