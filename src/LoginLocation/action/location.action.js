import { locationConstants } from "../constants/location.constants";
import { locationService } from "../services/location.service";


export const locationActions = {
  
      getAll,
 
  };

  function getAll() {
    
    return dispatch => {
      dispatch(request());
     console.log("UDID",localStorage.getItem('UDID'));
      locationService
        .getAll(localStorage.getItem('UDID'))
        .then(
          locations => dispatch(success(locations)),
  
          error => dispatch(failure(error.toString()))
        );
    };
  
    function request() {
      return { type: locationConstants.GETALL_REQUEST };
    }
    function success(locations) {
      return { type: locationConstants.GETALL_SUCCESS, locations };
    }
    function failure(error) {
      return { type: locationConstants.GETALL_FAILURE, error };
    }
  }