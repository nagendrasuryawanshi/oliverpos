import { locationConstants  } from '../constants/location.constants';

export function locations(state = {}, action) {
    switch (action.type) {
        case locationConstants.GETALL_REQUEST:
            return {
                loading: true
            };
        case locationConstants.GETALL_SUCCESS:
        console.log("locations",action.locations);
            return {
                locations: action.locations
            };
        case locationConstants.GETALL_FAILURE:
            return {
                error: action.error
            };

        default:
            return state
    }
}

