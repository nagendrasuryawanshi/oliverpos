import Config from '../../Config'

export const locationService = {    
    getAll,  
  };

  const API_URL=Config.key.OP_API_URL

function getAll(UDID) {
  //console.log("login Jason", JSON.stringify({ email, password }))
  const requestOptions = {
      method: 'GET',           
    //  mode: 'no-cors',
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json',
    }
  };

  //return fetch(`${config.apiUrl}/users/authenticate`, requestOptions)
  return fetch(`${API_URL}/ShopAccess/GetLocations?udid=`+UDID, requestOptions)
      .then(handleResponse)
      .then(locationsRes => {
              console.log("location Response",locationsRes.Content);
              localStorage.setItem('user', JSON.stringify(locationsRes.Content));
              localStorage.setItem('UserLocations', JSON.stringify(locationsRes.Content));
              let locations=locationsRes.Content;
              return locations;
        
          
      });
} 


function handleResponse(response) {
  return response.text().then(text => {
      const data = text && JSON.parse(text);
      if (!response.ok) {
          if (response.status === 401) {
              // auto logout if 401 response returned from api
              logout();
              location.reload(true);
          }

          const error = (data && data.message) || response.statusText;
          return Promise.reject(error);
      }

      return data;
  });
}