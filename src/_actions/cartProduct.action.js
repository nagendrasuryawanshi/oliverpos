import { cartProductConstants } from '../_constants/cartProduct.constants'
import { discount_calculate } from '../_components/CalculatorFunction';
import { ShopView } from '../ShopView'

export const cartProductActions = {

    refresh,
    addtoCartProduct,
    selectedProductDis
};

function addtoCartProduct(cartproductlist) {
    console.log("cartProductAction", cartproductlist);
    var udid = localStorage.getItem("UDID")
    //localStorage.setItem("CARD_PRODUCT_LIST",JSON.stringify(cartproductlist));
    localStorage.setItem("cartproductlist-" + udid, cartproductlist);
 

   
    const result=Array.from(new Set(cartproductlist.map(s => s.product_id)) )
    .map(id =>{
      return{
        Price:  cartproductlist.find(s=> s.product_id === id ).Price,
        Title: cartproductlist.find(s=> s.product_id === id ).Title,
        product_id: id,
        quantity:cartproductlist.find(s=> s.product_id === id ).quantity
       

      }

    } )
    console.log("result123",result);
    //-----------------------    {

    var totalPrice = 0;
    //let ListItem =  cartproductlist? cartproductlist:[];
    cartproductlist && cartproductlist.map((item, index) => {
        totalPrice += item.Price
    })


    let cart = localStorage.getItem("CART") ? JSON.parse(localStorage.getItem("CART")) : null
    let product = localStorage.getItem("PRODUCT") ? JSON.parse(localStorage.getItem("PRODUCT")) : null
    let single_product = localStorage.getItem("SINGLE_PRODUCT") ? JSON.parse(localStorage.getItem("SINGLE_PRODUCT")) : null
    console.log("PRODUCT", product)
    if (cart !== null) {
        var product_discount = 0;
        if (product !== null && single_product !== null) {
            product_discount = parseFloat(product.discount_amount)
        }
        console.log("product_discount", product_discount);
        cartproductlist && cartproductlist.map(item => {
            var price = parseFloat(item.Price);
            var discount = parseFloat(cart.discount_amount)
            var Tax = parseFloat(cart.Tax_rate);
            if (cart.discountType == 'Percentage' && discount < 100) {
                var discount_amount_is = (price * discount / 100);
                var afterDiscount = price - discount_amount_is;
                var total_tax = afterDiscount + (afterDiscount * Tax / 100);
                item["after_discount"] = afterDiscount;
                item["discount_Amount"] = discount_amount_is;
            } else if (cart.discountType == 'Percentage' && cart.discount_amount >= 100) {
               // alert("plaese not use 100% discount")
                 $('#no_discount').modal('show')
            }
            else {
                var new_discount = parseFloat(discount * 100 / totalPrice);
                if (new_discount >= 100 && cart.discountType !== 'Percentage') {
                     // alert("plaese not use 100% discount")
                // $('#no_discount').modal('show')
                } else {
                    discount_amount_is = (price * new_discount / 100);
                    afterDiscount = price - discount_amount_is;
                    total_tax = afterDiscount + (afterDiscount * Tax / 100);
                    item["after_discount"] = afterDiscount;
                    item["discount_Amount"] = discount_amount_is;
                }
            }
        })

    }
    if (product !== null && single_product !== null) {
        var cart_discount = 0;
        if (cart !== null) {
             cart_discount = parseFloat(cart.discount_amount)
        }
        console.log("cart_discount", cart_discount);
        var item = single_product
        var product_id = item.product_id;
        var price = parseFloat(item.Price);
        var discount = parseFloat(product.discount_amount)
        var Tax = parseFloat(product.Tax_rate);

        if (product.discountType == 'Percentage' && discount < 100) {

            if (cart_discount !== 0 && cart.discountType == 'Percentage') {
                var discount_amount_is = (price * (discount + cart_discount) / 100);

            } else if (cart_discount !== 0 && cart.discountType !== 'Percentage') {
                var new_discount = parseFloat(cart_discount * 100 / price);
                var discount_amount_is = (price * (discount + new_discount) / 100);

            } else {
                var discount_amount_is = (price * discount / 100)
            }
            var afterDiscount = price - discount_amount_is
            total_tax = (typeof afterDiscount !== "undefined") ? afterDiscount + (afterDiscount * Tax / 100) : price + (price * Tax / 100);
            var updateProductList = cartproductlist;
            updateProductList.map((item, index) => {
                if (item.product_id == product_id) {
                    updateProductList[index]["after_discount"] = afterDiscount
                    updateProductList[index]["discount_Amount"] = discount_amount_is
                 }
            })





        } else if (product.discountType == 'Percentage' && discount >= 100) {
             // alert("plaese not use 100% discount")
             $('#no_discount').modal('show')
        } else {
            if (cart_discount !== 0 && cart.discountType !== 'Percentage') {
                var new_discount1 = parseFloat(discount * 100 / price);
                var new_discount2 = parseFloat(cart_discount * 100 / price)
                var new_discount = parseFloat(new_discount1 + new_discount2)

            }
            else if (cart_discount !== 0 && cart.discountType == 'Percentage') {
                var new_discount1 = parseFloat(discount * 100 / price);
                var new_discount = parseFloat(new_discount1 + cart_discount)
             }
            else {
                var new_discount = parseFloat(discount * 100 / price);
             }
             if (new_discount >= 100 && product.discountType !== 'Percentage' ) {
                 // alert("plaese not use 100% discount")
                // $('#no_discount').modal('show')
            } else {
                discount_amount_is = (price * new_discount / 100)
                afterDiscount = price - discount_amount_is
                var total_tax = afterDiscount + (afterDiscount * Tax / 100)
                var updateProductList = cartproductlist;
                updateProductList.map((item, index) => {
                    if (item.product_id == product_id) {
                        updateProductList[index]["after_discount"] = afterDiscount
                        updateProductList[index]["discount_Amount"] = discount_amount_is
                    }
                })

                console.log("updateProductList", updateProductList)

            }
        }

    }

    localStorage.setItem("CARD_PRODUCT_LIST", JSON.stringify(cartproductlist));
    // }
    //}
    //-------------------------
    return dispatch => {
        dispatch(request());
        dispatch(success(JSON.parse(localStorage.getItem("CARD_PRODUCT_LIST"))));
    };

    function request() { return { type: cartProductConstants.PRODUCT_ADD_REQUEST } }
    function success(cartproductlist) { return { type: cartProductConstants.PRODUCT_ADD_SUCCESS, cartproductlist } }

}


function refresh() {
    return { type: cartProductConstants.PRODUCT_GETALL_REFRESH };
}

function selectedProductDis(selecteditem) {
    // console.log("selecteditemactionPage",selecteditem);

    return dispatch => {
        dispatch(request());
        dispatch(success(selecteditem))
    };

    function request() { return { type: cartProductConstants.SELECTED_PRODUCT_DISCOUNT_REQUEST } }
    function success(selecteditem) { return { type: cartProductConstants.SELECTED_PRODUCT_DISCOUNT_SUCCESS, selecteditem } }

}



