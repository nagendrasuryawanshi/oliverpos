import { allProductConstants } from '../_constants/allProduct.constants'
import { allProductService } from '../_services/allProduct.service';


export const allProductActions = {
    getAll,
    refresh,
    filteredProduct
};

function getAll() {
    return dispatch => {
        dispatch(request());
        let productlist=[]
        dispatch(success(productlist))
        allProductService.getAll()
            .then(               
                productlist => 
               { 
               // console.log("productlist",productlist)
                dispatch(success(productlist)),
                error => dispatch(failure(error.toString()))
               }
            );
    };

    function request() { return { type: allProductConstants.PRODUCT_GETALL_REQUEST } }
    function success(productlist) { return { type: allProductConstants.PRODUCT_GETALL_SUCCESS, productlist } }
    function failure(error) { return { type: allProductConstants.PRODUCT_GETALL_FAILURE, error } }
}


function refresh() {   
    return { type: allProductConstants.PRODUCT_GETALL_REFRESH };
}

function filteredProduct(filteredProduct=[]){
    return dispatch => {
        dispatch(success(filteredProduct))
        };

      function success(filteredProduct) { return { type: allProductConstants.FILTERED_ALL_PRODUCTS_SUCCESS, filteredProduct } }
}

