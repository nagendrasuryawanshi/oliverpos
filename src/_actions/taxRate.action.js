import { taxRateConstants } from '../_constants/taxRate.constants';
import { taxRateService } from '../_services/taxRate.service'


export const taxRateAction = {
    getAll,refresh   
};

function getAll() {
    return dispatch => {
        dispatch(request());
       // let taxratelist={}
       // dispatch(success(taxratelist))
        taxRateService.getAll()
            .then(               
                taxratelist => 
               { 
               // console.log("taxratelist",taxratelist)
                dispatch(success(taxratelist)),
                error => dispatch(failure(error.toString()))
               }
            );
    };

    function request() { return { type: taxRateConstants.GETALL_REQUEST } }
    function success(taxratelist) { return { type: taxRateConstants.GETALL_SUCCESS, taxratelist } }
    function failure(error) { return { type: taxRateConstants.GETALL_FAILURE, error } }
}

function refresh() {   
    return { type: taxRateConstants.GETALL_REFRESH };
}

