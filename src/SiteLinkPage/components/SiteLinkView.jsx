import React from 'react';
import { connect } from 'react-redux';
import { siteActions } from '../actions/site_link.action';
import { SiteLinkViewFirst } from './SiteLinkViewFirst';
import { history } from '../../_helpers';
class SiteLinkView extends React.Component {
    constructor(props) {
        super(props);

    }
    componentWillMount(){
        console.log("Login sitelist", localStorage.getItem("sitelist"));
        if(this.props.sitelist || localStorage.getItem("sitelist"))
        {//history.push('/site_link');
    }
    }
    render() {
     
       return (
        <div className="bgimg-1">
        <div className="content_main_wapper">
        <a href="/login" className="aonboardingbackicon">
              <button className="button onboardingbackicon">&nbsp;</button>&nbsp; Go Back
          </a>
  
          {/* <div className="dialog-content"> */}
              <div className="onboarding-loginBox">
              <div className="login-form">
                   <div className="onboarding-pg-heading">
                      <h1>Select Your Site</h1>
                      <p>
                        The sites below are registered to your Oliver POS Account.  Select the one you want to operate on this register?
                      </p>
                  </div>
                <SiteLinkViewFirst {...this.props}/>
              </div>
          </div>
                <div className="powered-by-oliver">
                    <a href="javascript:void(0)">Powered by Oliver POS</a>
                </div>
          {/* </div> */}
      </div>
      </div>
        );
    }
}

function mapStateToProps(state) {
    console.log("state",state);
    const { authentication } = state;
    return {
        sitelist :authentication.sitelist
    };
}

const connectedSiteLinkView = connect(mapStateToProps ,siteActions)(SiteLinkView);
export { connectedSiteLinkView as SiteLinkView };