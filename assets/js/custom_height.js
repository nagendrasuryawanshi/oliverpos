$(window).bind("load resize", function() {
    setHeightDesktop();
    EnableContentScroll();

    $('button.swipclock').on('click', function() {
        $(".secmk").toggleClass('cm-swipper');
    });

    if ($('.flat-toggle.cm-flat-toggle').hasClass("on")) {
        $('.cm-user-switcher .flat-toggle').find("span").addClass('open');
        $('.cm-user-switcher .flat-toggle').find("span").removeClass('close');
    } else {
        $('.cm-user-switcher .flat-toggle').find("span").addClass('close');
        $('.cm-user-switcher .flat-toggle').find("span").removeClass('open');
    }

    $('.flat-toggle.cm-flat-toggle').on('click', function() {
        $(this).toggleClass('on');
        if ($(this).hasClass("on")) {
            $(this).find("span").addClass('open');
            $(this).find("span").removeClass('close');
            showModal();
        } else {
            hideModal();
            $(this).find("span").addClass('close');
            $(this).find("span").removeClass('open');
        }
    });
    $(".full_height_modal").on("show.bs.modal", function() {
        var height = $(window).height() - ($(".nameSearch").height() + 100);
        $(this).find(".modal-body").css("height", height);
    });

    $(".full_height_one").on("show.bs.modal", function() {
        var height = $(window).height() - 150;
        $(this).find(".modal-body").css("height", height);
    });
});


// /-------Overlay popup setting------/
function showModal() {
    $('#registeropenclose1').appendTo("body").modal('show');
    $('.modal-backdrop').addClass('backdrop-changing');
}
function hideModal() {
    $('#registeropenclose1').appendTo("body").modal('hide');
    $('.modal-backdrop').removeClass('backdrop-changing');
}


function setHeightDesktop() {
  // alert("Alert Shows After 5 Seconds of Delay.");
    var cart_header = $(".cart_header").height();
    var bodyHeight = $(window).height() - ($("#colorFullHeader").height() + 16);

        // $(".window-height").css({
        //    "height" : $(window).height() - 150,
        //      // "top"  : 205
        // });
    
    $(".cm-body-init-scroll").height($(window).height() - ($(".cm-switcher").height() + $(".cm-sec-acc").height() + 45));
    // A_B_height S_C_height
    $("#A_B_height").height($(".A_B_height").height() - 15);
    $("#S_C_height").height($(".S_C_height").height() - 15);
    $("#side_head_foot").height($(window).height() - ($(".head_sidebar").height() + $(".sidebarFoot").height()));
    $(".window-header").height(bodyHeight - 20);
    $("#allProductHeight").height(bodyHeight - ($(".item-heading").height() + 70));
    $("#cart_product_list").height(bodyHeight - ($(".panel-right-side .panel-heading").height() + $(".table-calculate-price").height() + 4));
    /*------Activity Page------*/
    $(".window-header-search").height(bodyHeight - $(".searchDiv").height() - 2);
    $(".window-header-cname_chistory").height(bodyHeight - ($(".customer_name").height() + $(".customer_history").height() + $(".table_head").height() + 70));
    /*------CheckOut Refund-----*/
    $(".full_height_button").height(bodyHeight - 55);
    $("#UserInfo_checkout").height(bodyHeight - ($(".item-heading").height() + $("#userinfo_footer").height() + 135));
    $("#UserInfo_refund").height(bodyHeight - ($(".item-heading").height() + 79));
    $("#UserInfo_withbtn").height(bodyHeight - ($("#height90").height() + $(".item-heading").height() + 100));
    $("#buttonGroupPanel").height(bodyHeight - ($(".item-heading").height() + 78));
    $("#buttonGroup").height($(".items.preson_info").height() - ($(".panel.panelCalculator").height() + 60));
    /*-----customer view page-----*/
    $(".table_customer-history").height(bodyHeight - ($(".customer_name").height() + $(".customer_history").height() + $(".panel-footer-heading").height() + 132));
    $("#header-search-button").height(bodyHeight - ($(".create-new-customer").height() + $(".item-heading").height() + $(".search_customer_input").height() + 76));
    /*----customer display Page------*/
    $(".product_selled .advertise_img img").height(bodyHeight + 18);
    $("#cart_product_list_cview").height(bodyHeight - ($(".panel-right-side .panel-heading").height() + $(".table-calculate-price").height() - 17));
    /*----cash report------*/
    $(".cash-report").height(bodyHeight - ($(".customer_name").height() + $(".customer_history").height() + $(".table_head").height() + $("#cashblanceheight").height() + 65));

 EnableContentScroll();
}
$(document).ready(function() {

    $('#sidebarCollapse , .overlay').on('click', function() {
        $('#sidebar').toggleClass('active');
        $('#wrapper-module-with-slidebar').toggleClass('active');
        $('.overlay').fadeToggle();
        $(this).toggleClass('active');
    });
    $(".overflowscroll").mCustomScrollbar({
        theme: "minimal-dark",
    });

});

function EnableContentScroll(){
    $(".overflowscroll").mCustomScrollbar({
        theme: "minimal-dark",
    });
}
var countDown = function() {
    var d = new Date(new Date().getTime() + 800 * 120 * 120 * 2000);
    simplyCountdown('.simply-countdown-one', {
        year: d.getFullYear(),
        month: d.getMonth() - 7,
        day: d.getDate()
    });
};
countDown();

